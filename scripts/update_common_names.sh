#!/bin/bash
#echo "begin;truncate common_names_taxon; INSERT INTO common_names_taxon (name,cnt) SELECT ext_name,count(*) AS cnt FROM entry FORCE INDEX (ext_name_2) WHERE `type`='Q16521' GROUP BY ext_name HAVING cnt>2; commit;" | mysql --defaults-file=~/replica.my.cnf -h tools-db s51434__mixnmatch_p
#echo "begin;truncate common_names; insert into common_names select * from vw_common_names; commit;" | mysql --defaults-file=~/replica.my.cnf -h tools-db s51434__mixnmatch_p

echo 'begin;truncate common_names; INSERT IGNORE INTO `common_names_human` (`name`,`cnt`,`entry_ids`) SELECT `ext_name`,count(DISTINCT `catalog`) AS `cnt`,group_concat(`id`) as `entry_ids` FROM `entry` WHERE `type`="Q5" AND `q` IS NULL AND `ext_name` LIKE "____% ____% ____%" AND `ext_desc`!="" AND `catalog` IN (SELECT `id` FROM `catalog` WHERE `active`=1) GROUP BY `ext_name` HAVING `cnt`>=5; commit;' | mysql --defaults-file=~/replica.my.cnf -h tools-db s51434__mixnmatch_p
