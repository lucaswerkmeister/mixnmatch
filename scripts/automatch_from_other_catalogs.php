#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$am = new MixNMatch\AutoMatch ;

if ( !isset($argv[1]) ) die ( "Requires catalog name" );

$catalog = $argv[1] ;
if ( $catalog == 'all' ) {
	$catalogs = [] ;
	$sql = "SELECT id from catalog WHERE active=1" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $catalogs[] = $o->id ;
	$total = count($catalogs) ;
	foreach ( $catalogs AS $num => $catalog ) {
		print "Catalog {$catalog} ({$num} out of {$total})\n" ;
		$am->automatch_from_other_catalogs($catalog);
	}
} else {
	$am->automatch_from_other_catalogs($catalog);
}

?>