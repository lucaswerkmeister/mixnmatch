#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;
$qs = array() ;
$sql = "SELECT DISTINCT q FROM entry WHERE `user`=0 AND `q`>0" ;
if ( isset ( $argv[1] ) ) $sql .= " AND `catalog`=" . $argv[1] ;
else {
	$r = rand() / getrandmax() ;
	$sql .= " AND `random`>={$r} ORDER BY `random` LIMIT 100000" ;
}

$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $qs[] = 'Q'.$o->q ;

if ( count($qs) == 0 ) exit ( 0 ) ;
print count($qs)."\n";

$dbwd = $mnm->tfc->openDB ( 'wikidata' , 'wikidata' , true ) ;
$sql = "SELECT DISTINCT page_title FROM page,pagelinks WHERE pl_from=page_id AND page_namespace=0 AND pl_namespace=0 AND pl_title IN ('Q4167410','Q4167410','Q4167836') AND page_title IN ('" . implode("','",$qs) . "')" ;
$qs = [] ;
$result = $mnm->tfc->getSQL ( $dbwd , $sql ) ;
while($o = $result->fetch_object()) $qs[] = preg_replace ( '/\D/' , '' , $o->page_title ) ;

$qm = array_chunk ( $qs , 10000 ) ;
$qs = [] ;
foreach ( $qm AS $qs ) {
	$sql = "UPDATE entry SET q=null,user=null,timestamp=null WHERE user=0 AND q IN (".implode(',',$qs).")" ;
	$mnm->getSQL ( $sql ) ;
}

?>