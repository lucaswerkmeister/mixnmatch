#!/bin/bash
cd /data/project/mix-n-match
echo "SELECT term_full_entity_id,term_text FROM wb_terms WHERE term_entity_type='item' AND term_language='en' AND term_type='description' AND term_text LIKE 'painting by %' AND NOT EXISTS (SELECT * FROM page,pagelinks WHERE pl_from=page_id AND page_namespace=0 AND page_title=term_full_entity_id AND pl_namespace=120 AND pl_title='P170')" | sql wikidata > paintings_without_creator.tab.tmp
mv paintings_without_creator.tab.tmp ~/manual_lists/paintings_without_creator.tab
