#!/usr/bin/php
<?PHP

$min_q_required = 1 ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

function matchOnDates ( $born , $died , $name ) {
	global $mnm , $min_q_required ;

	$q = '' ;
	$qs = [] ;
	$entries2set = [] ;

	$sql = "SELECT * FROM vw_dates WHERE ext_name='" . $mnm->escape($name) . "' AND substr(born,1,4)='{$born}' AND substr(died,1,4)='{$died}'" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		if ( isset($o->q) AND isset($o->user) AND $o->user>0 AND $o->q>0 ) {
			$q = "{$o->q}" ;
			if ( isset($qs[$q]) ) $qs[$q]++ ;
			else $qs[$q] = 1 ;
		} else if ( (isset($o->user) and $o->user=0) or (!isset($o->q)) ) {
			$entries2set[] = $o ;
		} else { # Something odd, abort, abort!
			return ;
		}
	}

	if ( count($qs) != 1 ) return ; // None, or multiple, q values, abort!
	if ( count($entries2set) == 0 ) return ; // Nothing to do
	if ( $qs[$q] < $min_q_required ) return ; // Not enough other evidence

	$ret = 0 ;
	foreach ( $entries2set AS $o ) {
		if ( $mnm->setMatchForEntryID ( $o->entry_id , $q , 4 , true , false ) ) $ret++ ;
	}
	return $ret ;
}

$dir = '/data/project/mix-n-match' ;
$years_both_file = $dir . '/years_both' ;

// Refresh data file
if ( 1 ) {
	exec ( 'echo "connect s51434__mixnmatch_p ; SELECT concat(substr(born,1,4),\'-\',substr(died,1,4)) AS years,ext_name FROM vw_dates WHERE q is null or user=0 HAVING length(years)=9" | sql local | sort -u > /data/project/mix-n-match/years_noq' ) ;
	exec ( 'echo "connect s51434__mixnmatch_p ; SELECT concat(substr(born,1,4),\'-\',substr(died,1,4)) AS years,ext_name FROM vw_dates WHERE q is NOT null AND user>0 HAVING length(years)=9" | sql local | sort -u  > /data/project/mix-n-match/years_withq' ) ;
	exec ( 'cat /data/project/mix-n-match/years_noq /data/project/mix-n-match/years_withq | sort | uniq -d > ' . $years_both_file ) ;
}

$cnt = 0 ;
$mnm = new MixNMatch\MixNMatch ;
$rows = explode ( "\n" , file_get_contents ( $years_both_file ) ) ;
foreach ( $rows AS $row ) {
	if ( !preg_match ( '/^(\d{4})-(\d{4})\t(.+?)$/' , $row , $m ) ) continue ;
	$did_set = matchOnDates ( $m[1] , $m[2] , $m[3] ) ;
	if ( isset($did_set) ) $cnt += $did_set ;
}
print "{$cnt} entries set (level {$min_q_required})!\n" ;

?>