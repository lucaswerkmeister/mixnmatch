#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once dirname(__DIR__) . '/vendor/autoload.php';

function retire_properties_with_catalog () {
	global $mnm ;
	$sql = "UPDATE `props_todo` SET `status`='HAS_CATALOG',`note`='Auto-matched to catalog' WHERE `status`='NO_CATALOG' AND `property_num` IN (SELECT `wd_prop` FROM `catalog` WHERE `wd_prop` IS NOT NULL and `wd_qual` IS NULL and `active`=1)" ;
	$mnm->getSQL ( $sql ) ;
}

function import_new_properties_from_sparql () {
	global $mnm ;

	$props = [] ;
	$sql = "SELECT `property_num` FROM `props_todo`" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $props[] = $o->property_num ;


	# Authority Control properties for people, without Mix'n'match catalog
	$sparql = 'SELECT ?p ?pLabel WHERE { ?p rdf:type wikibase:Property; wdt:P31 wd:Q19595382. MINUS { ?p wdt:P2264 _:b2. } SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } } order by ?pLabel' ;
	$j = $mnm->tfc->getSPARQL($sparql);
	foreach ( $j->results->bindings AS $b ) {
		$p = preg_replace ( '|^.+/P|' , '' , $b->p->value ) * 1 ;
		$label = $mnm->escape($b->pLabel->value) ;
		if ( in_array($p, $props) ) continue ;
		$sql = "INSERT IGNORE INTO `props_todo` (`property_num`,`property_name`,`default_type`) VALUES ({$p},'{$label}','Q5')" ;
		$mnm->getSQL ( $sql ) ;
	}
}

$mnm = new MixNMatch\MixNMatch ;

retire_properties_with_catalog();

import_new_properties_from_sparql();

retire_properties_with_catalog();

?>