#!/usr/bin/php
<?PHP

# This script imports/updates aux data like `| P625: 33.56/35.371 |` from entry descriptions

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog = $argv[1] ?? 0 * 1 ;
if ( $catalog <= 0 ) die ( "Missing argument: CATALOG_ID\n" ) ;

$mnm = new MixNMatch\MixNMatch ;
$sql = "SELECT id,ext_desc FROM entry WHERE catalog={$catalog}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	if ( !preg_match_all('/\| *P(\d+) *: *([^\| ]+)/',$o->ext_desc,$matches, PREG_SET_ORDER) ) continue ;
	foreach ( $matches AS $m ) {
		$property = $m[1] * 1 ;
		$value = $m[2] ;
		if ( $property == 625 ) {
			if ( preg_match ( '|^(.+)/(.+)$|' , $value , $m ) ) {
				$mnm->setLocation ( $o->id , $m[1] , $m[2] ) ;
			}
		} else {
			$mnm->setAux ( $o->id , $property , $value ) ;
		}
	}
}

?>