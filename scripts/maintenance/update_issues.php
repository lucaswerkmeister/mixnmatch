#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

# Close issues from inactive catalogs
$sql = "UPDATE `issues` set `status`='INACTIVE_CATALOG' where `status`='OPEN' AND EXISTS (SELECT * FROM entry,catalog WHERE entry_id=entry.id AND entry.catalog=catalog.id AND active!=1)" ;
$mnm->getSQL ( $sql ) ;

# Close date mismatch issues where the mnm data is Jan 01
$sql = "UPDATE `issues` SET `status`='JAN01' WHERE `status`='OPEN' AND `type`='MISMATCH_DATES' AND json LIKE '%mnm_time%-01-01%'" ;
$mnm->getSQL ( $sql ) ;

# Remove issues matched to Q0 or Q-1
$sql = "DELETE FROM `issues` where `status`='OPEN' AND EXISTS (SELECT * FROM entry WHERE entry_id=entry.id AND entry.q<=0 AND entry.user>0)" ;
$mnm->getSQL ( $sql ) ;

# Auto-close duplicates that are redirects
$sql = "SELECT * FROM `issues` WHERE `status`='OPEN' AND `type`='WD_DUPLICATE'" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$items = json_decode ( $o->json ) ;
	$items = (array) $items ;
	$items = array_values($items);
	if ( count($items) != 2 ) continue ; # Too complicated...
	$q1 = $items[0] ;
	$q2 = $items[1] ;
	$sql = "SELECT * FROM `page`,`redirect` WHERE `page_namespace`=0 AND `page_title` IN ('{$q1}','{$q2}') AND page_id=rd_from AND `rd_namespace`=0 AND `rd_title` IN ('{$q1}','{$q2}') AND `page_title`!='rd_title'" ;
	$mnm->openWikidataDB() ;
	$result2 = $mnm->tfc->getSQL ( $mnm->dbwd , $sql ) ;
	if ($o2 = $result2->fetch_object()) {
		$sql = "UPDATE `issues` SET `status`='RESOLVED_ON_WIKIDATA' WHERE id={$o->id}" ;
		$mnm->getSQL ( $sql ) ;
	}
}

?>