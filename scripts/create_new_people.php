#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once ( "/data/project/mix-n-match/manual_lists/large_catalogs/shared.php" ) ;

$lc = new largeCatalog (2) ; # VIAF
$lc->prop2field[2]['P213'] = 'ISNI' ; // Activating for this specific purpose
$lc->prop2field[2]['P1006'] = 'NTA' ; // Activating for this specific purpose

$mnm = new MixNMatch\MixNMatch ;
$mnm->tfc->getQS('mixnmatch:CreateNewPeople','',true) ;

$maintenance = new MixNMatch\Maintenance ;

$min_commands = 7 ;
$min_cnt = 10 ;
if ( isset($argv[1]) ) $min_cnt = $argv[1] * 1 ;

$touched_catalogs = [] ;
$sql = "SELECT * FROM common_names_human WHERE cnt>={$min_cnt}" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $cnh = $result->fetch_object() ) {
	$entry_ids = explode ( ',' , $cnh->entry_ids ) ;
	try {
		$q = $maintenance->createItemFromMultipleEntries ( $entry_ids , $cnh->name , $min_commands , $min_cnt ) ;
		print "Created https://www.wikidata.org/wiki/{$q} for {$cnh->name}\n" ;
		// Remove common name from list
		$sql = "DELETE FROM common_names_human WHERE id={$cnh->id}" ;
		$mnm->getSQL ( $sql ) ;
	} catch (\Exception $e) {
		print_r($e);
	}
}

foreach ( $touched_catalogs AS $catalog => $dummy ) {
	$mnm->queue_job($catalog,'microsync');
}

?>