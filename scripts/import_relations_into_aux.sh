#!/bin/bash
# Clears the auxiliary table from broken jonk; might not be necessary
#echo 'connect s51434__mixnmatch_p; DELETE FROM auxiliary WHERE aux_p IN (select distinct property from mnm_relation) AND aux_name NOT LIKE "Q%";' | sql local


# This updates the auxiliary table from mnm_relations where q has been set in the target entry
echo 'connect s51434__mixnmatch_p; INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) SELECT mnm_relation.entry_id,property,concat("Q",entry.q) FROM entry,mnm_relation WHERE target_entry_id=entry.id AND user>0 AND q>0 AND NOT EXISTS (SELECT * FROM auxiliary WHERE auxiliary.entry_id=mnm_relation.entry_id AND aux_p=property);' | sql local
