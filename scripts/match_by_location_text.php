#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

if ( !isset ( $argv[1] ) ) {
	print "Needs argument : catalog_id\n" ;
	exit ( 0 ) ;
}

$catalog_id = $argv[1] * 1 ;
$catalog = $mnm->loadCatalog($catalog_id,true) ;
$lang = $catalog->data()->search_wp;

$found = 0 ;
$sql = "SELECT entry.id,entry.ext_name,location_text.text FROM location_text,entry WHERE entry_id=entry.id AND location_text.property=131 AND catalog={$catalog_id} AND (entry.q IS NULL or entry.user=0)" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$label = $o->ext_name ;
	$loc_label = $o->text ;
	$label = trim(preg_replace('/\s*\(.*\)\s*/',' ',$label)) ;
	$loc_label = trim(preg_replace('/^(Kreis|Provinz|Woiwodschaft|Unitary Authority|Autonomen Republik|County|Region) /','',$loc_label)) ;
	$sparql = "SELECT DISTINCT ?q { ?q rdfs:label '{$label}'@{$lang} ; wdt:P131* ?loc . ?loc rdfs:label '{$loc_label}'@{$lang} }" ;
	$qs = $mnm->tfc->getSPARQLitems($sparql,'q') ;
	if ( count($qs) == 0 ) continue ;
	if ( count($qs) > 1 ) {
		print "MORE THAN ONE: https://mix-n-match.toolforge.org/#/entry/{$o->id} : {$label} / {$loc_label}\n" ;
		print_r($qs) ;
		continue ;
	}
	$q = $qs[0] ;
	#print "{$label}/{$o->id} => $q\n" ;
	$mnm->setMatchForEntryID($o->id,$q,4,true,false);
	$found++ ;
}

if ( $found > 0 ) $mnm->queue_job($catalog_id,'microsync');

?>