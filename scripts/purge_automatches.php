#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1] ) ) die ( "Catalog ID required\n" ) ;
$catalog = $argv[1] * 1 ; # eg 3268
if ( $catalog == 0 ) die ( "Bad catalog ID {$argv[1]}\n" ) ;

$mnm = new MixNMatch\MixNMatch ;

$sql = "UPDATE entry SET q=NULL,user=NULL,`timestamp`=NULL WHERE catalog={$catalog} AND user=0" ;
$mnm->getSQL ( $sql ) ;

$sql = "DELETE FROM multi_match WHERE catalog={$catalog}" ;
$mnm->getSQL ( $sql ) ;


?>