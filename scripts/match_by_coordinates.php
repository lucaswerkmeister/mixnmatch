#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

// $commands = [] , one QS V1 command per element, no newlines
function runCommandsQS ( $commands ) {
	global $qs ;
	$commands = implode ( "\n" , $commands ) ;
	$tmp_uncompressed = $qs->importData ( $commands , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp_uncompressed['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	if ( !isset($qs->last_item) ) return ;
	$last_item = 'Q' . preg_replace ( '/\D/' , '' , "{$qs->last_item}" ) ;
	return $last_item ;
}

function getQS () {
	global $toolname ;
	$qs = new \QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/mix-n-match/reinheitsgebot.conf" ;
	$qs->toolname = $toolname ;
	$qs->sleep = 1 ;
	$qs->use_command_compression = true ;
	return $qs ;
}

function tryMatchViaSearch ( $entry , $results ) {
	global $mnm , $catalogs_to_microsync , $simulate , $verbose ;
	$matches = [] ;
	$ext_name = strtolower($entry->ext_name) ;
	foreach ( $results AS $r ) {
		$titles = explode ( "\n" , strtolower($r->snippet) ) ;
		if ( in_array($ext_name, $titles) ) {
			$matches[] = $r->title ;
		} else {
			foreach ( $titles AS $t ) {
				$t = trim($t) ;
				if ( str_starts_with($t, "{$ext_name} ") or str_ends_with($t, " {$ext_name}") ) {
					$matches[] = $r->title ;
					break ;
				}
			}
		}
	}
	if ( count($matches)==1 ) {
		$q = $matches[0] ;
		print "Matching https://mix-n-match.toolforge.org/#/entry/{$entry->id} to https://www.wikidata.org/wiki/{$q}\n" ;
		if ( !$simulate ) $mnm->setMatchForEntryID ( $entry->id , $q , 4 , true , false ) ;
		$catalogs_to_microsync["{$o->catalog}"] = true ;
		return true ;
	} else if ( count($matches)>1 ) {
		print "WARNING: https://mix-n-match.toolforge.org/#/entry/{$entry->id} seems to match:\n" ;
		foreach ( $matches AS $m ) print "- https://www.wikidata.org/wiki/{$m}\n" ;
	}
	return false ;
}

function tryMatchViaSparql ( $entry ) {
	global $mnm , $max_distance_sparql , $max_automatch_distance , $verbose ;
	# Check SPARQL (thorough)
	$sparql = "SELECT ?place ?location ?distance ?p31 WHERE {
    SERVICE wikibase:around { 
      ?place wdt:P625 ?location . 
      bd:serviceParam wikibase:center 'Point({$entry->lon} {$entry->lat})'^^geo:wktLiteral . 
      bd:serviceParam wikibase:radius '{$max_distance_sparql}' . 
      bd:serviceParam wikibase:distance ?distance .
    }
    OPTIONAL { ?place wdt:P31 ?p31 }
} ORDER BY (?distance) LIMIT 500" ;
	if ( $verbose ) print "{$sparql}\n\n" ;
	$j = $mnm->tfc->getSPARQL ( $sparql , 'place' ) ;
	$bindings = ($j->results->bindings??[]) ;
	foreach ( $bindings as $b ) {
		if ( $b->distance->value > $max_automatch_distance ) break ;
		if ( !isset($b->p31) or !isset($b->p31->value) ) continue ;
		if ( ($b->p31->type??'') != 'uri' ) continue ;
		$p31 = preg_replace('|^.+/|','',$b->p31->value);
		if ( $p31 != $entry->type ) continue ;
		$q = preg_replace('|^.+/|','',$b->place->value);
		if ( $q == "Q{$entry->q}" ) break ; # Already the target
		print "https://mix-n-match.toolforge.org/#/entry/{$entry->id} => {$b->place->value} AS AUTOMATCH\n" ;
		if ( !$simulate ) $mnm->setMatchForEntryID ( $entry->id , $q , 0 , true , false ) ;
		break ;
	}
	return count($bindings) == 0 ;
}


$simulate = $argv[2]??false ;

$default_max_distance = '500m' ;
$max_automatch_distance = 0.1 ; # 100m
$max_results_for_random_catalogs = 5000 ;
$verbose = false ;

$mnm = new MixNMatch\MixNMatch ;
$qs = getQS();

$permissions = [] ; 
$sql = "SELECT * FROM kv_catalog WHERE kv_key IN ('allow_location_match','allow_location_create','allow_location_operations','location_distance','location_force_same_type')" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	if ( $o->kv_key == 'location_distance' ) {
		$permissions[$o->kv_key][$o->catalog_id] = $o->kv_value  ;
	} else {
		$permissions[$o->kv_key][$o->kv_value][] = $o->catalog_id ;
	}
}
$bad_catalogs = $permissions['allow_location_operations']['no'] ;

$use_single_catalog = false ;
if ( isset($argv[1]) and $argv[1]!='0') {
	$catalog = $argv[1] * 1 ;
	$use_single_catalog = true ;
	if ( in_array($catalog,$bad_catalogs) ) {
		print "Catalog {$catalog} is in the list of Bad Catalogs for this!\n" ;
		exit(0);
	}
}

if ( $simulate ) {
	print "SIMULATING!\n" ;
	#$verbose = true ;
	if ( $use_single_catalog ) {
		#$permissions['allow_location_match']['yes'][] = $catalog ;
		#$permissions['allow_location_create']['yes'][] = $catalog ;
	}
}

$catalogs_to_microsync = [] ;

$sql = "SELECT * FROM vw_location WHERE (q IS NULL OR user=0) AND ext_name!=''" ;
if ( $use_single_catalog ) {
	$sql .= " AND catalog={$catalog}" ;
} else {
	$r = rand()/getrandmax() ;
	$sql .= " AND catalog NOT IN (".implode(',',$bad_catalogs).")" ;
	$sql .= " AND `random`>={$r} ORDER BY `random` LIMIT {$max_results_for_random_catalogs}" ;
}

$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$prop = '' ;
	$value = '' ;
	$force_same_type = in_array($o->catalog, $permissions['location_force_same_type']['yes']) ;
	if ( $force_same_type ) {
		$prop = 'P31' ;
		$value = $o->type ;
	}

	$max_distance = $permissions['location_distance'][$o->catalog] ?? $default_max_distance ;
	$max_distance_sparql = 1 ; # km
	if ( preg_match("/^([0-9.]+)km$/",$max_distance,$m) ) $max_distance_sparql = $m[1]*1 ;
	else if ( preg_match("/^([0-9.]+)m$/",$max_distance,$m) ) $max_distance_sparql = ((float)$m[1])/1000 ;


	# Check search (quick)
	$query = "nearcoord:{$max_distance},{$o->lat},{$o->lon}" ;
	$sr = $mnm->getSearchResults ( $query , $prop , $value ) ;
	if ( $verbose ) print "https://www.wikidata.org/w/index.php?search=".urlencode($query)."\n" ;
	if ( count($sr) > 0 ) {
		if ( in_array($o->catalog, $permissions['allow_location_match']['yes']) ) {
			if ( !tryMatchViaSearch ( $o , $sr ) ) tryMatchViaSparql ( $o ) ;
		}
		continue ;
	}
	if ( !in_array($o->catalog, $permissions['allow_location_create']['yes']) ) continue ; # No point

	if ( !tryMatchViaSparql ( $o ) ) continue ;

	if ( $verbose ) {
		print_r($o);
		print "https://wikishootme.toolforge.org/#lat={$o->lat}&lng={$o->lon}&zoom=15&layers=mixnmatch,wikidata_image,wikidata_no_image\n" ;
	}

	$commands = $mnm->getCreateItemForEntryCommands($o) ;
	if ( !isset($commands) ) continue ;
	if ( $verbose ) print_r($commands);
	if ( !$simulate ) {
		$q = runCommandsQS($commands) ;
		$mnm->setMatchForEntryID ( $o->id , $q , 4 , true , false ) ;
	} else $q = "DUMMY" ;
	print "Matching https://mix-n-match.toolforge.org/#/entry/{$o->id} to https://www.wikidata.org/wiki/{$q} (NEW); catalog {$o->catalog}\n" ;
}

foreach ( $catalogs_to_microsync as $catalog_id => $dummy ) {
	print "Syncing {$catalog_id}\n" ;
	if ( !$simulate ) $mnm->queue_job($catalog_id,'microsync');
}

?>