#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;


function matchCatalog ( $meta ) {
	global $mnm ;
	if ( is_array($meta) ) $meta = (object) $meta ;

	$sql = "SELECT * FROM vw_aux WHERE catalog={$meta->catalog} AND (user=0 OR q IS NULL)" ;
	if ( isset($meta->year_prop) ) $sql .= " AND aux_p={$meta->year_prop} AND `type`!=''" ;

	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		match_entry ( $meta , $o ) ;
	}
}

function match_entry ( $meta , $o ) {
	global $mnm ;

	$candidate_items = [] ;

	if ( isset($meta->year_prop) ) {
		if ( !preg_match ( '|^\+(\d+)|' , $o->aux_name , $m ) ) return ;
		$year = $m[1] ;
		$label = str_replace ( '\'' , '\\\'' , $o->ext_name) ;
		$sparql = "SELECT DISTINCT ?q { VALUES ?props { wdt:P1476 rdfs:label } ?q wdt:P31|wdt:P279* wd:{$o->type} ; ?props '{$label}'@en ; wdt:P{$meta->year_prop} ?date FILTER ( year(?date) = {$year} ) }" ;
		$candidate_items = $mnm->tfc->getSPARQLitems ( $sparql ) ;
	}

	if ( count ( $candidate_items) != 1 ) return ;

	$q = array_shift ( $candidate_items ) ;
	$mnm->setMatchForEntryID ( $o->id , $q , 4 , true , false ) ;
}

$metadata = [
	[
		'catalog' => 3457 ,
		'year_prop' => 577
	] ,
] ;

foreach ( $metadata AS $meta ) {
	matchCatalog ( $meta ) ;
}

?>