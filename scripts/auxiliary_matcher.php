#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$catalog = ($argv[1] ?? -1) * 1 ;
if ( $catalog < 0 ) die ( "Usage : {$argv[0]} CATALOG_ID [use_random=0] [batch_size=-1] [TESTING=0]\n") ;
$use_rand = ($argv[2] ?? 0) * 1 ;
if ( $catalog == 0 ) $use_rand = true ;
$batch = ($argv[3] ?? -1) * 1 ;
$testing = ($argv[4] ?? 0)*1 == 1 ;

$aux = new MixNMatch\Auxiliary ( $testing ) ;
$aux->matchEntriesViaAuxiliary ( $catalog , $use_rand , $batch ) ;

?>