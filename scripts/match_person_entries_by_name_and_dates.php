#!/usr/bin/php
<?PHP

# THIS SCRIPT CREATES "FULL" NEW MATCHES TO WIKIDATA ITEMS, BASED ON (FUZZY) NAME, BIRTH&DEATH YEAR MATCH

#USAGE:
# match_person_entries_by_name_and_dates.php // Random 50K
# match_person_entries_by_name_and_dates.php 1[,2,3,...] // Specific catalog(s)

require_once dirname(__DIR__) . '/vendor/autoload.php';
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);


if ( isset($argv[1]) ) {
	$catalog = $argv[1]*1 ;
	if ( $catalog <= 0 ) die ( "Bad catalog: {$argv[1]}\n" ) ;
}

$verbose = preg_match ( '/\bverbose\b/' , $argv[2]??'') ;

$pm = new MixNMatch\PersonMatcher ( false , true , $verbose ) ;

$sql = "" ;
if ( isset($catalog) ) {
	$sql = "SELECT * FROM vw_dates WHERE catalog IN ({$catalog}) AND (q IS NULL or user=0) AND born!='' AND died!=''" ;
} else {
	$sql = "SELECT max(id) AS m FROM entry" ;
	$result = $pm->mnm->getSQL ( $sql ) ;
	$o = $result->fetch_object() ;
	$r = (int) ( $o->m * ( rand() / getrandmax() ) );

	$sql = "SELECT entry_id,born,died,ext_name FROM person_dates,entry WHERE entry_id=entry.id AND entry_id>={$r} AND (q IS NULL or q=-1 or user=0) AND born!='' AND died!='' ORDER BY entry_id LIMIT 50000" ;
}

$result = $pm->mnm->getSQL ( $sql ) ;
if ( $verbose ) print "Initial query done.\n" ;
while($o = $result->fetch_object()){
	$pm->process_entry ( $o ) ;
}

if ( isset($catalog) ) {
	$pm->mnm->queue_job($catalog,'microsync');
}

?>