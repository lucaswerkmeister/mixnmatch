#!/usr/bin/php
<?PHP
declare(strict_types=1);

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # E_ALL|
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

function add2sql ( $get_prop , &$sql ) {
	global $mnm ;
	$query = "SELECT ?p ?v ?vLabel { ?p rdf:type wikibase:Property ; wdt:P{$get_prop} ?v SERVICE wikibase:label { bd:serviceParam wikibase:language 'en' } }" ;
	foreach ( $mnm->tfc->getSPARQL_TSV ( $query ) as $o ) {
		$prop = preg_replace('|\D|','',$mnm->tfc->parseItemFromURL($o['p']))*1 ;
		$item = preg_replace('|\D|','',$mnm->tfc->parseItemFromURL($o['v']))*1 ;
		$sql[] = "({$get_prop},{$prop},{$item},'".$mnm->escape($o['vLabel'])."')" ;
	}
}

$mnm = new MixNMatch\MixNMatch ;

$sql = [] ;
add2sql(17,$sql) ;
add2sql(31,$sql) ;
if ( count($sql)<20000 ) exit(0); # Emergency exit in case something went wrong with the queries
$mnm->getSQL("TRUNCATE `property_cache`") ;
$sql = "INSERT INTO `property_cache` (`prop_group`,`property`,`item`,`label`) VALUES " . implode(',',$sql) ;
$mnm->getSQL($sql) ;

?>