#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

# Which props are already in the database?
$known_props = [] ;
$sql = "SELECT DISTINCT property FROM auth_control_gender" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $known_props[$o->property] = 1 ;

# All external ID props
$props = $mnm->tfc->getSPARQLitems ( 'SELECT DISTINCT ?property { ?property wikibase:propertyType wikibase:ExternalId ; wdt:P31 wd:Q19595382 }' ) ;

# Max number of records for external ID props
$number_of_records = [] ;
$j = $mnm->tfc->getSPARQL ( 'SELECT ?property (max(?nor*1) AS ?records) { ?property wikibase:propertyType wikibase:ExternalId ; wdt:P31 wd:Q19595382 ; wdt:P4876 ?nor } GROUP BY ?property' ) ;
foreach ( $j->results->bindings AS $b ) {
	if ( !isset($b->records) or !isset($b->records->value) ) continue ;
	$prop = $mnm->tfc->parseItemFromURL ( $b->property->value ) ;
	$prop = preg_replace ( '/\D/' , '' , $prop ) ; # Numeric
	$nor = $b->records->value * 1 ;
	if ( $nor == 0 ) continue ;
	$number_of_records[$prop] = $nor ;
}

# Try this multiple times, sometimes we get lucky...
function subSparql ( $sparql ) { # Varname must be `cnt`
	global $mnm ;
	$max = 5 ;
	$count = 0 ;
	while ( $count < $max ) {
		$j = $mnm->tfc->getSPARQL ( $sparql ) ;
		if ( !isset($j) or $j === null or $j === FALSE or !isset($j->results) or !isset($j->results->bindings) or count($j->results->bindings) != 1 ) {
			$count++ ;
			sleep ( 2*$count ) ;
			continue ;
		}
		$b = $j->results->bindings[0] ;
		return $b->cnt->value ;
	}
}

foreach ( $props AS $prop ) {
	$prop = preg_replace ( '/\D/' , '' , $prop ) ; # Numeric
	if ( isset ( $known_props[$prop]) ) continue ;

	$total = subSparql ( "SELECT (count(?item) AS ?cnt) { ?item wdt:P{$prop} [] ; wdt:P31 wd:Q5 }" ) ;
	$male = subSparql ( "SELECT (count(?item) AS ?cnt) { ?item wdt:P{$prop} [] ; wdt:P31 wd:Q5 ; wdt:P21 wd:Q6581097 }" ) ;
	$female = subSparql ( "SELECT (count(?item) AS ?cnt) { ?item wdt:P{$prop} [] ; wdt:P31 wd:Q5 ; wdt:P21 wd:Q6581072 }" ) ;
	$unknown = subSparql ( "SELECT (count(?item) AS ?cnt) { ?item wdt:P{$prop} [] ; wdt:P31 wd:Q5 MINUS { ?item wdt:P21 [] } }" ) ;

	if ( isset($total) and isset($male) and isset($female) and !isset($unknown) ) {
		# Fallback SPARQL
		$unknown = subSparql ( "SELECT (count(?item) AS ?cnt) { ?item wdt:P{$prop} [] ; wdt:P31 wd:Q5 . OPTIONAL { ?item wdt:P21 ?gender } FILTER (!BOUND(?gender)) }" ) ;
		if ( !isset($unknown) ) {
			# This does not allow for "other" but at lease P214 will run through...
			$unknown = $total - $male - $female ;
		}
	}

	if ( !isset($total) or !isset($male) or !isset($female) or !isset($unknown) ) {
		print "P{$prop} failed {$total}/{$male}/{$female}/{$unknown}\n" ;
		$sql = "INSERT IGNORE `auth_control_gender` (property,query_failed) VALUES ({$prop},1)" ;
		#$mnm->getSQL ( $sql ) ;
		continue ;
	}

	print "P{$prop} succeeded with individual queries\n" ;

	$other = $total - $male - $female - $unknown ; # other
	if ( $other < 0 ) $other = 0 ; # For when some items have two genders.

	$keys = [ 'property','total','male','female','unknown','other' ] ;
	$values = [ $prop , $total , $male , $female , $unknown , $other ] ;
	if ( isset($number_of_records[$prop]) ) {
		$keys[] = 'number_of_records' ;
		$values[] = $number_of_records[$prop] ;
	}
	$sql = 'INSERT IGNORE INTO `auth_control_gender` (`' ;
	$sql .= implode ( '`,`' , $keys ) ;
	$sql .= '`) VALUES (' ;
	$sql .= implode ( ',' , $values ) ;
	$sql .= ')' ;

	$mnm = new MixNMatch ; # DB timeout
	$mnm->getSQL ( $sql ) ;
}

?>