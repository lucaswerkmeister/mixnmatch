#!/usr/bin/php
<?PHP

/*
This script can find entries with the same auxiliary data (VIAF or GND), where some entries are manually matched against Wikidata, and apply the same match to the ones that are not matched yet.
*/

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$total_matched = 0 ;
$used_catalogs = [] ;

$rely_on_entry_is_matched = false ;

if ( $rely_on_entry_is_matched ) {
	$sql = 'UPDATE auxiliary SET entry_is_matched=1 WHERE entry_is_matched=0 AND EXISTS (SELECT * FROM entry WHERE entry.id=entry_id AND q IS NOT NULL AND q>0 AND user IS NOT NULL and user>0)' ;
	$mnm->getSQL ( $sql ) ;
}

$sql = "SELECT aux_p,aux_name,group_concat(entry_id) AS entry_ids,count(entry_id) AS cnt,sum(entry_is_matched) AS entry_is_matched FROM auxiliary WHERE aux_p IN (214,227) GROUP BY aux_p,aux_name HAVING cnt>1 AND cnt>entry_is_matched" ;
if ( $rely_on_entry_is_matched ) $sql .= " AND entry_is_matched>0" ;

$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	$sql = "SELECT entry.* FROM entry,catalog WHERE entry.id IN ({$o->entry_ids}) AND catalog.id=entry.catalog AND catalog.active=1" ;
	$result2 = $mnm->getSQL ( $sql ) ;
	$catalogs = [] ;
	$manual_qs = [] ;
	$entries_without_q = [] ;
	while ( $o2 = $result2->fetch_object() ) {
		if ( isset($o2->user) and $o2->user != null and $o2->user > 0 ) $manual_qs[$o2->q] = $o2->q ;
		else {
			$entries_without_q[] = $o2->id ;
			if ( !isset($used_catalogs[$o2->catalog]) ) $catalogs[$o2->catalog] = 1 ;
		}
	}
	if ( count($manual_qs) == 0 ) continue ; # No q values to set
	if ( count($manual_qs) > 1 ) continue ; # Too many q values to set; TODO log potential issue
	if ( count($entries_without_q) == 0 ) continue ; # No entries to set

	$q = '' ;
	foreach ( $manual_qs AS $key => $dummy ) $q = $key ;
	#print_r ( $o ) ;
	#print "https://www.wikidata.org/wiki/Q{$q}:\n" ;
	foreach ( $entries_without_q AS $entry_id ) {
		#print "{$mnm->root_url}/#/entry/{$entry_id}\n" ;
		$mnm->setMatchForEntryID ( $entry_id , $q , 4 , true , false ) ;
		$total_matched++ ;
	}
	#$sql = "UPDATE `auxiliary` SET `entry_is_matched`=1 WHERE `aux_p`={$o->aux_p} AND `aux_name`='".$mnm->escape($o->aux_name)."' AND entry_id IN ({$o->entry_ids})" ;
	#print "{$sql}\n" ;

	foreach ( $catalogs AS $catalog ) $used_catalogs[$catalog] = 1 ;
}

if ( $total_matched > 0 ) print "Total matched: {$total_matched}\n" ;

foreach ( $used_catalogs AS $catalog => $dummy ) {
	#print "UPDATING CATALOG {$catalog}\n" ;
	$mnm->queue_job($catalog,'microsync',0,'',0,0,'LOW_PRIORITY');
}

?>