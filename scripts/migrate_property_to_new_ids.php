#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

function loadGroupedEntries ( $catalog_id ) {
	global $mnm ;
	$ret = [] ;
	$sql = "SELECT * FROM `entry` WHERE `catalog`={$catalog_id}" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		$ret[$o->ext_name][] = $o ;
	}
	return $ret ;
}

function description_check ( $old , $new ) {
	if ( $old->ext_desc == $new->ext_desc ) return true ;

	# Date check
	if ( preg_match_all ( '|(\d{3,4})|' , $old->ext_desc , $m ) ) {
		foreach ( $m[1] AS $year ) {
			if ( stristr($new->ext_desc,$year) === FALSE ) return false ;
		}
		return true ;
	}

	return false ;
}

$catalog_old = ($argv[1]??0)*1 ;
$catalog_new = ($argv[2]??0)*1 ;
if ( $catalog_old * $catalog_new == 0 ) die ( "USAGE: {$argv[0]} OLD_CATALOG_ID NEW_CATALOG_ID\n" ) ;

$mnm = new MixNMatch\MixNMatch ;
$co = $mnm->loadCatalog($catalog_old,true) ;
$cn = $mnm->loadCatalog($catalog_new,true) ;

$prop = $co->data()->wd_prop ;
if ( !isset($prop) ) $prop = $cn->data()->wd_prop ;
if ( !isset($prop) ) die ( "Missing/mismatching property\n") ;
$prop = "P{$prop}" ;

print "Migrating for {$prop}\n" ;

# Import old matches into old catalog
$catalog = new MixNMatch\Catalog ( $catalog_old , $mnm ) ;
$catalog->syncFromSPARQL($prop) ;

# Load all matches, grouped by names, from old and new catalog
$old = loadGroupedEntries ( $catalog_old ) ;
$new = loadGroupedEntries ( $catalog_new ) ;

# Aggregate used q in new catalog
$used_q = [] ;
foreach ( $new AS $name => $oa ) {
	foreach ( $oa AS $on ) {
		if ( !isset($on->user) or $on->user==0 ) continue ;
		if ( !isset($on->q) or $on->q <= 0 ) continue ;
		$used_q[$on->q] = 1 ;
	}
}

# Port unique name matches, respecting descriptions
foreach ( $new AS $name => $oa ) {
	if ( count($oa) != 1 ) continue ; # TODO flag up?
	$on = $oa[0] ;
	if ( isset($on->user) and $on->user>0 ) continue ;
	if ( isset($on->q) and $on->q <= 0 ) continue ;
	if ( !isset($old[$name]) ) continue ; # TODO flag up?
	if ( count($old[$name]) != 1 ) continue ; # TODO flag up?
	$oo = $old[$name][0] ;
	if ( !isset($oo->user) or $oo->user==0 ) continue ;
	if ( !isset($oo->q) or $oo->q <= 0 ) continue ;
	$use_auto_match = false ;
	if ( !description_check($oo,$on) ) { print "{$name} failed description check '{$oo->ext_desc}'/'{$on->ext_desc}'\n" ; $use_auto_match = true ; }
	if ( isset($used_q[$oo->q]) ) { print "{$name}: Q{$oo->q} already in use\n" ; $use_auto_match = true ; }
	$used_q[$oo->q] = 1 ;
	if ( $use_auto_match ) {
		if ( $on->q != $oo->q ) {
			print "Automatch {$name} '{$oo->ext_desc}'|'{$on->ext_desc}' {$oo->ext_id} => {$on->ext_id} [{$on->user}:{$on->q}/{$oo->q}]\n" ;
			$mnm->setMatchForEntryID ( $on->id , $oo->q , 0 , true , false ) ;
		}
		continue ;
	}
	print "Port {$name} '{$oo->ext_desc}'|'{$on->ext_desc}' {$oo->ext_id} => {$on->ext_id} [{$on->user}:{$on->q}/{$oo->q}]\n" ;
	$mnm->setMatchForEntryID ( $on->id , $oo->q , $oo->user , true , false ) ;
}

# Final checks
$double_qs = [] ;
$sql = "SELECT `q`,count(*) AS `cnt` FROM `entry` WHERE `catalog`={$catalog_new} AND `user`>0 AND `q` IS NOT NULL AND `q`>0 GROUP BY `q` HAVING `cnt`>1" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $double_qs[] = $o->q ;
if ( count($double_qs) > 0 ) {
	print "WARNING: The following Qs are used at least twice in manual matches:\nQ" . implode ( ", Q" , $double_qs ) . "\n" ;
}

?>