#!/usr/bin/php
<?PHP

ini_set('memory_limit','3000M');

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/../vendor/autoload.php';
require_once ( '/data/project/mix-n-match/manual_lists/large_catalogs/shared.php' ) ;

$add_references = true ;
$add_ext_id = true ;
$add_statement_if_no_date = true ;
$add_statement_if_different = false ;
$skip_catalogs = [ 98,101,471,510,675,257 ] ;
// 323 was deactivated for Sandra F, because upstream has better dates; day-resolution dates now imported
// 675 was deactivated for Andy Mabbett, to check
// 257 was deactivated https://www.wikidata.org/wiki/Topic:U2v1aqpvi1763jht

$mnm = new MixNMatch\MixNMatch ;
$lc = new largeCatalog ( 0 ) ;
unset($lc->db); # Free DB connection

$catalogs = [] ;


function getQS () {
	$toolname = 'mix-n-match' ; // Or fill this in manually
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = "Mix'n'match:add_person_dates" ;
	$qs->sleep = 1 ; // Sleep sec between edits
	return $qs ;
}

# Make sure we have all available subject items
function importSourceItems () {
	global $mnm , $catalogs ;
	$tmp_catalog = new Catalog ( 0 , $this->mnm ) ;
	$catalogs = $tmp_catalog->loadCatalogs() ;

	$props = [] ;
	foreach ( $catalogs AS $c ) {
		if ( isset($c->source_item) ) continue ; // Already done
		if ( isset($c->wd_prop) and !isset($c->wd_qual) ) $props["P{$c->wd_prop}"][] = $c->id ;
	}

	$mnm->wil->loadItems ( array_keys ( $props ) ) ;

	foreach ( $props AS $p => $cs ) {
		$i = $mnm->wil->getItem ( $p ) ;
		if ( !isset($i) ) continue ;
		$claims = $i->getClaims ( 'P1629' ) ;
		if ( count($claims) == 0 ) continue ;
		$q = $i->getTarget ( $claims[0] ) ;
		if ( !isset($q) ) continue ;
		if ( !preg_match ( '/^Q(\d+)$/' , $q , $m ) ) continue ;
		$q = $m[1] ;
		foreach ( $cs AS $c ) {
			$sql = "UPDATE catalog SET source_item=$q WHERE id=$c" ;
			$result = $mnm->getSQL($sql) ;
			$catalogs[$c]->source_item = $q ;
		}
	}
}

function setAsInWikidata ( $entry_id ) {
	global $mnm ;
	$sql = "UPDATE person_dates SET in_wikidata=1 WHERE entry_id=$entry_id AND in_wikidata=0" ;
	$mnm->getSQL($sql);
}

function checkDateExists ( $q , &$o , $what , $prop ) {
	global $catalogs , $mnm , $skip_catalogs , $global_out , $global_warning , $lc ;
	global $add_references , $add_statement_if_no_date , $add_statement_if_different ;

	if ( in_array ( $o->catalog , $skip_catalogs ) ) return ;

	$i = $mnm->wil->getItem ( $q ) ;
	if ( !isset($i) ) return ;
	
	# Human paranoia
	$is_human = false ;
	$claims = $i->getClaims ( 'P31' ) ;
	foreach ( $claims AS $c ) {
		if ( 'Q5' == $i->getTarget ( $c ) ) $is_human = true ;
	}
	if ( !$is_human ) return ;

	$out = "" ;
	$ref = "" ;
	if ( isset($catalogs[$o->catalog]->source_item) ) $ref .= "\tS248\tQ" . $catalogs[$o->catalog]->source_item ;

	if ( isset($catalogs[$o->catalog]->wd_prop) and !isset($catalogs[$o->catalog]->wd_qual) ) $ref .= "\tS{$catalogs[$o->catalog]->wd_prop}\t\"{$o->ext_id}\"" ;
	else if ( isset($o->ext_url) AND $o->ext_url != '' ) $ref .= "\tS854\t\"{$o->ext_url}\"" ;

	$year = '' ;
	$month = '01' ;
	$day = '01' ;
	$precision = '' ;
	$date = $o->$what ;
	if ( preg_match ( '/^(\d+)-(\d{1,2})-(\d{1,2})$/' , $date , $m ) ) { list(,$year,$month,$day)=$m ; $precision=11 ; }
	else if ( preg_match ( '/^(\d+)-(\d{1,2})$/' , $date , $m ) ) { list(,$year,$month)=$m ; $precision=10 ; }
	else if ( preg_match ( '/^(\d+)$/' , $date , $m ) ) { list(,$year)=$m ; $precision=9 ; }
	else return ; // Broken date
	if ( strlen($month) == 1 ) $month = "0$month" ;
	if ( strlen($day) == 1 ) $day = "0$day" ;
	$time = "+$year-$month-$day"."T00:00:00Z" ;
	$date = "$time/$precision" ;
	
	if ( $ref != '' ) { // Only add reference if there's a point to it
		$ref .= "\tS1810\t\"{$o->ext_name}\"" ; // {$catalogs[$o->catalog]->search_wp}:
	}
	
	if ( $ref == '' ) return ; // Just skip it without reference!
		

	$claims = $i->getClaims ( $prop ) ;
	if ( count($claims) == 0 ) {
		if ( $add_statement_if_no_date ) {
			$was_removed_before = $lc->hasPropertyEverBeenRemovedFromItem ( $q , $prop ) ;
			if ( !$was_removed_before and $year*1 > 1582 ) { # Gregorian filter
				$out = "$q\t$prop\t$date" ;
			}
		}
	} else {
		$found_equal = false ;
		$found_better = false ;
		foreach ( $claims AS $c ) {
			if ( !isset($c) or !isset($c->mainsnak) or !isset($c->mainsnak->datavalue) or !isset($c->mainsnak->datavalue->value) ) continue ;
			$v = $c->mainsnak->datavalue->value ;
			if ( $v->calendarmodel != 'http://www.wikidata.org/entity/Q1985727' ) { $found_better = true ; continue ; } # Wrong calendar, pretend better date exists; not touching this through QS
			if ( $v->precision*1 > $precision*1 ) { $found_better = true ; continue ; }
			if ( $v->time != $time or $v->precision*1 != $precision*1 ) {
				if ( preg_match ( '/^\D*(\d+)/' , $v->time , $n1 ) and preg_match ( '/^\D*(\d+)/' , $time , $n2 ) and $n1[1]*1!=$n2[1]*1 ) {
					$global_warning = true ;
					$mnm->addIssue ( $o->id , 'MISMATCH_DATES' , ['prop'=>$prop,'wd_time'=>$v->time,'mnm_time'=>$time,'q'=>$q] ) ;
				}
				return ;
			}
			$found_equal = true ;

			if ( isset($c->references) and count($c->references) > 0 ) { // Already has references, check if this one is among them
				$has_this_reference = false ;
				if ( !isset($catalogs[$o->catalog]->source_item) ) continue ; // This catalog doesn't have a source item, skip adding references
				$q_si = 'Q' . $catalogs[$o->catalog]->source_item ;
				foreach ( $c->references AS $reference ) {
				
					// "stated in"
					if ( isset($reference->snaks) and isset($reference->snaks->P248) ) {
						foreach ( $reference->snaks->P248 AS $claim ) {
							$target = $i->getTarget ( (object) [ 'mainsnak' => $claim ] ) ;
							if ( $target == $q_si ) $has_this_reference = true ;
						}
					}
					
					// "reference URL"
					if ( isset($reference->snaks) and isset($reference->snaks->P854) ) {
						foreach ( $reference->snaks->P854 AS $claim ) {
							if ( !isset($claim->datavalue) ) continue ;
							if ( !isset($claim->datavalue->value) ) continue ;
							if ( $claim->datavalue->value == $o->ext_url ) $has_this_reference = true ;
						}
					}
					
					
				}
#print $has_this_reference ? "yup\n" : "nope\n" ;
				if ( $has_this_reference ) {
					setAsInWikidata ( $o->entry_id ) ;
					continue ;
				}
			}

			# Found this very same date, but it does not have this reference yet (actual reference only!)
			if ( $add_references and $ref != '' ) {
				$out = "$q\t$prop\t$date" ;
				setAsInWikidata ( $o->entry_id ) ;
			}
		}
#print "$found_better\t$found_equal\n" ;
		if ( !$found_better and !$found_equal ) {
			# Did not find an equal or more precise date
			if ( $add_statement_if_different ) {
				$out = "$q\t$prop\t$date" ;
				if ( $year*1 < 1584 ) $out .= "\tP31\tQ26961029" ;
			}
		}
	}
	
	if ( $out == '' ) return ;
	$out .= $ref ;
	$out .= "\t/* Based on {$mnm->root_url}/#/entry/{$o->id} */" ;
	
	$global_out .= "$out\n" ;
}

function checkPropertyExists ( $o ) {
	global $mnm , $catalog_data , $lc , $global_out , $add_ext_id ;
	if ( !$add_ext_id ) return ;
	if ( !isset($catalog_data[$o->catalog]->data()->wd_prop) ) return ;
	if ( isset($catalog_data[$o->catalog]->data()->wd_qual) ) return ;
	if ( !isset($o->q) or $o->q === null or $o->q*1 <= 0 ) return ;
	if ( !isset($o->user) or $o->user == 0 ) return ;
	$q = "Q{$o->q}" ;
	$p = "P".$catalog_data[$o->catalog]->data()->wd_prop ;
	$ext_id = str_replace('"','',trim($mnm->fixStringForQS($o->ext_id))) ;
	if ( $ext_id == '' ) return ;

	$i = $mnm->wil->getItem ( $q ) ;
	if ( !isset($i) ) return ;
	if ( $i->hasClaims($p) ) return ;
	if ( $lc->hasPropertyEverBeenRemovedFromItem ( $q , $p ) ) return ;

	$command = "{$q}\t{$p}\t\"{$ext_id}\"" ;
	print "{$command}\n" ;
	$global_out .= "{$command}\n" ;
}


importSourceItems() ;

# Now check for dates
if ( isset($argv[1]) ) $catalog = $argv[1] * 1 ;
$items = [] ;
$sql = "SELECT * FROM entry,person_dates WHERE entry_id=entry.id AND in_wikidata=0 AND is_matched=1 AND q IS NOT NULL AND q!=0 AND user>0" ;
#$sql = "SELECT * FROM entry,person_dates WHERE entry_id=entry.id AND entry_id='74555135'" ; # TESTING
if ( count($skip_catalogs) > 0 ) $sql .= " AND catalog NOT IN (" . implode(',',$skip_catalogs) . ")" ;
$sql .= " AND catalog NOT IN (SELECT id FROM catalog WHERE active!=1)" ;
$catalogs_to_load = [] ;
if ( isset($catalog) ) {
	$sql .= " AND catalog=$catalog" ;
	$catalogs_to_load[] = $catalog ;
} else {
	$r = rand()/getrandmax() ;
	$sql .= " AND random>$r" ;
	$sql .= " ORDER BY random" ;
	$sql .= " LIMIT 50000" ;
}
$result = $mnm->getSQL($sql);
while($o = $result->fetch_object()) {
	$items['Q'.$o->q][] = $o ;
	$catalogs_to_load[$o->catalog] = $o->catalog ;
}

$catalog_data = [] ;
foreach ( $catalogs_to_load AS $c ) $catalog_data[$c] = $mnm->loadCatalog($c,true) ;

$items_chunked = array_chunk ( $items , 500 , true ) ;
unset ( $items ) ;
foreach ( $items_chunked AS $items ) {
	$mnm->wil = new WikidataItemList ; // Reset
	$mnm->wil->loadItems ( array_keys ( $items ) ) ;

	foreach ( $items AS $q => $entries ) {
		foreach ( $entries AS $o ) {
			$global_warning = false ;
			$global_out = '' ;
			if ( isset($catalog_data[$o->catalog]) ) checkPropertyExists($o);
			if ( preg_match ( '/^(\d+)/' , $o->born , $m1 ) and preg_match ( '/^(\d+)/' , $o->died , $m2 ) ) {
				$born = $m1[1]*1 ;
				$died = $m2[1]*1 ;
				if ( $died-$born < 10 ) { setAsInWikidata (  $o->entry_id) ; continue ; }
			}
			checkDateExists ( $q , $o , 'born' , 'P569' ) ;
			checkDateExists ( $q , $o , 'died' , 'P570' ) ;
			if ( $global_out == '' ) { setAsInWikidata (  $o->entry_id ) ; continue ; }
			if ( $global_warning ) { setAsInWikidata (  $o->entry_id ) ; continue ; }
			
			$qs = getQS() ;
			$tmp = $qs->importData ( $global_out , 'v1' ) ;
			$qs->runCommandArray ( $tmp['data']['commands'] ) ;
		}
	}
}

?>