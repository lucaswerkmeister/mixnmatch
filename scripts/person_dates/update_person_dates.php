#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/../vendor/autoload.php';

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);

if ( !isset($argv[1]) ) die ( "Requires catalog number\n" ) ;
$catalog = $argv[1] * 1 ;

$mnm = new MixNMatch\MixNMatch ;
$pd = new MixNMatch\PersonDates ( $catalog , $mnm ) ;
$pd->loadCodeFragment() ;
$pd->clearOldDates() ;

$values = [] ;
$sql = "SELECT id,ext_id,ext_name,ext_desc,q,user FROM entry WHERE catalog=$catalog AND `type`='Q5' AND " . $mnm->descriptionIsNotEmptySQL();
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$commands = $pd->processEntry ( $o ) ;
	foreach ( $commands AS $command ) {
		if ( $command->getAction() != MixNMatch\Command::SET or $command->getTarget() != MixNMatch\Command::PERSON_DATES ) continue ;
		$data = $command->getData() ;
		$entry_id = $command->getEntryID() ;
		$is_matched = $data->is_matched?1:0 ;
		$values[] = "({$entry_id},'{$data->born}','{$data->died}',{$is_matched})" ;
	}
}

if ( count($values) > 0 ) {
	$sql = "INSERT IGNORE INTO person_dates (entry_id,born,died,is_matched) VALUES " . implode ( ',' , $values ) ;
	$mnm->getSQL ( $sql ) ;
	$pd->updateHasPersonDates('yes') ;
	$pd->touchCodeFragment();
}

?>