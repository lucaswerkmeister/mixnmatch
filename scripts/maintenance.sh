#!/bin/bash

# Some obsolete thing I guess
#echo "connect s51434__mixnmatch_p; update catalog set type='biography' where type='unknown' AND active=1 AND (SELECT count(*) from entry WHERE catalog=catalog.id)=(SELECT count(*) from entry WHERE catalog=catalog.id AND type='person')" | sql local

# Ditto above
#echo "connect s51434__mixnmatch_p; update catalog SET type='art' WHERE type='unknown' AND url LIKE '%imdb%episodes'" | sql local

# Remove multi_match where there is a manual matcho
echo "connect s51434__mixnmatch_p; DELETE FROM multi_match WHERE EXISTS (SELECT * FROM entry WHERE entry_id=entry.id AND q IS NOT NULL AND user IS NOT NULL and user>0)" | sql local
