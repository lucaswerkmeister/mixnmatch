#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

$cache = [] ;
$mnm = new MixNMatch\MixNMatch ;
$dbwd = $mnm->tfc->openDB ( 'wikidata' , 'wikidata' , true , true ) ;

$sql = "SELECT * FROM entry WHERE q IS NOT NULL AND `type`='Q5'" ;
$sql .= " AND user IN (3,4)" ; # Auto-matching users only
# $sql .= " AND catalog=163" ; # Specific catalog only
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$q = 'Q' . $o->q ;
	$has_human_link = false ;
	if ( isset($cache[$q]) ) {
		$has_human_link = $cache[$q] ;
	} else {
		$sql = "SELECT * FROM page,pagelinks WHERE page_namespace=0 AND page_title='{$q}' AND pl_from=page_id AND pl_title='Q5' AND pl_namespace=0 LIMIT 1" ;
		$result2 = $mnm->tfc->getSQL ( $dbwd , $sql ) ;
		while ($x = $result2->fetch_object()) $has_human_link = true ;
		$cache[$q] = $has_human_link ;
	}
	if ( $has_human_link ) continue ;

	$mnm->removeMatchForEntryID ( $o->id ,  2 ) ; # It me
#	print "https://www.wikidata.org/wiki/{$q} is not human, but linked to by:\n" ;
#	print_r ( $o ) ;
}

?>