#!/usr/bin/php
<?PHP

ini_set('memory_limit','3000M');

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$catalog = ($argv[1] ?? 0)*1 ;
$testing = ($argv[2] ?? 0)*1 == 1 ;

$aux = new MixNMatch\Auxiliary ( $testing ) ;
$aux->addAuxiliaryToWikidata ( $catalog ) ;

?>
