#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';



if ( !isset($argv[1]) ) die ("Requires catalog ID");

$mnm = new MixNMatch\MixNMatch ;
$catalog_id = $argv[1] * 1 ;
$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
$ext_ids = $catalog->get_all_external_ids() ;

if ( $catalog_id == 4681 ) {
	$url = "https://www.photolondon.org.uk/api/public/people/a/0/1000" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	foreach ( $j->people AS $p ) {
		if ( isset($ext_ids[$p->id]) ) continue ;

		$o = (object) [
			'catalog' => $catalog_id ,
			'id' => $p->id ,
			'name' => $p->p_forenames.' '.$p->p_lastname ,
			'desc' => $p->p_briefBio ,
			'url' => "https://www.photolondon.org.uk/#/details?id=".$p->id,
			'type' => 'Q5' ,
			'aux' => [["P106","Q33231"]]
		];
		if ( $p->p_gender=='male' ) $o->aux[] = ["P21","Q6581097"] ;
		if ( $p->p_gender=='female' ) $o->aux[] = ["P21","Q6581072"] ;

		$born = $p->p_birthYearOnly ;
		$died = $p->p_deathYearOnly ;
		if ( $born == '0' ) $born = '' ;
		if ( $died == '0' ) $died = '' ;
		$entry_id = $mnm->addNewEntry($o);
		if ( $born.$died != '' ) $mnm->setPersonDates ( $entry_id , $born , $died ) ;
		$ext_ids[$o->id] = 1 ;
	}
}

if ( $catalog_id == 4679 ) {
	for ( $page = 1 ; $page <= 12 ; $page++ ) {
		$url = "https://www.dccomics.com/proxy/search?type=generic_character&page={$page}&sortBy=title-ASC" ;
		$text = file_get_contents ( $url ) ;
		$j = json_decode ( $text ) ;
		foreach ( $j->results as $c ) {
			$id = preg_replace('|^.*/|','',$c->fields->search_api_url) ;
			if ( isset($ext_ids[$id]) ) continue ;
			$o = (object) [
				'catalog' => $catalog_id ,
				'id' => $id ,
				'name' => $c->fields->dc_solr_sortable_title ,
				'desc' => strip_tags($c->fields->{'body:value'}[0]) ,
				'url' => "https://www.dccomics.com".$c->fields->search_api_url,
				'type' => 'Q15632617' ,
				'aux' => [['P31','Q1114461'],['P1080','Q1152150']]
			];
			$entry_id = $mnm->addNewEntry($o);
			$ext_ids[$id] = 1 ;
		}
	}
}

if ( $catalog_id == 4600 ) {
	$url = 'https://en.foto-ch.ch/api/?a=streamsearch&type=photographer&limit=25000&offset=0&lang=en' ;
	$text = file_get_contents ( $url ) ;

	# At this moment,URL outputs the same JSON twice, fix this
	$parts = explode ( '}{' , $text ) ;
	if ( count($parts) > 1 ) $text = $parts[0] . '}' ;

	$j = json_decode ( $text ) ;
	foreach ( $j->photographer_results AS $v ) {
		if ( isset($ext_ids[$v->id]) ) continue ;
		$d = [] ;
		$born = '' ;
		$died = '' ;
		if ( $v->gen_geburtsdatum*1 == 0 and substr($v->geburtsdatum,0,1)!='0' ) $born = $v->geburtsdatum ;
		if ( $v->gen_geburtsdatum*1 == 1 and substr($v->geburtsdatum,0,1)!='0' ) $born = substr($v->geburtsdatum,0,4) ;
		if ( $v->gen_todesdatum*1 == 0 and substr($v->todesdatum,0,1)!='0'  ) $died = $v->todesdatum ;
		if ( $v->gen_todesdatum*1 == 1 and substr($v->todesdatum,0,1)!='0'  ) $died = substr($v->todesdatum,0,4) ;
		if ( $born.$died != '' ) $d[] = "{$born} - {$died}" ;
		foreach ( ['namenszusatz','titel','fotografengattungen_set','arbeitsorte'] AS $k ) {
			if ( isset($v->$k) and $v->$k != '' ) $d[] = "{$k}: {$v->$k}" ;
		}
		$o = (object) [
			'catalog' => $catalog_id ,
			'id' => $v->id ,
			'name' => "{$v->vorname} {$v->nachname}" ,
			'desc' => implode(' | ', $d ) ,
			'url' => "https://en.foto-ch.ch/photographer?detail={$v->id}&type=photographer",
			'type' => 'Q5' ,
			'aux' => [['P106','Q33231']]
		];
		if ( $v->pnd!='' ) $o->aux = ['P227',$v->pnd] ;
		$entry_id = $mnm->addNewEntry($o);
		$ext_ids[$v->id] = $entry_id ;
		if ( $born.$died != '' ) $mnm->setPersonDates ( $entry_id , $born , $died ) ;
	}
}

if ( $catalog_id == 4589 ) {
	foreach (range('A', 'Z') as $char) {
		$url = "https://www.classicalarchives.com/api/composer_list_all.json?letter={$char}" ;
		$json = json_decode(file_get_contents($url));
		if ( !isset($json) or $json==null ) continue ;
		foreach ( $json AS $v ) {
			if ( isset($ext_ids[$v->id]) ) continue ;
			$d = [] ;
			if ( isset($v->d) ) $d[] = $v->d ;
			if ( isset($v->nat) ) $d[] = $v->nat ;
			$o = (object) [
				'catalog' => $catalog_id ,
				'id' => $v->id ,
				'name' => preg_replace ( '|^(.+?), (.+)$|' , '$2 $1' , $v->n ) ,
				'desc' => implode(' | ', $d ) ,
				'url' => "https://www.classicalarchives.com/newca/#!/Composer/{$v->id}",
				'type' => 'Q5' ,
				'aux' => [ ['P106','Q36834'] ] # Occupation:composer
			];
			$ext_ids[$v->id] = $mnm->addNewEntry($o);
		}
	}
}

if ( $catalog_id == 4361 ) {
	foreach (range('A', 'Z') as $char) {
		$url = "https://api.woerterbuchnetz.de/dictionaries/Meyers/lemmata/lemid/{$char}00000/100000/json" ;
		$json = json_decode(file_get_contents($url));
		if ( !isset($json) or $json==null ) continue ;
		foreach ( $json AS $v ) {
			$id = $v->lemid ;
			if ( isset($ext_ids[$id]) ) continue ;
			$name = html_entity_decode($v->lemma, ENT_COMPAT, 'UTF-8') ;
			$o = (object) [
				'catalog' => $catalog_id ,
				'id' => $id ,
				'name' => $name ,
				'url' => "https://www.woerterbuchnetz.de/Meyers?lemid={$id}"
			];
			$ext_ids[$id] = $mnm->addNewEntry($o);
		}
	}
}

if ( $catalog_id == 4098 ) {
	# Update descriptions and aux data ONLY
	$ext_ids = [] ;
	$sql = "SELECT `id`,`ext_id` FROM `entry` WHERE `catalog`={$catalog_id} AND ext_desc=''" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $ext_ids[$o->ext_id] = $o->id ;
	foreach ( $ext_ids AS $ext_id => $entry_id ) {
		$url = "http://studium.univ-paris1.fr/api/prosopography/{$ext_id}" ;
		$j = json_decode(file_get_contents($url)) ;
		$born = '' ;
		$died = '' ;
		$aux = [] ;
		$desc = [] ;
		foreach ( $j->identity->shortDescription??[] AS $v ) $desc[] = $v->value ;
		foreach ( $j->identity->datesOfLife??[] AS $v ) {
			$desc[] = trim(preg_replace ( '/[\\\\%]/' , '' , $v->value )) ;
			foreach ( $v->meta->dates??[] AS $date ) {
				$born = $date->startDate->date??'' ;
				$died = $date->endDate->date??'' ;
			}
		}
		foreach ( $j->origin->birthPlace??[] AS $bp ) {
			$desc[] = "Born in {$bp->value}" ;
		}
		foreach ( $j->identity->gender??[] AS $v ) {
			$desc[] = "gender: {$v->value}" ;
			if ( $v->value == 'male' ) $aux[] = ['P21','Q6581097'] ;
			if ( $v->value == 'female' ) $aux[] = ['P21','Q6581072'] ;
		}
		$desc = trim ( implode ( '; ' , $desc ) ) ;
		if ( $desc!='' ) $mnm->setDescriptionForEntryID ( $entry_id , $desc ) ;
		if ( $born.$died != '' ) $mnm->setPersonDates ( $entry_id , $born , $died ) ;
		foreach ( $aux AS $a ) $mnm->setAux ( $entry_id , $a[0] , $a[1] ) ;
	}
}

if ( $catalog_id == 4097 ) {
	foreach (range('A', 'Z') as $letter) {
		$url = "https://biblio.hiu.cas.cz/api/search?exports=portaroSearchItemParagraph,portaroSearchItemMoreParagraph,portaroSearchItemAuthorityParagraph&fond=31&kind=document&kind=authority&pageNumber=1&pageSize=20000&prefix={$letter}&recordRelatedRecordFond=1&recordRelatedRecordFond=7&recordRelatedRecordFond=8&recordRelatedRecordFond=5&recordRelatedRecordFond=9&recordRelatedRecordFond=10&recordRelatedRecordFond=6&recordRelatedRecordFond=12&recordRelatedRecordFond=13&recordRelatedRecordFond=14&recordRelatedRecordFond=15&recordRelatedRecordFond=16&recordRelatedRecordFond=19&recordRelatedRecordFond=3&recordRelatedRecordFond=4&recordRelatedRecordFond=31&recordRelatedRecordFond=45&recordRelatedRecordFond=52&recordRelatedRecordFond=56&recordRelatedRecordFond=55&recordRelatedRecordFond=58&sorting=PNAZEV&type=authority-index&format=json" ;
		$json = json_decode(file_get_contents($url)) ;
		foreach ( $json->result->content AS $c ) {
			$o = (object) [
				'id' => $c->recordUuid,
				'name' => $c->name,
				'desc' => '',
				'url' => "https://biblio.hiu.cas.cz/records/{$c->recordUuid}",
				'catalog' => $catalog_id ,
				'aux' => [ ['P6656',$c->id] ] ,
				'type' => 'Q5'
			] ;
			if ( isset($ext_ids[$o->id]) ) continue ; # Had that
			$ext_ids[$o->id] = $o->id ;
			if ( preg_match('|(.+?), (\d{3}.+)$|',$o->name,$m) ) {
				$o->name = trim($m[1]) ;
				$o->desc = trim($m[2]) ;
			}
			if ( preg_match('|(.+), (.+)$|',$o->name,$m) ) {
				$o->name = trim("{$m[2]} {$m[1]}") ;
			}
			$mnm->addNewEntry($o);
		}
	}
}

if ( $catalog_id == 3862 ) {
	$url = 'https://lod-cloud.net/lod-data.json' ;
	$json = json_decode(file_get_contents($url)) ;
	foreach ( $json AS $id => $v ) {
		$o = (object) [
			'id' => $id,
			'name' => $v->title ?? $id,
			'desc' => $v->description->en ?? $v->description->de ?? '',
			'url' => "https://lod-cloud.net/dataset/{$id}",
			'catalog' => $catalog_id
		] ;
		if ( isset($ext_ids[$o->id]) ) continue ; # Had that
		$ext_ids[$o->id] = $o->id ;
		$mnm->addNewEntry($o);
	}
}

if ( $catalog_id == 3386 ) {
	$zero = '0' ;
	$one = '1' ;
	$two = '2' ;
	$underscore = '_' ;
	$url = 'https://www.geschichtsquellen.de/autor.json?item_id=0' ;
	$json = json_decode(file_get_contents($url)) ;
	foreach ( $json->data AS $x ) {
		$o = (object) [ 'type' => 'Q5' , 'catalog' => $catalog_id ] ;
		if ( isset($x->$zero->$underscore) ) {
			if ( preg_match ( '|<a href="/autor/(\d+)">(.+?)</a>|' , $x->$zero->$underscore , $m ) ) {
				$o->id = $m[1] ;
				$o->name = $m[2] ;
				$o->url = "https://www.geschichtsquellen.de/autor/{$o->id}" ;
				$o->name = preg_replace ( '|^(.+), (.+)$|' , '$2 $1' , $o->name ) ;
			} else continue ;
		}  else continue ;
		if ( isset($x->$one->$underscore) and $x->$one->$underscore != '' ) {
			$o->desc = $o->name ;
			$o->name = $x->$one->$underscore ;
		}
		if ( isset($x->$two) and $x->$two != '' ) {
			$o->aux = [ [ 'P227' , "{$x->$two}" ] ] ;
		}
		if ( isset($ext_ids[$o->id]) ) continue ; # Had that
		$entry_id = $mnm->addNewEntry($o);
		$ext_ids[$o->id] = $entry_id ;
		if ( isset($o->desc) ) {
			$mnm->setAlias ( $entry_id , $o->desc ) ;
		}
	}
}

if ( $catalog_id == 3387 ) {
	# Get authors, see above
	$author2entry = [] ;
	$sql = 'SELECT id,ext_id FROM entry WHERE catalog=3386' ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $author2entry[$o->ext_id] = $o->id ;

	$zero = '0' ;
	$one = '1' ;
	$two = '2' ;
	$three = '3' ;
	$underscore = '_' ;
	$url = 'https://www.geschichtsquellen.de/werk.json?item_id=0' ;
	$json = json_decode(file_get_contents($url)) ;
	foreach ( $json->data AS $x ) {
		$o = (object) [ 'type' => 'Q47461344' , 'catalog' => $catalog_id ] ;
		if ( isset($x->$zero->$underscore) ) {
			if ( preg_match ( '|<a href="/werk/(\d+)">(.+?)</a>|' , $x->$zero->$underscore , $m ) ) {
				$o->id = $m[1] ;
				$o->name = $m[2] ;
				$o->url = "https://www.geschichtsquellen.de/werk/{$o->id}" ;
			} else continue ;
		}  else continue ;
		if ( isset($x->$one->$underscore) and $x->$one->$underscore != '' ) {
			$o->desc = $o->name ;
			$o->name = $x->$one->$underscore ;
		}
		if ( isset($x->$two->$underscore) and $x->$two->$underscore != '' ) {
			$o->desc .= ' | ' . $x->$two->$underscore ;
		}
		$author_entry_id = 0 ;
		if ( isset($x->$three->$underscore) ) {
			if ( preg_match ( '|<a href="/autor/(\d+)">.+?</a>|' , $x->$three->$underscore , $m ) ) {
				$author_id = $m[1] ;
				if ( isset($author2entry[$author_id]) ) $author_entry_id = $author2entry[$author_id] ;
			}
		}
		if ( isset($ext_ids[$o->id]) ) continue ; # Had that
		$entry_id = $mnm->addNewEntry($o);
		$ext_ids[$o->id] = $entry_id ;
		if ( $author_entry_id != 0 ) $mnm->linkEntriesViaProperty ( $entry_id , 'P50' , $author_entry_id ) ;
	}
}


if ( $catalog_id == 2849 ) {
	$url = 'https://nordicwomensliterature.net/wp-json/nwl/v1/writers/en' ;
	$json = json_decode(file_get_contents($url)) ;
	foreach ( $json AS $writer ) {
		if (!preg_match('|/([^/]+)/$|',$writer->profile_url,$m)) continue ;
		$id = $m[1] ;
		$name = $writer->name ;
		$name = preg_replace('|^(.+?), (.+)$|','$2 $1',$name);
		$desc = '' ;
		if ( isset($writer->country) ) $desc = $writer->country ;
		$o = (object) [
			'catalog' => $catalog_id,
			'id' => $id,
			'name' => $name,
			'url' => $writer->profile_url,
			'desc' => $desc ,
			'type' => 'Q5'
		];
		if ( isset($ext_ids[$o->id]) ) continue ; # Had that
		$entry_id = $mnm->addNewEntry($o);
		$ext_ids[$o->id] = $entry_id ;
		$mnm->setPersonDates($entry_id,$writer->born,$writer->dead);
	}
	# All female
	$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) SELECT id,21,'Q6581072' FROM entry WHERE catalog={$catalog_id}" ;
	$mnm->getSQL($sql);

	# All writers
	$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) SELECT id,106,'Q36180' FROM entry WHERE catalog={$catalog_id}" ;
	$mnm->getSQL($sql);
}

if ( $catalog_id == 2964 ) { # Pantheon
	$data = [
		[ 'person' , 'Q5' ] ,
		[ 'place' , 'Q2221906' ] ,
		[ 'country' , 'Q6256' ] ,
		[ 'occupation' , 'Q28640' ] ,
		[ 'era' , 'Q11514315' ] ,
	] ;
	foreach ( $data AS $d ) {
		$key = $d[0] ;
		$url = 'https://api.pantheon.world/'.$key ;
		$type = $d[1] ;
		$key_slug = "{$key}_slug" ;
		$json = json_decode(file_get_contents($url)) ;
		foreach ( $json AS $entry ) {
			if ( isset($entry->slug) ) $slug = $entry->slug ;
			else if ( isset($entry->$key_slug) ) $slug = $entry->$key_slug ;
			else die ( "No slug for {$key}\n" ) ;
			if ( isset($entry->name) ) $name = $entry->name ;
			else if ( isset($entry->$key) ) $name = $entry->$key ;
			#else if ( isset($entry->id) ) $name = $entry->id ;
			else die ( "No name for {$key}\n" ) ;
			$desc = [] ;
			foreach ( ['id','description','start_year','end_year','industry','domain','group','occupation','birthdate','birthyear','bplace_name','deathdate','deathyear','dplace_name'] AS $k ) {
				if ( isset($entry->$k) and $entry->$k!==null and $entry->$k!='' ) $desc[] = "{$k}: {$entry->$k}" ;
			}
			$o = [
				'catalog' => $catalog_id ,
				'name' => $name ,
				'id' => $slug ,
				'type' => $type ,
				'desc' => implode('; ',$desc),
				'url' => "https://pantheon.world/profile/{$key}/{$slug}" ,
			] ;
			if ( isset($entry->wp_id) and preg_match('/^Q\d+$/i',$entry->wp_id) ) $o->q = strtoupper($entry->wp_id);
			if ( isset($ext_ids[$o->id]) ) continue ; # Had that
			$entry_id = $mnm->addNewEntry($o);
			$ext_ids[$o->id] = $entry_id ;
			if ( isset($entry->lat) and isset($entry->lon) and $entry->lat!==null and $entry->lon!==null ) {
				$mnm->setLocation($entry_id,$entry->lat,$entry->lon);
			}
		}
	}

}

$catalog->updateStatistics() ;

$job_id = $mnm->queue_job($catalog_id,'update_person_dates');
$mnm->queue_job($catalog_id,'match_person_dates',$job_id);
$mnm->queue_job($catalog_id,'automatch_by_search');
$mnm->queue_job($catalog_id,'automatch_from_other_catalogs');

?>