#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once ( '/data/project/mix-n-match/manual_lists/large_catalogs/shared.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

function search_for_person_name ( $name ) {
	global $mnm ;
	$query = $mnm->sanitizePersonName ( $name ) ;
	$query .= ' haswbstatement:P31=Q5' ;
	$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	return array_map(function($value) { return $value->title;} , $j->query->search);
}

function query_for_items_with_dates ( $year_born , $year_died ) {
	global $mnm ;
	$year_born2 = $year_born*1 + 1 ;
	$year_died2 = $year_died*1 + 1 ;
	$sparql = "SELECT ?q WHERE {
		?q wdt:P569 ?dob. hint:Prior hint:rangeSafe true.
		FILTER(\"{$year_born}-00-00\"^^xsd:dateTime <= ?dob && ?dob < \"{$year_born2}-00-00\"^^xsd:dateTime)
		?q wdt:P570 ?dod. hint:Prior hint:rangeSafe true.
		FILTER(\"{$year_died}-00-00\"^^xsd:dateTime <= ?dod && ?dod < \"{$year_died2}-00-00\"^^xsd:dateTime)
		}" ;
	return $mnm->tfc->getSPARQLitems($sparql,"q") ;
}

function generate_quickstatement_commands($entries) {
	global $mnm , $lc , $verbose ;
	$all_commands = [] ;
	foreach ( $entries AS $num => $entry ) {
		try {
			$commands = $mnm->getCreateItemForEntryCommands ( $entry , $lc ) ;
		} catch ( Exception $ex ) {
			if ( $verbose ) print "getCreateItemForEntryCommands raised exception for https://mix-n-match.toolforge.org/#/entry/{$entry->id}\n" ;
			return ;
		}
		if ( !isset($commands) ) {
			if ( $verbose ) print "Issue with creating item for https://mix-n-match.toolforge.org/#/entry/{$entry->id}\n" ;
			return ;
		}
		array_shift ( $commands ) ; # Remove CREATE
		foreach ( $commands AS $k => $v ) {
			if ( $num > 0 and preg_match ( '/^LAST\t[LADS]/' , $v ) ) continue ; // Labels etc. from first entry only
			$key = $v ;
			if ( preg_match ( '/^(LAST\t.+?\t.+?)(\t.+)$/' , $v , $m ) ) {
				$key = $m[1] ;
				if ( isset($all_commands[$key]) ) $v = $all_commands[$key] . $m[2] ;
			}
			$all_commands[$key] = $v ;
		}
	}
	$all_commands = array_unique ( $all_commands ) ;
	array_unshift ( $all_commands , 'CREATE' ) ; # Prefix CREATE

	# Filter same name/description
	$label_in_lang = [] ;
	foreach ( $all_commands AS $command ) {
		$c = explode ( "\t" , $command ) ;
		if ( preg_match('|^L(.+)$|',$c[1]??'',$m) ) $label_in_lang[$m[1]] = $c[2] ;
	}
	$tmp = $all_commands ;
	$all_commands = [] ;
	foreach ( $tmp AS $command ) {
		$c = explode ( "\t" , $command ) ;
		if ( preg_match('|^D(.+)$|',$c[1]??'',$m) ) {
			if ( $label_in_lang[$m[1]]??'' == $c[2] ) continue ;
		}
		$all_commands[] = $command ;
	}
	return $all_commands ;
}

function match_all_entries_to_existing_item($entries,$q,$full_label) {
	global $mnm , $testing , $verbose ;
	$q = $mnm->sanitizeQ($q);
	#if ( preg_match)
	if ( !isset($q) or $q=='Q' or $q=='Q0' or $q=='0' or $q=='' or $q==null or !is_string($q) ) {
		if ( $verbose ) print "Invalid item to match to: {$q}\n" ;
		return ;
	}
	if ( $verbose ) print "Matching entries for {$full_label} to https://www.wikidata.org/wiki/{$q}\n" ;
	foreach ( $entries AS $entry ) {
		if ( $verbose ) print "- Matching entry https://mix-n-match.toolforge.org/#/entry/{$entry->id}\n" ;
		if ( !$testing ) $mnm->setMatchForEntryID($entry->id,$q,3,true,false);
	}
}

function label_born_died_exists($label,$year_born,$year_died,$entries,$full_label) {
	global $mnm , $verbose ;
	$search_items = search_for_person_name($label)??[];
	$query_items = query_for_items_with_dates($year_born,$year_died)??[];
	$intersection = array_intersect($search_items, $query_items) ;
	if ( !is_array($intersection) ) return true ; # Weird flex but OK...
	if (count($intersection) > 0) {
		if ( count($intersection) == 1 ) match_all_entries_to_existing_item($entries,$intersection[0],$full_label);
		else if ( $verbose ) print "Item(s) for {$full_label} exist: ".implode(',',$intersection)."\n" ;
		return true ;
	}
	return false ;
}

function create_new_item_and_match_entries($all_commands,$entries,$full_label) {
	global $mnm , $testing , $verbose ;
	if ( $testing ) $q = null ;
	else $q = $mnm->tfc->runCommandsQS ( $all_commands ) ;
	if ( isset($q) ) {
		$mnm->fix_references_in_item($q);
		if ( $verbose ) print "Created https://www.wikidata.org/wiki/{$q} for {$full_label}\n" ;
		foreach ( $entries AS $num => $entry ) {
			$mnm->setMatchForEntryID ( $entry->id , $q , 3 , true , false ) ;
			if ( $verbose ) print "- https://mix-n-match.toolforge.org/#/entry/{$entry->id}\n" ;
		}
	} else if ( $testing ) {
		if ( $verbose ) print "TESTING -- SKIPPING CREATION FOR {$full_label}\n" ;
	} else {
		if ( $verbose ) print "FAILED to CREATE NEW ITEM FOR {$full_label}\n" ;
	}
}


function process_entry_group($entry_ids,$year_born,$year_died) {
	global $mnm , $lc , $min_count_create , $verbose , $testing ;
	$matched = [] ;
	$auto = [] ;
	$unmatched = [] ;
	$label = "" ;
	$sql = "SELECT * FROM entry WHERE id IN (".join(',',$entry_ids).")" ;
	$result = $mnm->getSQL ( $sql ) ;
	while ( $o = $result->fetch_object() ) {
		$label = $o->ext_name ;
		if ( $o->q == null ) $unmatched[] = $o ;
		else if ( $o->user == 0 ) $auto[] = $o ;
		else if ( $o->q >= 0 ) $matched[] = $o ;
	}
	$auto_or_unmatched = array_merge($auto,$unmatched) ;
	$full_label = "{$label} ({$year_born}-{$year_died})" ;
	#if ( $verbose ) print "Matched: ".count($matched)." | Auto: ".count($auto)." | Unmatched: ".count($unmatched)." | {$full_label}\n" ;

	# At least one already matched entry
	if ( count($matched) > 0 ) {
		# Paranoia
		$qs = array_unique(array_map(function($entry) { return $entry->q; } , $matched));
		if ( count($qs) == 1 ) match_all_entries_to_existing_item($auto_or_unmatched,$qs[0],$full_label);
		else if ( $verbose ) {
			print "More than two entries for {$full_label}, not touching this\n" ;
			print_r($matched);
		}
		return ;
	}

	# No already matched entries
	if ( count($auto_or_unmatched) < $min_count_create ) return ;

	# Make sure no such entry on Wikidata
	if ( label_born_died_exists($label,$year_born,$year_died,$auto_or_unmatched,$full_label) ) return ;

	# Generate QS commands
	$all_commands = generate_quickstatement_commands($auto_or_unmatched);
	if (!isset($all_commands) ) return ;

	# Generate and run QS
	create_new_item_and_match_entries($all_commands,$auto_or_unmatched,$full_label);
}


# ____________________________________________________________________________________________________


$mnm = new MixNMatch\MixNMatch ;
$mnm->tfc->getQS('mix-n-match');
$lc = new largeCatalog ( 2 ) ;
$min_count_create = 3 ;

if ( !isset($argv[1]) ) die ("USAGE: {$argv[0]} CATALOG_ID\n" ) ;
$catalog_id = $argv[1]*1 ;
if ( $catalog_id == 0 ) { # Random catalog with person dates
	$sql = "SELECT * FROM catalog WHERE active=1 AND has_person_date='yes' ORDER BY rand() LIMIT 1" ;
	$result = $mnm->getSQL ( $sql ) ;
	$o = $result->fetch_object();
	$catalog_id = $o->id ;
	print "Using catalog #{$catalog_id}: {$o->name}\n" ;
}
$testing = isset($argv[2]) ;
$verbose = $testing ;

$sql = "SELECT * FROM catalog WHERE id={$catalog_id}" ;
$result = $mnm->getSQL ( $sql ) ;
if ( $o = $result->fetch_object() ) {
	if ( $o->active != 1 ) die ( "Catalog {$catalog_id} is not active\n" ) ;
} else die ( "Catalog {$catalog_id} does not exist\n" ) ;

$sql = "select group_concat(DISTINCT v1.entry_id) as entry_ids,v1.year_born,v1.year_died,count(DISTINCT v1.catalog) AS cnt from vw_dates v2,vw_dates v1
WHERE v2.catalog={$catalog_id} AND (v2.q is null or v2.user=0) AND v2.year_born!='' AND v2.year_died!=''
AND v1.year_born=v2.year_born
AND v1.year_died=v2.year_died
AND v1.ext_name=v2.ext_name
AND v1.catalog in (SELECT id FROM catalog WHERE active=1)
GROUP BY v1.ext_name,v1.year_born,v1.year_died
HAVING cnt>=2
ORDER BY cnt DESC" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	$entry_ids = explode ( "," , $o->entry_ids ) ;
	process_entry_group($entry_ids,$o->year_born,$o->year_died);
}

?>