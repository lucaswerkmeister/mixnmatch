#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

if ( isset($argv[1]) ) $catalog = $argv[1]*1 ;
else die ( "Needs catalog parameter\n" ) ;

$sql = "SELECT * FROM vw_words WHERE catalog={$catalog} AND json IS NOT NULL AND skip=0" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$json_array = json_decode ( $o->json ) ;
	if ( !is_array($json_array) ) continue ;
	foreach ( $json_array AS $j ) {
		$p = preg_replace ( '/\D/' , '' , $j->prop ) * 1 ;
		if ( $o->type == 'Q5' and $p == 106 ) {
			$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) VALUES ({$o->entry_id},{$p},'" . $mnm->escape($j->q) . "')" ;
			$mnm->getSQL ( $sql ) ;
		}
	}
}

?>