#!/usr/bin/php
<?PHP

# This script is run daily
# It updates the all_people_bd table (q,year born, year dead) from SPARQL
# That data is used in match_person_entries_by_name_and_dates.php

require_once dirname(__DIR__) . '/vendor/autoload.php';

$basedir = '/data/scratch/mix_n_match' ; #'/tmp' ;
$tmp_sql_file = "$basedir/person_dates.sql" ;
$tmp_json_file = "$basedir/person_dates.json" ;
if ( file_exists($tmp_json_file) ) unlink ( $tmp_json_file ) ;
$cmd = "curl -s -g -o {$tmp_json_file} 'https://query.wikidata.org/sparql?format=json&query=SELECT%20%3Fq%20%28YEAR%28%3Fborn%29%20AS%20%3Fb%29%20%28YEAR%28%3Fdied%29%20AS%20%3Fd%29%20%7B%20%3Fq%20wdt%3AP31%20wd%3AQ5%20%3B%20wdt%3AP569%20%3Fborn%20%3B%20wdt%3AP570%20%3Fdied%20%7D'" ;
exec ( $cmd ) ;
if ( !file_exists($tmp_json_file) ) die ( "File {$tmp_json_file} was not created\n" ) ;


function output ( $s ) {
	global $out_fh , $out_cnt ;
	if ( $out_cnt == 0 ) fwrite ( $out_fh , "INSERT INTO all_people_bd (q,born,died) VALUES " ) ;
	else $s = ",$s" ;
	fwrite ( $out_fh , $s ) ;
	$out_cnt++ ;
	if ( $out_cnt < 3 ) return ;
	$out_cnt = 0 ;
	fwrite ( $out_fh , ";\n" ) ;
}

function flushCache ( &$cache ) {
	if ( isset($cache['q']) and isset($cache['b']) and isset($cache['d']) and $cache['b'] < $cache['d'] ) {
		output ( "(" . substr($cache['q'],1) . "," . $cache['b'] . "," . $cache['d'] . ")" ) ;
	}
	$cache = [] ;
}

if ( file_exists($tmp_sql_file) ) unlink ( $tmp_sql_file ) ;
$out_fh = fopen ( $tmp_sql_file , 'w' ) ;
fwrite ( $out_fh , "CONNECT s51434__mixnmatch_p;\n" ) ;
fwrite ( $out_fh , "TRUNCATE all_people_bd;\n" ) ;
$out_cnt = 0 ;

$last_var = '' ;
$cache = [] ;

$handle = @fopen($tmp_json_file, "r");
while (($line = fgets($handle)) !== false) {
	if ( preg_match ( '/^\s+"(.)" : \{/' , $line , $m ) ) {
		$last_var = $m[1] ;
		if ( $last_var == 'q' ) flushCache ( $cache ) ;
		continue ;
	}
	if ( preg_match ( '/^\s+"value" : "(.+?)"/' , $line , $m ) ) {
		$v = $m[1] ;
		if ( $v == '' ) continue ;
		if ( $last_var == 'q' ) $v = preg_replace ( '/^.+Q/' , 'Q' , $v ) ;
		$cache[$last_var] = $v ;
	}
}
fclose ( $handle ) ;
fclose ( $out_fh ) ;

exec ( "cat {$tmp_sql_file} | sql local" ) ;

?>