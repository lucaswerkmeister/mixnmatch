#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

$property_blacklist = [17,18,19,27,50,106,131,170,2528,233] ;

if ( isset($argv[1]) ) {
	$catalog = $argv[1]*1 ;
	if ( $catalog == 0 ) die("Either no parameter (all catalogs), or catalog ID as parameter");
}

$mnm = new MixNMatch\MixNMatch ;

$sql = "SELECT e1.id,e1.catalog,e2.q FROM entry e1, entry e2,auxiliary,catalog
WHERE entry_id=e1.id
AND e1.catalog IN (SELECT id FROM catalog WHERE active=1)
AND (e1.q IS NULL OR e1.user=0)
AND catalog.id=e2.catalog AND catalog.active=1
AND wd_prop>0 AND wd_prop=aux_p AND wd_qual IS NULL
AND e2.ext_id=aux_name
AND e2.q>0 AND e2.q IS NOT NULL AND e2.user>0" ;
if ( count($property_blacklist) > 0 ) $sql .= " AND `aux_p` NOT IN (" . implode(',',$property_blacklist) . ")" ;
if ( isset($catalog) ) $sql .= " AND catalog.id={$catalog}";
$sql .= " LIMIT 5000";

$catalogs = [] ;
$id2q = [] ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$id2q["{$o->id}"] = $o->q ;
	$catalogs["{$o->catalog}"] = true ;
}

$ts = $mnm->getCurrentTimestamp();

$log_line = "{$ts}\n" ;
$log_file = '/data/project/mix-n-match/internal_auxiliary_matcher.log' ;
file_put_contents ( $log_file , $log_line , FILE_APPEND ) ;

foreach ( $id2q AS $entry_id => $q ) {
	$mnm->setMatchForEntryID ( $entry_id , $q , 4 , true , false ) ;
}

foreach ( $catalogs AS $cid => $dummy ) {
	$mnm->queue_job($cid,'microsync');
}

?>