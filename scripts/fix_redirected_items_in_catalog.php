#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1]) ) die("USAGE: {$argv[0]} [CATALOG_ID|ALL]\n");

function get_matched_items_in_catalog($catalog,$mnm) {
	$sql = "SELECT DISTINCT `q` FROM `entry` WHERE `catalog`={$catalog} AND q IS NOT NULL AND q>0 AND user>0" ;
	$result = $mnm->getSQL ( $sql ) ;
	$qs = [] ;
	while($o = $result->fetch_object()) $qs[] = "Q{$o->q}" ;
	return $qs ;
}

function fix_redirects($catalog,$qs,$mnm) {
	if ( count($qs) == 0 ) return;

	$chunks = array_chunk ( $qs , 5000 ) ;
	foreach ( $chunks as $chunk ) {
		$redirected_items = [] ; # page_id => Qxxx
		$sql = "SELECT page_id,page_title FROM page WHERE page_namespace=0 AND page_is_redirect=1 AND page_title IN ('".implode("','",$chunk)."')" ;
		$dbwd = $mnm->openWikidataDB() ;
		$result = $mnm->tfc->getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()) $redirected_items[$o->page_id] = $o->page_title ;
		if ( count($redirected_items) == 0 ) continue;

		$q2q = $mnm->getRedirectTargets($redirected_items);
		if ( count($q2q) == 0 ) continue ;

		unset($redirected_items);
		foreach ( $q2q AS $q_from => $q_to ) {
			if ( !preg_match('|^Q(\d+)$|',$q_to,$m) ) continue ;
			$q_to = $m[1] ;
			$q_from = preg_replace ( '/\D/' , '' , "{$q_from}" ) ;
			$sql = "UPDATE `entry` SET `q`={$q_to} WHERE `catalog`={$catalog} AND `q`={$q_from}" ;
			$mnm->getSQL($sql);
		}
	}
}

function fix_deleted($catalog,$qs,$mnm) {
	$chunks = array_chunk ( $qs , 5000 ) ;
	foreach ( $chunks as $chunk ) {
		$sql = "SELECT page_title FROM page WHERE page_namespace=0 AND page_title IN ('".implode("','",$chunk)."')" ;
		$dbwd = $mnm->openWikidataDB() ;
		$result = $mnm->tfc->getSQL ( $dbwd , $sql ) ;
		$exists = [] ;
		while($o = $result->fetch_object()) $exists[$o->page_title] = true ;
		$non_existing = [] ;
		foreach ( $qs AS $q ) {
			if ( !isset($exists[$q]) ) $non_existing[] = $q ;
		}
		unset($exists);
		if ( count($non_existing)==0 ) continue ;

		foreach ( $non_existing AS $q ) {
			$q = preg_replace ( '/\D/' , '' , "{$q}" ) ;
			$sql = "UPDATE `entry` SET `q`=NULL,`user`=NULL,`timestamp`=NULL WHERE `catalog`={$catalog} AND `q`={$q}" ;
			$mnm->getSQL($sql);
		}
	}
}

# Init
$mnm = new MixNMatch\MixNMatch ;

$catalogs = [] ;
if ( trim(strtolower($argv[1])) == 'all' ) {
	$sql = "SELECT `id` FROM `catalog` WHERE `active`=1" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $catalogs[] = $o->id ;
} else {
	$catalog = new MixNMatch\Catalog ( $argv[1] , $mnm ) ;
	if ( $catalog->isActive() ) $catalogs[] = $catalog->id() ;
}

foreach ( $catalogs AS $catalog ) {
	$qs = get_matched_items_in_catalog($catalog,$mnm);
	fix_redirects($catalog,$qs,$mnm);
	fix_deleted($catalog,$qs,$mnm);
}

?>