#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1]) ) die ( "catalog required\n" ) ;
$catalog = $argv[1] * 1 ;
if ( $catalog == 0 ) die ( "Bad catalog ID {$argv[1]}\n" ) ;
$testing = (isset($argv[2]) and $argv[2]=='1') ;

$mnm = new MixNMatch\MixNMatch ;
$d2a = new MixNMatch\DescriptionToAux ( $catalog , $mnm , $testing ) ;
$d2a->loadCodeFragment() ;

$sql = "SELECT * FROM entry WHERE catalog={$catalog}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$commands = $d2a->processEntry ( $o ) ;
	if ( $testing ) print_r ( $commands ) ;
	else $d2a->enact ( $commands ) ;
}

if ( !$testing ) {
	$d2a->touchCodeFragment();
	$mnm->queue_job($catalog,'auxiliary_matcher');
}

?>