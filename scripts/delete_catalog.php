#!/usr/bin/php
<?PHP

# This deletes a catalog, its entries, and associated metadata.
# This can not be undone!
# Requires catalog ID as first, and the word "really" as second parameter

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1]) ) die ( "catalog required\n" ) ;
$catalog = $argv[1] * 1 ;
if ( $catalog <= 0 ) die ( "Bad catalog\n" ) ;
if ( !isset($argv[2]) or $argv[2] != 'really' ) die ( "Second parameter 'really' required\n" ) ;

# Tables that have a `catalog` column. `entry` must be last in the array!
$tables_direct = ['code_fragments','autoscrape','jobs','overview','wd_matches','update_info','catalog_default_statement','entry'] ;

# Tables that have an `entry_id` column.
$tables_entry_id = ['aliases','descriptions','auxiliary','entry2word','issues','kv_entry','location_text','mnm_relation','multi_match','person_dates','location','log'] ;

$mnm = new MixNMatch\MixNMatch ;

$batch_size = 10000 ;
while ( 1 ) {
	$entry_ids = [] ;
	$entry_ids_sql = "SELECT `id` FROM `entry` WHERE `catalog`={$catalog} LIMIT {$batch_size}" ;
	$result = $mnm->getSQL ( $entry_ids_sql ) ;
	while($o = $result->fetch_object()) $entry_ids[] = $o->id ;
	if ( count($entry_ids) == 0 ) break ;
	$entry_ids = implode ( ',' , $entry_ids ) ;
	foreach ( $tables_entry_id AS $table ) {
		$mnm->getSQL ( "DELETE FROM `{$table}` WHERE `entry_id` IN ({$entry_ids})" ) ;
	}
	$mnm->getSQL ( "DELETE FROM `entry` WHERE `id` IN ({$entry_ids})" ) ;
}

$mnm->getSQL ( "DELETE FROM `kv_catalog` WHERE `catalog_id`={$catalog}" ) ;
foreach ( $tables_direct AS $table ) {
	$mnm->getSQL ( "DELETE FROM `{$table}` WHERE `catalog`={$catalog}" ) ;
}

# Finish it!
$mnm->getSQL ( "DELETE FROM `catalog` WHERE `id`={$catalog}" ) ;

?>