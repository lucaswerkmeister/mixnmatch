#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);

# Continuous sync?
if ( isset($argv[1]) and $argv[1] == 'sync' ) {
	# Start with:
	# jsub -continuous -mem 4g -cwd -N rcw_sync -quiet ./scripts/recent_changes_watcher.php sync
	while ( 1 ) {
		$rcw = new MixNMatch\RecentChangesWatcher ;
		$rcw->qs_tool_name = 'mixnmatch:rcw_sync' ;
		$rcw->syncMatchesToWikidata() ; # Slow!
	}
	exit(0);
}

if ( isset($argv[1]) and $argv[1] == 'watch' ) {
	# Start with:
	# jsub -continuous -mem 4g -cwd -N rcw_watch -quiet ./scripts/recent_changes_watcher.php watch
	while ( 1 ) {
		$rcw = new MixNMatch\RecentChangesWatcher ;
		$rcw->qs_tool_name = 'mixnmatch:rcw_watch' ;
		$rcw->updateRedirectedItems() ;
		$rcw->unmatchDeletedItems() ;
		$rcw->checkMatchesViaProperties() ;
		$rcw->checkAuxiliaryMatches() ;
		$rcw->matchPersonDates() ;
		$rcw->syncWdMatches() ;
		#$rcw->addUnknownBatch() ;
	}
	exit ( 0 ) ;
}


$testing = ($argv[1] ?? 0)*1 == 1 ;

$rcw = new MixNMatch\RecentChangesWatcher ( $testing ) ;
if ( $testing ) exit(0);

$rcw->updateRedirectedItems() ;
$rcw->unmatchDeletedItems() ;
$rcw->checkMatchesViaProperties() ;
$rcw->checkAuxiliaryMatches() ;
$rcw->matchPersonDates() ;
$rcw->syncWdMatches() ;

?>