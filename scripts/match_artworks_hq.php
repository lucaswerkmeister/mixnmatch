#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$use_single_catalog = false ;
if ( isset($argv[1]) ) {
	$catalog = $argv[1] * 1 ;
	$use_single_catalog = true ;
}

$mnm = new MixNMatch\MixNMatch ;

function getSearch ( $query ) {
	#if ( isset($property) and isset($q) ) $query .= " haswbstatement:{$property}={$q}" ;
	$url = "http://www.wikidata.org/w/api.php?action=query&list=search&format=json&srlimit=500&srsearch=" . urlencode ( $query ) ;
	return json_decode ( file_get_contents ( $url ) ) ;
}

$used_catalogs = [] ;
$sql = "SELECT * FROM vw_artwork" ;
if ( $use_single_catalog ) $sql .= " WHERE catalog={$catalog}" ;
#$sql = "SELECT * FROM vw_artwork WHERE id=72445099" ; # TESTING FIXME
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	#$query = '"'.$o->ext_name.'"' ;
	$query = preg_replace ( '/\(.+?\)/' , '' , $o->ext_name ) ;
	$j = getSearch ( $query ) ;
	if ( count($j->query->search) == 0 ) continue ; # Nothing found

	if ( !preg_match('|^\+0*(\d+)|',$o->inception,$m) ) continue ;
	$year = $m[1] ;

	$qs_search = [] ;
	foreach ( $j->query->search AS $v ) {
		$q = 'Q' . preg_replace ( '/\D/' , '' , $v->title ) ;
		$qs_search[$q] = $q ;
	}

	$sparql = "SELECT ?art { ?art wdt:P170 wd:{$o->creator} ; wdt:P571 ?inception FILTER ( year(?inception)={$year} ) }" ;
	#print "{$sparql}\n" ;
	$qs_sparql = $mnm->tfc->getSPARQLitems ( $sparql , 'art' ) ;

	$qs = [] ;
	foreach ( $qs_sparql AS $q ) {
		if ( !isset($qs_search[$q]) ) continue ;
		$qs[$q] = $q ;
	}

	if ( count($qs) == 0 ) continue ;
	if ( count($qs) == 1 ) {
		$mnm->setMatchForEntryID ( $o->id , $q , 0 , true , true ) ;
		continue ;
	}

	$qs2 = [] ;
	foreach ( $qs AS $q ) $qs2[] = preg_replace ( '/\D/' , '' , $q ) ;
	$sql = "INSERT IGNORE INTO multi_match (entry_id,catalog,candidates,candidate_count) VALUES ({$o->id},{$o->catalog},'" . implode ( ',' , $qs2 ) . "'," . count($qs2) . ")" ;
	$mnm->getSQL ( $sql ) ;

	#print_r($o);
	#print_r($qs_search);
	#print_r($qs);
	
	/*
	$j = getSearch ( $o->ext_name , 'P'.$o->aux_p , $o->aux_name ) ;
	if ( count($j->query->search) == 0 ) continue ; # Nothing found

	$used_catalogs[$o->catalog] = $o->catalog ;

	# Single match
	if ( count($j->query->search) == 1 ) {
		$q = $j->query->search[0]->title ;
		$mnm->setMatchForEntryID ( $o->id , $q , 0 , true , true ) ;
		continue ;
	}

	# Multi-match
	$qs = [] ;
	foreach ( $j->query->search AS $v ) {
		$qs[] = preg_replace ( '/\D/' , '' , $v->title ) ;
	}
	$sql = "INSERT IGNORE INTO multi_match (entry_id,catalog,candidates,candidate_count) VALUES ({$o->id},{$o->catalog},'" . implode ( ',' , $qs ) . "'," . count($qs) . ")" ;
	$mnm->getSQL ( $sql ) ;
	*/
}

# Unnecessary, but just in case...
foreach ( $used_catalogs AS $catalog_id ) {
	$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
	$catalog->updateStatistics();
	$catalog->useAutomatchers(0);
}


?>
