#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';
#require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ; # TODO required?

function setLocationQ ( $o , $q ) {
	global $mnm , $location_name2q ;

	# Set cache
	$location_name2q[$o->text] = $q ;

	# Update location_text
	$qnum = preg_replace ( '/\D/' , '' , $q ) ;
	$sql = "UPDATE location_text SET q={$qnum} WHERE id={$o->id}" ;
#print "{$sql}\n" ;
	$mnm->getSQL($sql) ;

	# Update auxiliary
	$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) VALUES ({$o->entry_id},{$o->property},'{$q}')" ;
#print "{$sql}\n" ;
	$mnm->getSQL($sql) ;
}

$mnm = new MixNMatch\MixNMatch ;

if ( !isset($argv[1]) ) die ( "catalog required\n" ) ;
$catalog = $argv[1] * 1 ;
if ( $catalog == 0 ) dir ( "Bad catalog ID {$argv[1]}\n" ) ;

# Get catalog data
$sql = "SELECT * FROM catalog WHERE id={$catalog}" ;
$result = $mnm->getSQL ( $sql ) ;
$cat = $result->fetch_object() ;
$lang = $cat->search_wp ;

# Get country names
$country_name2q = [] ;
$sparql = "SELECT DISTINCT ?q ?qLabel { VALUES ?country_types { wd:Q3624078 wd:Q6256 } ?q wdt:P31 ?country_types. { { ?q rdfs:label ?qLabel } UNION { ?q skos:altLabel ?qLabel } } FILTER((LANG(?qLabel)) = '{$lang}') }" ;
$j = $mnm->tfc->getSPARQL ( $sparql ) ;
foreach ( $j->results->bindings AS $b ) {
	$q = $mnm->tfc->parseItemFromURL ( $b->q->value ) ;
	$name = $b->qLabel->value ;
	$country_name2q[$name] = $q ;
}

# Name cache (from same catalog)
$location_name2q = [] ;
$sql = "SELECT DISTINCT `text`,q FROM location_text WHERE entry_id IN (SELECT id FROM entry WHERE catalog={$catalog}) AND q IS NOT NULL" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$location_name2q[$o->text] = 'Q' . $o->q ;
}

# Try to match the missing ones
$sql = "SELECT * FROM location_text WHERE entry_id IN (SELECT id FROM entry WHERE catalog={$catalog}) AND q IS NULL" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$name = $o->text ;
	if ( isset($location_name2q[$name]) ) {
		setLocationQ ( $o , $location_name2q[$name] ) ;
		continue ;
	}

	$country_q = '' ;
	if ( preg_match ( '|^(.+), ([^,]+)$|' , $name , $m ) ) {
		if ( isset($country_name2q[$m[2]]) ) {
#print "{$name} => " ;
			$name = trim ( $m[1] ) ;
#print "{$name}\n" ;
			$country_q = $country_name2q[trim($m[2])] ;
		}
	}

	$region_sparql = '' ;
	$is_match = preg_match ( '|^(.+), ([^,]+)$|' , $name , $m ) ;
	if ( !$is_match ) $is_match = preg_match ( '|^(.+) \(([^\(\)]+)\)$|' , $name , $m ) ;
	if ( $is_match ) {
		$region = $m[2] ;
		$name = $m[1] ;
		$region_sparql = "; wdt:P131* ?region . ?region rdfs:label '{$region}'@{$lang} " ;
	}

	$sparql = "SELECT DISTINCT ?q WHERE { ?q (wdt:P31/wdt:P279*) wd:Q486972 ; rdfs:label '{$name}'@{$lang} {$region_sparql}" ;
	if ( $country_q != '' ) {
		$sparql .= " . ?q (wdt:P17|wdt:P131*) wd:{$country_q}" ;
	}
	$sparql .= " }" ;
#print "{$sparql}\n" ;

	$items = $mnm->tfc->getSPARQLitems ( $sparql ) ;
#print_r ( $items ) ;
	if ( count($items) != 1 ) continue ;
	setLocationQ ( $o , $items[0] ) ;
}

?>