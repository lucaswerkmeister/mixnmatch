#!/usr/bin/php
<?php

require_once ( '/data/project/mix-n-match/scripts/mixnmatch.php' ) ;

$catalog_object = 2324 ;
$catalog_person = 2349 ;

$mnm = new MixNMatch ;

# Cache
$person_cache = [] ;
$sql = "SELECT id,ext_id FROM entry WHERE catalog={$catalog_person}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $person_cache[$o->ext_id] = $o->id ;

function getOrCreateArtist ( $ext_id , $name , $desc ) {
	global $person_cache , $mnm , $catalog_person ;
	if (isset($person_cache[$ext_id])) return $person_cache[$ext_id] ;
	$desc = trim(strip_tags($desc)) ;
	$o = [
		'type' => 'Q5' ,
		'catalog' => $catalog_person ,
		'id' => $ext_id ,
		'name' => $name ,
		'url' => 'http://collections.ashmolean.org/collection/search/trigger/person_id/value/'.$ext_id ,
		'desc' => $desc
	] ;
	$entry_id = $mnm->addNewEntry($o);
	$person_cache[$ext_id] = $entry_id ;
	return $entry_id ;
}

$sql = "SELECT * FROM entry WHERE catalog={$catalog_object}" ;
$sql .= " AND ext_desc=''" ;
#$sql .= " AND (q is null or user=0)" ; # For re-runs
#$sql .= " AND ext_id=38610" ; # TESTING
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$html = file_get_contents($o->ext_url) ;
	$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
	if (!preg_match('/<div class="item-detail">(.+?)<div class="disclaimer">/',$html,$m)) continue;
	if (!preg_match_all('/<li>(.+?)<\/li>/',$m[0],$li)) continue ;
	$desc = [] ;
	foreach ( $li[1] AS $part ) {
		if ( preg_match('/<p>(.+?)<\/p>.+?<p>(.+?)<\/p>/',$part,$m) ) {
			if ( trim($m[2]) != '' and $m[1]!='Title' ) $desc[] = trim($m[1]).":".trim($m[2]) ;
		} else if ( preg_match('/<p>Artist\/maker<\/p>.+?href=".*?value\/(\d+)">(.+?)<\/a>(.*?)$/',$part,$m) ) {
			$desc[] = "Artist/maker:".trim($m[2]) ;
			$artist_entry_id = getOrCreateArtist ( $m[1] , $m[2] , $m[3] ) ;
			$mnm->linkEntriesViaProperty($o->id,'P170',$artist_entry_id) ;
		}
	}
	$desc = implode ( "|" , $desc ) ;
	if ( !isset($o->ext_desc) or $o->ext_desc == null or trim($o->ext_desc) == '' ) {
		$sql = "UPDATE entry SET ext_desc='" . $mnm->escape($desc) . "' WHERE id={$o->id}" ;
		$mnm->getSQL($sql);
	}
}

?>