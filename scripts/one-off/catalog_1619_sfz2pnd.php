#!/usr/bin/php
<?PHP

# This script generates QuickStatements edits for Wikidata items with P7902 to replace the "sfz" prefix with the canonical "pnd"

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$mnm = new MixNMatch ;

$sparql = "SELECT ?q ?sfz { ?q wdt:P7902 ?sfz FILTER ( STRSTARTS(?sfz,'sfz') ) }" ;
$j = $mnm->tfc->getSPARQL ( $sparql ) ;
foreach ( $j->results->bindings AS $b ) {
	$q = $mnm->tfc->parseItemFromURL ( $b->q->value ) ;
	$sfz = $b->sfz->value ;
	$url = "https://www.deutsche-biographie.de/{$sfz}.html" ;
	$html = @file_get_contents ( $url ) ;
	if ( !isset($html) or $html == null or $html == '' ) {
		print "Could not get {$url} for {$q}\n" ;
		continue ;
	}
	if ( !preg_match ( '|>GND:\s*(\d+X*)</a>|' , $html , $m ) ) {
		print "No GND in {$url} for {$q}\n" ;
		continue ;
	}
	$gnd = $m[1] ;
	$pnd = "pnd{$gnd}" ;
	print "{$q}\tP7902\t\"{$pnd}\"\n" ;
	print "-{$q}\tP7902\t\"{$sfz}\"\n" ;
}

?>