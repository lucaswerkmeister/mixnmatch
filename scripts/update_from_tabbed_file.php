#!/usr/bin/php
<?PHP
declare(strict_types=1);

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # E_ALL|
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1]) ) die ("USAGE: {$argv[0]} CATALOG_ID [test]\n") ;
$catalog = $argv[1]*1 ;

$uc = new MixNMatch\UpdateCatalog ( $catalog ) ;
$uc->setTesting ( $argv[2]??'' == 'test' ) ;
print_r ( $uc->update_catalog() ) ;
$uc->run_updates();

?>