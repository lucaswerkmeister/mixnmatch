#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);
require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( !isset($argv[1]) ) die ( "USAGE: {$argv[0]} CATALOG_ID\n") ;
$catalog = $argv[1] * 1 ;

$am = new MixNMatch\AutoMatch ;
$am->verbose = true ;
$am->automatch_simple ( $catalog ) ;

?>
