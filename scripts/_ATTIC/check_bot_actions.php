#!/usr/bin/php
<?PHP

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
require_once ( '/data/project/mix-n-match/opendb.inc' ) ;

#$catalog = 467 ;
$sparql = 'SELECT DISTINCT ?q { ?q (p:P569|p:P570) ?statement . ?statement prov:wasDerivedFrom/<http://www.wikidata.org/prop/reference/P4025> [] }' ; # birth or death with a reference with P4025 statement
$user = 'Reinheitsgebot' ;
$pattern = 'wbcreateclaim.*\b(P570|P569)\b.*Mix\'n\'match:References$' ;

// Get potentially affected items
$items = [] ;

if ( isset($catalog) and $catalog != '' and $catalog != 0 ) {
	$db = openMixNMatchDB() ;
	$sql = "SELECT DISTINCT q FROM entry WHERE catalog=$catalog AND q is not null and q>0 AND user>0" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()) $items[] = 'Q' . $o->q ;
} else if ( isset($sparql) and $sparql != '' ) {
	$items = getSPARQLitems ( $sparql ) ;
	foreach ( $items AS $k => $v ) $items[$k] = "Q$v" ;
}

// Get edits
$user_id = '' ;
$db = openDB ( 'wikidata' , 'wikidata' ) ;
$sql = "SELECT user_id FROM user WHERE user_name='" . $db->real_escape_string($user) . "'" ;
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()) $user_id = $o->user_id ;
if ( $user_id == '' ) die ( "Unknown user $user\n" ) ;

$sql = "SELECT rev_id,rev_comment,rev_parent_id,page_title from page,revision_compat where page_id=rev_page AND page_namespace=0 and page_title IN ('" . implode("','",$items) . "') AND rev_user=$user_id" ;
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()) {
	if ( !preg_match ( '/' . str_replace('/','\\/',$pattern) . '/' , $o->rev_comment ) ) continue ;
	$url_undo = "https://www.wikidata.org/w/index.php?title={$o->page_title}&action=edit&undoafter={$o->rev_parent_id}&undo={$o->rev_id}" ;
	$url_undo_api = "https://www.wikidata.org/w/api.php?action=edit&title={$o->page_title}&undo={$o->rev_id}&token=" ;
	$url_diff = "https://www.wikidata.org/w/index.php?title={$o->page_title}&diff={$o->rev_id}&oldid={$o->rev_parent_id}" ;
	print "{$o->page_title}: {$o->rev_id} / {$o->rev_parent_id} $url_undo_api\n$url_diff\n\n" ;
}

?>