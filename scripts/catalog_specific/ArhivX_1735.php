#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog = 1735 ;

$mnm = new MixNMatch\MixNMatch ;

function fixDate ($v ) {
	if ( !isset($v) ) return '' ;
	$v = trim($v) ;
	$v = preg_replace('|(-00)+$|','',$v) ;
	if ( !preg_match('|^\d{3,4}[0-9-]*$|',$v) ) return '' ; # Paranoia
	return $v ;
}

$sql = "SELECT * FROM entry WHERE catalog={$catalog}" ;
$sql .= " AND (q IS NULL or user=0) AND id NOT IN (SELECT DISTINCT entry_id FROM vw_dates WHERE catalog={$catalog})" ; # DEACTIVATE FOR TESTING
#$sql .= " AND ext_id='61492'" ; # TESTING
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	$xml_url = "http://data.arhivx.net/rdf.php?id={$o->ext_id}" ;
	$rdf = file_get_contents($xml_url) ;
	$rdf = str_replace("\n"," ",$rdf);
	$rdf = preg_replace ( '|<(.+?):(.+?)>|' , '<$1_$2>' , $rdf ) ;
	$xml=simplexml_load_string($rdf);
	if ( isset($xml->rdf_Description) ) {
		$born = fixDate($xml->rdf_Description->schema_birthDate);
		$died = fixDate($xml->rdf_Description->schema_deathDate);
		if ( $born.$died != '' ) {
			$sql = "INSERT IGNORE INTO person_dates (entry_id,born,died) VALUES ({$o->id},'{$born}','{$died}')" ;
			$mnm->getSQL($sql);
		}
	}
}

?>