#!/usr/bin/php
<?PHP

# NOTE: No catalog for OLID works yet

$toolname = 'mixnmatch_olid_works' ;

require_once dirname(__DIR__) . '/../vendor/autoload.php';
require_once ( '/data/project/listeria/Chris-G-botclasses/botclasses.php' );
require_once ( "/data/project/quickstatements/public_html/quickstatements.php" ) ;

function getQS () {
	global $toolname ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/mix-n-match/reinheitsgebot.conf" ;
	$qs->toolname = $toolname ;
	$qs->sleep = 1 ;
	$qs->use_command_compression = true ;
	return $qs ;
}

function runCommandsQS ( &$qs , $commands ) {
	$commands = implode ( "\n" , $commands ) ;
	$tmp_uncompressed = $qs->importData ( $commands , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp_uncompressed['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	if ( !isset($qs->last_item) ) return ;
	$last_item = 'Q' . preg_replace ( '/\D/' , '' , "{$qs->last_item}" ) ;
	return $last_item ;
}

function set_author ( $work_q , $author_q ) {
	$commands = [] ;
	$commands[] = "{$work_q}\tP50\t{$author_q}" ;
	#print_r ( $commands ) ;
	$qs = getQS() ;
	runCommandsQS ( $qs , $commands ) ;	
}

function write_wikitext () {
	global $wikitext , $mnm ;

	# Create wiki page
	$wiki = [] ;
	$wiki[] = "= Potential issues with OLID =" ;
	$wiki[] = "== Possible authors ==" ;
	foreach ( $wikitext['possible'] AS $v ) {
		$wiki[] = "# {{Q|" . preg_replace('|\D|','',$v[0]) . "}} => {{Q|" . preg_replace('|\D|','',$v[1]) . "}}" ;
	}
	$wiki[] = "== Multiple authors with the same OLID ID ==" ;
	foreach ( $wikitext['multiple'] AS $v ) {
		$parts = [] ;
		foreach ( $v AS $part ) $parts[] = "{{Q|" . preg_replace('|\D|','',$part) . "}}" ;
		$wiki[] = "# " . implode ( ", " , $parts ) ;
	}
	$wiki = implode ( "\n" , $wiki ) ;

	# Write page
	$ini = parse_ini_file ( '/data/project/mix-n-match/bot.ini' ) ;
	$wiki_user = $ini['user'] ;
	$wiki_pass = $ini['pass'] ;
	$w = new wikipedia ;
	$w->quiet = true ;
	$w->url = "https://www.wikidata.org/w/api.php";
	$w->setUserAgent( 'User-Agent: '.$wiki_user.' (http://www.wikidata.org/wiki/User:' . str_replace(' ','_',$wiki_user) . ')'  );
	$w->login( $wiki_user , $wiki_pass );
	$page = "User:Magnus Manske/OLID" ;
	$title = str_replace(' ','_',$page) ;

	$p = $w->getpage ( $title ) ;
	if ( trim($p) == trim($wiki) ) {
		return ; 
	}

	$ts = $mnm->getCurrentTimestamp() ;
	$w->edit( $title, $wiki, "Update $ts" );
}

function match_or_create_author ( $author , $work_q , $olid_work_id ) {
	global $mnm , $author_cache , $wikitext ;

	# Try cache
	if ( isset ( $author_cache[$author->id]) ) {
		set_author ( $work_q , $author_cache[$author->id] ) ;
		return ;
	}

	# Try to find match
	$j = $mnm->getSearchResults ( '' , 'P648' , $author->id ) ;
	if ( count($j) == 1 ) {
		$author_q = $j[0]->title ;
		$author_cache[$author->id] = $author_q ;
		set_author ( $work_q , $author_q ) ;
		return ;
	}
	if ( count($j) > 1 ) {
		$multi = [] ;
		foreach ( $j AS $a ) $multi[] = $a->title ;
		$wikitext['multiple'][] = $multi ;
		return ;
	}

	# See if a person with that name exists
	$query = $author->name ;
	$query = trim ( preg_replace ( '| *[A-Z]\. *|' , ' ' , $query ) ) ;
	#print "{$author->id} not found, looking for {$author->name} as '{$query}'\n" ;
	$j = $mnm->getSearchResults ( $query , 'P31' , 'Q5' ) ;
	if ( count($j) > 0 ) {
		#print "At least one item for {$author->name} exists, skipping\n" ;
		if ( count($j) == 1 ) {
			$author_q = $j[0]->title ;
			$wikitext['possible'][] = [ $work_q , $author_q ] ;
		}
		return ;
	}

	if ( preg_match ( '/\b(\d{2,}|International|Symposium|Congress|Society|Office|Inc|Record|Association|Publishing|Fictitious|character|Conference|in|and|mu[zs]eum|advanced|study|institute{0,1}|sie|die|erste|war|dept|engineering|conference|research|council|&|undifferentiated|Consortium|for|the|of|\(|\[)\b/i' , $author->name) ) return ;

	# Create author
	#print "{$author->name} does not appear to exist!\n" ;
	$ts = $mnm->getCurrentTimestamp() ;
	$ts = "+".substr($ts,0,4)."-".substr($ts,4,2)."-".substr($ts,6,2)."T00:00:00Z/11" ;
	$refs = "\tS854\t\"https://openlibrary.org/books/{$olid_work_id}\"\tS813\t{$ts}" ;
	$commands = [] ;
	$commands[] = 'CREATE' ;
	$commands[] = "LAST\tLen\t\"{$author->name}\"" ;
	$commands[] = "LAST\tP31\tQ5{$refs}" ;
	$commands[] = "LAST\tP648\t\"{$author->id}\"{$refs}" ;
	$commands[] = "LAST\tP106\tQ36180{$refs}" ;
	#print_r ( $commands ) ;
	$qs = getQS() ;
	$author_q = runCommandsQS ( $qs , $commands ) ;	
	print "New author: https://www.wikidata.org/wiki/{$author_q}\n" ;
	$author_cache[$author->id] = $author_q ;
	set_author ( $work_q , $author_q ) ;
}

$mnm = new MixNMatch\MixNMatch ;
$author_cache = [] ;
$wikitext = [ 'possible' => [] , 'multiple' => [] ] ;

# Get OLID works on Wikidata without author (P50)
$sparql = 'SELECT ?work ?olid { ?work wdt:P31 wd:Q3331189 ; wdt:P648 ?olid MINUS { ?work wdt:P50 [] } MINUS { ?work wdt:P98 [] } }' ;
$sparql_j = $mnm->tfc->getSPARQL ( $sparql ) ;

foreach ( $sparql_j->results->bindings AS $b ) {
	$q = $mnm->tfc->parseItemFromURL ( $b->work->value ) ;
	$olid_work_id = $b->olid->value ;
	$olid_work_id = preg_replace ( '|/.*$|' , '' , $olid_work_id ) ; # Paranoia
	if ( !preg_match ( '|^OL\d+M$|' , $olid_work_id ) ) continue ; # Paranoia
	#print "{$q}\t{$olid_work_id}\n" ;
	$olid_full_id = "OLID:{$olid_work_id}" ;
	$url = "https://openlibrary.org/api/books?format=json&bibkeys={$olid_full_id}&jscmd=data" ;
	$api_j = @json_decode ( @file_get_contents ( $url ) ) ;
	if ( !is_object($api_j) ) continue ; # Paranoia
	$j = $api_j->$olid_full_id ;
	if ( !is_object($j) ) continue ;

	if ( !isset($j->authors) or !is_array($j->authors) ) continue ; # Paranoia
	$olid_authors = [] ;
	foreach ( $j->authors AS $author ) {
		if ( !preg_match ( '|^https://openlibrary\.org/authors/(OL\d+A)|' , $author->url , $m ) ) continue ;
		if ( !isset($author->name ) ) continue ;
		$olid_authors[] = (object) [ 'name' => $author->name , 'id' => $m[1] ] ;
	}
	if ( count ( $olid_authors ) == 0 ) continue ;
	foreach ( $olid_authors AS $author ) {
		match_or_create_author ( $author , $q , $olid_work_id ) ;
	}
}

write_wikitext() ;

?>