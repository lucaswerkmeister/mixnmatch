#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 98 ;

$mnm = new MixNMatch\MixNMatch ;
$sql = "SELECT id,ext_url FROM entry WHERE catalog={$catalog_id} AND q!=0" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$url = $o->ext_url ;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, TRUE);
//	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$a = curl_exec($ch);

	if ( preg_match ( '/301 Moved Permanently/' , $a ) ) {
		$ts = $mnm->getCurrentTimestamp() ;
		$sql_na = "UPDATE entry SET q=0,user=4,`timestamp`='{$ts}' WHERE id={$o->id}" ;
		if ( preg_match ( '|Location: https://openlibrary.org/authors/(\S+)|' , $a , $m ) ) {
			$new_id = $mnm->escape ( $m[1] ) ;
			$sql = "SELECT id FROM entry WHERE catalog={$catalog_id} AND ext_id='{$new_id}'" ;
			$result2 = $mnm->getSQL ( $sql ) ;
			if($o2 = $result2->fetch_object()){
				#print "MAKE N/A1: {$sql_na}\n" ;
				$mnm->getSQL ( $sql_na ) ;
			} else {
				$sql = "UPDATE entry SET ext_id='{$new_id}',ext_url='https://openlibrary.org/authors/{$new_id}' WHERE id={$o->id}" ;
				#print "CHANGE TO {$sql}\n" ;
				$mnm->getSQL ( $sql ) ;
			}
		} else {
			#print "MAKE N/A2: {$sql_na}\n" ;
			$mnm->getSQL ( $sql_na ) ;
		}
	} else {
		#print "NOT REDIRECTED\n" ;
	}
}

$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
$catalog->updateStatistics() ;

?>