#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog = 2184 ;

$mnm= new MixNMatch\MixNMatch ;

$sparql = 'SELECT DISTINCT ?q { ?q (wdt:P212|wdt:P957) [] MINUS { ?q wdt:P724 [] } }' ;
$items = $mnm->tfc->getSPARQLitems ( $sparql ) ;
#$items = [ 'Q6507' ] ; # TESTING

$has_q = [] ;
$sql = "SELECT DISTINCT q FROM entry WHERE catalog={$catalog} AND q IS NOT NULL AND user!=0" ;
$result = $mnm->getSQL ( $sql ) ;
while ($o = $result->fetch_object()) $has_q['Q'.$o->q] = $o->q ;

foreach ( $items AS $q ) {
	if ( isset($has_q[$q]) ) continue ;
	$url_wd = "https://www.wikidata.org/wiki/{$q}" ;
	$url_ia = "https://archive.org/services/context/books?url=" . urlencode($url_wd) ;
#	print "{$url_ia}\n" ;
	$j = json_decode ( file_get_contents ( $url_ia ) ) ;
	if ( isset($j->status) and $j->status == 'error' ) continue ;
	$entries = [] ;
	foreach ( $j AS $o ) {
		if ( !is_object($o) ) continue ;
		if ( !isset($o->metadata) ) continue ;
		if ( !isset($o->metadata->identifier) ) continue ;
		$entry = (object) [
			'catalog' => $catalog ,
			'id' => $o->metadata->identifier ,
			'name' => $o->metadata->title ,
			'aux' => [] ,
			'q' => $q ,
			'type' => 'Q3331189' # Edition
		] ;
		$entry->url = 'https://archive.org/details/' . $entry->id ;
		if ( is_array($entry->name) ) $entry->name = $entry->name[0] ;

		$d = [] ;
		foreach ( ['creator','publisher'] AS $k ) {
			if ( !isset($o->metadata->$k) ) continue ;
			$v = $o->metadata->$k ;
			if ( !is_array($v) ) $v = [$v] ;
			$d[] = ucfirst($k) . ': ' . implode('/',$v) ;
		}
		if ( count($d) > 0 ) $entry->desc = implode ( "; " , $d ) ;

		$hadthat = [] ;
		foreach ( ['openlibrary','openlibrary_edition','openlibrary_work'] AS $ol ) {
			if ( !isset($o->metadata->$ol) ) continue ;
			if ( isset($hadthat[$o->metadata->$ol]) ) continue ;
			$entry->aux[] = ['P648',$o->metadata->$ol] ;
			$hadthat[$o->metadata->$ol] = 1 ;
		}

		$entries[$entry->id] = $entry ;
	}

	if ( count($entries) == 0 ) continue ;
#	if ( count($entries) == 1 ) $mnm->addNewEntry ( $entries[0] ) ; # Paranoia
#	else print "{$q} has " . count($entries) . " matches on IA\n" ;
	foreach ( $entries AS $e ) $mnm->addNewEntry ( $e ) ;
}

?>