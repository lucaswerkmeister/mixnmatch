#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$sql = "SELECT * FROM entry WHERE catalog=1221 AND (q IS NULL OR user=0)" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	if ( !preg_match ( '/^\d{4}-\d{4}$/' , $o->ext_desc ) ) continue ;
	$sparql = "SELECT ?q { ?q wdt:P236 '{$o->ext_desc}'}" ;
	$items = $mnm->tfc->getSPARQLitems ( $sparql , 'q' ) ;
	if ( count($items) != 1 ) continue ;
	$mnm->setMatchForEntryID ( $o->id , $items[0] , 3 , true , false ) ;
}

?>