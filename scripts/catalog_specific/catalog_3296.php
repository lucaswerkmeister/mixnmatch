#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 3296 ;

$ranks = [
	'kingdom' => 'Q36732' ,
	'phylum' => 'Q38348' ,
	'class' => 'Q37517' ,
	'order' => 'Q36602' ,
	'family' => 'Q35409' ,
	'genus' => 'Q34740' ,
	'subgenus' => 'Q3238261' ,
	'species' => 'Q7432' ,
	'subspecies' => 'Q68947'
] ;

$mnm = new MixNMatch\MixNMatch ;

$limit = 1000 ;
$offset = 0 ;
$again = true ;
while ( $again ) {
	$url = "http://api.gbif.org/v1/species?limit={$limit}&offset={$offset}" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	foreach ( $j->results AS $r ) {
		if ( $r->taxonomicStatus != 'ACCEPTED' ) continue ;
		if ( $r->synonym ) continue ;
		$o = (object) [
			'id' => $r->key ,
			'name' => $r->scientificName ,
			'desc' => [ $r->rank ] ,
			'catalog' => $catalog_id ,
			'type' => 'Q16521' ,
			'aux' => [
				[ 'P225' , $r->scientificName ]
			]
		] ;
		if ( isset($r->vernacularName) and strtolower($r->vernacularName)!=strtolower($r->scientificName) ) $o->desc[] = $r->vernacularName  ;
		$o->desc = implode ( " | " , $o->desc ) ;
		$rank = strtolower($r->rank) ;
		if ( isset($ranks[$rank]) ) $o->aux[] = [ 'P105' , $ranks[$rank] ] ;

		#print_r ( $o ) ;
		$mnm->addNewEntry ( $o ) ;
	}
	$again = !$j->endOfRecords ;
	$offset += $limit ;
}

$catalog = new MixNMatch\Catalog ( $catalog_id , $mnm ) ;
$catalog->updateStatistics() ;
$mnm->queue_job ( $catalog_id , 'taxon_matcher' ) ;

?>