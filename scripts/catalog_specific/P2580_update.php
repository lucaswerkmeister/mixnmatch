#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog = 177 ;
$prop = 'P2580' ;

$mnm = new MixNMatch\MixNMatch ;

$rows = explode ( "\n" , file_get_contents('https://bbld.de/beacon.all.txt') ) ;
$ids = [] ;
foreach ( $rows AS $row ) {
	$row = trim($row) ;
	if ( $row == '' ) continue ;
	if ( preg_match ( '/^#/' , $row ) ) continue ;
	$ids[$row] = $row ;
}

# Set unmatched entries not in beacon to N/A
$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND ext_id NOT IN ('" . implode("','",$ids) . "') AND (q is NULL or user=0)" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	$mnm->setMatchForEntryID ( $o->id , 0 , 4 , false , true ) ;
}

# Find ids not in catalog
$sql = "SELECT ext_id FROM entry WHERE catalog={$catalog}" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	if ( isset($ids[$o->ext_id]) ) unset($ids[$o->ext_id]) ;
}

# Remove ISNI/GNDs (not necessary, but there is an issue on WD about GNDs in this property)
foreach ( $ids AS $id ) {
	if ( preg_match ( '/^\d+/' , $id ) ) unset ( $ids[$id] ) ;
}

# Create remaining
foreach ( $ids AS $id ) {
	$o = (object) [
		'catalog' => $catalog ,
		'id' => $id ,
		'name' => preg_replace ( '/[_-]/' , ' ' , $id ) ,
		'desc' => '' ,
		'url' => "https://bbld.de/" . urlencode($id)
	] ;

	$born = '' ;
	$died = '' ;
	if ( preg_match ( '/-(\d{4})-(\d{4})$/' , $id , $m ) ) {
		$born = $m[1] ;
		$died = $m[2] ;
	}

	$html = file_get_contents ( $o->url ) ;

	# Name/desc
	if ( preg_match ( '/<h1>(.+?)<\/h1>/' , $html , $m ) ) {
		$o->name = $m[1] ;
		if ( preg_match ( '/^(.+?) +(\(.+)$/' , $o->name , $m ) ) {
			$o->name = trim($m[1]) ;
			$o->desc = $m[2] ;
		}
		if ( preg_match ( '/^(.+?), (.+)$/' , $o->name , $m ) ) { # B, A => A B
			$o->name = $m[2] . ' ' . $m[1] ;
		}
		if ( preg_match ( '/^(.+) v\. (.+)$/' , $o->name , $m ) ) { # v. => von
			$o->name = $m[1] . ' von ' . $m[2] ;
		}
	}

	# Dates
	if ( preg_match ( '/\*\s*(\d{4}-\d{2}-\d{2})/' , $html , $m ) ) {
		$born = $m[1] ;
	}
	if ( preg_match ( '/\+\s*(\d{4}-\d{2}-\d{2})/' , $html , $m ) ) {
		$died = $m[1] ;
	}

	# Description
	$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
	if ( preg_match ( '/<h2>Leben<\/h2>(.+?)<\/p>/' , $html , $m ) ) {
		$o->desc = trim(strip_tags($m[1])) ;
	}

	if ( $born.$died != '' ) $o->type = 'Q5' ;

#print_r ( $o ) ;
#print "{$born} - {$died}\n" ;

	$entry_id = $mnm->addNewEntry ( $o ) ;
	if ( $born != '' or $died != '' ) {
		$sql = "INSERT IGNORE INTO `person_dates` (entry_id,born,died) VALUES ({$entry_id},'{$born}','{$died}')" ;
		$mnm->getSQL ( $sql ) ;
	}
#print "=> {$mnm->root_url}/#/entry/{$entry_id}\n" ;
}

exec ( '/data/project/mix-n-match/scripts/match_person_entries_by_name_and_dates.php '.$catalog ) ;

?>