#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/../vendor/autoload.php';
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

$catalog = 1640 ;
$beacon_file = '1640.beacon'; #'http://documents.cerl.org/beacon/gnd.txt' ;
$mnm = new MixNMatch\MixNMatch ;

function fixDate ( $d ) {
	if ( $d == '' ) return $d ;
	if ( preg_match ( '/^(\d{3,4})/' , $d , $m ) ) return $m[1] ;
	return '' ;
}

$sql = "SELECT * FROM entry WHERE ext_name=ext_id AND catalog={$catalog} AND " . $mnm->descriptionIsEmptySQL() ;
$sql .= " AND q IS NULL" ;

$r = rand() / getrandmax() ;
$sql .= " AND random>={$r} ORDER BY random LIMIT 20000" ;
#print "$sql\n" ; exit(0) ;

#$sql .= " LIMIT 10" ; # TESTING
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$json_url = "https://data.cerl.org/thesaurus/{$o->ext_id}?format=json&style=jsonld" ;
#print "{$json_url}\n" ;
	$jt = @file_get_contents ( $json_url ) ;
#	$jt = exec ( "curl '{$json_url}'" ) ;
	if ( !isset($jt) or $jt == '' or $jt === null ) continue ;
	$j = json_decode ( $jt , true ) ;
	if ( !isset($j) or $j == '' or $j === null ) continue ;
	$g = $j['@graph'][0] ;
#print_r ( $g ) ;
	$no = (object) [] ;

	if ( isset($g['rdaGr2:nameOfThePerson']) ) {
		$no->ext_name = $g['rdaGr2:nameOfThePerson'][0]['@value'] ;
	} else if ( isset($g['ct:imprintName']) ) {
		$no->ext_name = $g['ct:imprintName'][0]['@value'] ;
	}
	if ( isset($no->ext_name) ) $no->ext_name = preg_replace ( '/^(.+?), (.+)$/' , '$2 $1' , $no->ext_name ) ;


	$d = [] ;
	$born = '' ;
	$died = '' ;
	if ( isset($g['rdaGr2:dateOfBirth']) ) $born = fixDate($g['rdaGr2:dateOfBirth'][0]['@value']) ;
	if ( isset($g['rdaGr2:dateOfDeath']) ) $died = fixDate($g['rdaGr2:dateOfDeath'][0]['@value']) ;
	if ( $born.$died != '' ) $d[] = "{$born}-{$died}" ;
	if ( isset($g['skos:note']) ) {
		foreach ( $g['skos:note'] AS $note ) {
			$d[$note['@value']] = $note['@value'] ;
		}
	}
	if ( isset($g['ct:activityNote']) ) {
		foreach ( $g['ct:activityNote'] AS $note ) {
			$d[$note['@value']] = $note['@value'] ;
		}
	}
	if ( count($d) > 0 ) $mnm->setDescriptionForEntryID ( $o->id , implode('; ',$d) ) ;

	if ( isset($g['owl:sameAs']) ) {
		foreach ( $g['owl:sameAs'] AS $sa ) {
			if ( preg_match ( '/^http:\/\/viaf\.org\/viaf\/(.+)\/*$/' , $sa['@id'] , $m ) ) {
				try {
					$mnm->setAux ( $o->id , 'P214' , $m[1] ) ;
				} catch (Exception $e) {
					// Ignore
				}
			}
		}
	}

	if ( count($no) == 0 ) continue ;

	$sql = [] ;
	foreach ( $no AS $k => $v ) $sql[] = "`{$k}`='".$mnm->escape($v)."'" ;
	$sql = "UPDATE entry SET " . implode ( ',' , $sql ) . " WHERE id={$o->id}" ;
	$mnm->getSQL ( $sql ) ;
}

?>