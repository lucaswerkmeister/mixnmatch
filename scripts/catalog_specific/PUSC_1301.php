#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog = 1301 ;

$mnm = new MixNMatch\MixNMatch ;

$sql = "SELECT * FROM entry WHERE catalog={$catalog}" ;
$sql .= " AND (q IS NULL or user=0) AND id NOT IN (SELECT DISTINCT id FROM vw_aux WHERE catalog={$catalog})" ; # DEACTIVATE FOR TESTING
#$sql .= " AND ext_id='58964'" ; # TESTING
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	$marc_xml_url = "http://catalogo.pusc.it/cgi-bin/koha/pusc-opac-export-auth.pl?format=marcxml&authid={$o->ext_id}" ;
	$xml=simplexml_load_string(file_get_contents($marc_xml_url));
	if ( !isset($xml->datafield) ) continue ;
	foreach ( $xml->datafield AS $df ) {
		if ( !isset($df->subfield) ) continue ;
		if ( count($df->subfield) != 2 ) continue ;
		list($k,$v) = $df->subfield ;
		#print "{$k} => {$v}\n" ;
		if ( $k == 'viaf' ) $mnm->setAux($o->id,'P214',$v) ;
		if ( $k == 'isni' ) $mnm->setAux($o->id,'P213',$v) ;
		if ( $k == 'wikidata' ) $mnm->setMatchForEntryID($o->id,$v,4,true,false) ;
		if ( $k == 'idref' ) $mnm->setAux($o->id,'P269',$v) ;
		if ( $k == 'nukat' ) $mnm->setAux($o->id,'P1207',str_replace(' ','',$v)) ;
		if ( $k == 'uri' ) {
			if ( preg_match('|^https{0,1}://d-nb.info/gnd/(.+)$|',$v,$m) ) $mnm->setAux($o->id,'P227',$m[1]) ;
			if ( preg_match('|^https{0,1}://catalogue.bnf.fr/ark:/12148/cb(.+)$|',$v,$m) ) $mnm->setAux($o->id,'P268',$m[1]) ;
		}
		# TODO: datoses (eg XX983819)
		# TODO: vatlib (eg ADV11576380)
	}
}

?>