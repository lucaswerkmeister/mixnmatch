#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$catalog_id = 4512 ;

$mnm = new MixNMatch\MixNMatch ;

# Get existing
$existing = [] ;
$sql = "SELECT `ext_id` FROM `entry` WHERE `catalog`={$catalog_id}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $existing[$o->ext_id] = 1 ;

for ( $page = 1 ; $page <= 75 ; $page++ ) {
	$url = "https://www.olschki.it/autori/a-z/p{$page}" ;
	$h = file_get_contents($url);
	$m = preg_match_all('|<a href="/catalogo/autore/(\d+)">(.+?)</a>|', $h, $matches, PREG_SET_ORDER) ;
	foreach ( $matches as $m ) {
		$name = preg_replace ( '|^(.+?), (.+)$|' , '$2 $1' , $m[2] ) ;
		$o = (object) [
			'catalog' => $catalog_id ,
			'id' => $m[1] ,
			'name' => $name ,
			'desc' => '' ,
			'url' => "https://www.olschki.it/catalogo/autore/{$m[1]}" ,
			'type' => 'Q5'
		] ;
		if ( isset($existing[$o->id]) ) continue ;
		$mnm->addNewEntry ( $o ) ;
		$existing[$o->id] = 1 ;
	}

}

?>