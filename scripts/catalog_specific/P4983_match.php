#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/../vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$sql = "SELECT * FROM entry WHERE catalog=1068 AND (q IS NULL OR user=0)" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	$title = $o->ext_name ;
	$year = $o->ext_desc ;
	if ( !preg_match ( '/^\d{4}$/',$year) ) continue ;
	$sparql = "SELECT DISTINCT ?q { ?q wdt:P31/wdt:P279* wd:Q5398426 . ?q rdfs:label \"{$title}\"@en . ?q wdt:P580|wdt:P577 ?start FILTER (YEAR(?start)={$year}) }" ;
	$items = $mnm->tfc->getSPARQLitems ( $sparql , 'q' ) ;
	if ( count($items) != 1 ) continue ;
	$mnm->setMatchForEntryID ( $o->id , $items[0] , 3 , true , false ) ;
}

?>
