#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_WARNING|E_ALL);
ini_set('memory_limit','3500M');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$cmd = $argv[1] ?? '' ;

$autoscrape = new MixNMatch\AutoScrape ;

if ( $cmd == 'update_next' ) {
	$catalog = $autoscrape->getNextCatalogToUpdate() ;
} else {
	if ( !isset ( $argv[2] ) ) {
		print "Needs arguments : command catalog_id\n" ;
		exit ( 0 ) ;
	}
	$catalog = $argv[2] ;
}

$ok_only = (isset($argv[3]) and $argv[3]=='force')?false:true ;
if ( $cmd == 'test' ) $ok_only = false ;
if ( !$autoscrape->loadByCatalog($catalog,$ok_only) ) die ( $autoscrape->error."\n" ) ;

$autoscrape->max_urls_requested = 1 ;
if ( $cmd == 'test' ) $autoscrape->runTest() ;
else if ( $cmd == 'vtest' ) {
	$autoscrape->verbose = true ;
	$autoscrape->runTest() ;
} else if ( $cmd == 'run' or $cmd == 'update_next' ) {
	$autoscrape->scrapeAll() ;
} else print "Unknown command $cmd\n" ;


?>