#!/usr/bin/php
<?PHP

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ( isset($argv[1]) ) $catalog = $argv[1] * 1 ;
else die ( "Catalog required" ) ;
$testing = $testing = (isset($argv[2]) and $argv[2]=='1') ; ;

$mnm = new MixNMatch\MixNMatch ;
$h2d = new MixNMatch\HTMLtoDescription ( $catalog , $mnm ) ;
$h2d->loadCodeFragment() ;

$sql = $h2d->getMainSQL() ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	if ( $testing ) print "Trying {$o->ext_url}\n" ;

	$commands = $h2d->processEntry ( $o ) ;
	
	if ( $testing ) print_r ( $commands ) ;
	else $h2d->enact ( $commands ) ;
}

# Process results
if ( !$testing ) {
	$job_id = $mnm->queue_job($catalog,'update_person_dates');
	$mnm->queue_job($catalog,'match_person_dates',$job_id);
	$job_id = $mnm->queue_job($catalog,'generate_aux_from_description');
	$mnm->queue_job($catalog,'auxiliary_matcher',$job_id);
}

?>