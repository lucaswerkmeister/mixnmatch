<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once '/data/project/mix-n-match/classes/LargeCatalogs.php';

function killme ( $msg = '' ) {
	global $out ;
	if ( $msg != '' ) $out['status'] = $msg ;
	header ( 'Content-type: application/json; charset=UTF-8' ) ;
	if ( isset($_REQUEST['callback']) ) print $_REQUEST['callback'].'(' ;
	print json_encode ( $out ) ;
	if ( isset($_REQUEST['callback']) ) print ')' ;
	exit(0);
}

$lc = new MixNMatch\LargeCatalogs ;

$out = [ 'status' => 'OK' , 'data' => [] ] ;

$action = $lc->getRequest ( 'action' , '' ) ;
if ( $action == 'bbox' ) {
	# https://mix-n-match.toolforge.org/lc_api.php?action=bbox&bbox=-2.5,51.4526,-2.49,51.4536
	$limit = 5000 ;
	$slim = $lc->getRequest ( 'slim' , 0 ) * 1 ;
	$bbox = $lc->getRequest ( 'bbox' , '' ) ;
	$bbox = preg_replace ( '/[^0-9,\.\-]/' , '' , $bbox ) ;
	$bbox = explode ( ',' , $bbox ) ;
	$out['bbox'] = $bbox ;
	foreach ( $bbox AS $k => $v ) $bbox[$k] = $v * 1 ;
	if ( count($bbox) != 4 ) killme("Required parameter bbox does not have 4 comma-separated numbers") ;

	$out['catalogs'] = [] ;
	$catalogs = $lc->getCatalogs() ;
	foreach ( $catalogs AS $catalog ) {
		if ( $catalog->has_lat_lon == 0 ) continue ;
		$out['catalogs'][$catalog->id] = $catalog ;
		$sql = "SELECT * FROM `{$catalog->table}` WHERE `longitude` BETWEEN {$bbox[0]} AND {$bbox[2]} AND `latitude` BETWEEN {$bbox[1]} AND {$bbox[3]} LIMIT {$limit}" ;
		$out['sql'][] = $sql ;

		$result = $lc->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $slim ) {
				foreach ( $o AS $k => $v ) {
					if ( !in_array($k,['ext_id','name','title','desc','description','latitude','longitude']) ) unset ( $o->$k ) ;
				}
			}
			$o->catalog = $catalog->id ;
			$out['data'][] = $o ;
			if ( count($out['data']) >= $limit ) break ;
		}
		if ( count($out['data']) >= $limit ) break ;
	}

} else {
	killme ( "Unknown action '{$action}'" ) ;
}

killme() ;

?>