<?PHP

//ini_set('memory_limit','2500M');
//set_time_limit ( 60 * 2 ) ; // Seconds
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch ;

$min_paintings = 5 ;

$painters = [] ;
$names = [] ;
$rows = explode ( "\n" , file_get_contents ( '/data/project/mix-n-match/manual_lists/paintings_without_creator.tab' ) ) ;
foreach ( $rows AS $row ) {
	if ( !preg_match ( '/^(Q\d+)\tpainting by (.+?)$/' , $row , $m ) ) continue ;
	$q = $m[1] ;
	$name = $m[2] ;
	if ( preg_match ( '/\b(n\/a|and|or|studio|group|anonymous|unknown|school|unidentified)\b/i' , $name ) ) continue ;
	$key = strtolower ( $name ) ;
	$key = preg_replace ( '/^sir /' , '' , $key ) ;
	$key = preg_replace ( '/, bt$/' , '' , $key ) ;
	$key = preg_replace ( '/\(.+?\)/' , ' ' , $key ) ;
	$key = preg_replace ( '/,{0,1} ra\s*$/' , ' ' , $key ) ;
	$key = preg_replace ( '/,{0,1} pra\s*$/' , ' ' , $key ) ;
	$key = trim ( preg_replace ( '/\s+/' , ' ' , $key ) ) ;
	$painters[$key][] = $q ;
	
	$name = trim ( preg_replace ( '/\((RCE|BStGS|NGoS) .+?\)/' , '' , $name ) ) ;
	$names[$key][$name]++ ;
}


uasort ( $painters , function ( $a , $b ) {
	return count($b) - count($a) ;
} ) ;

print $mnm->tfc->getCommonHeader ( 'Painters' ) ;
print "<div class='lead'>Paintings on Wikidata with no CREATOR (P170), but 'painting by' in the description ($min_paintings paintings minumum).<br/>
This list is regenerated daily. Please make sure a suitable creator item doesn't exist already before creating a new one!<br/>
(Click on their name to perform a search on Wikidata.)</div>" ;

foreach ( $painters AS $key => $qs ) {
	if ( count($qs) < $min_paintings ) break ;
	$k2 = $mnm->tfc->escapeAttribute(urlencode($key)) ;
	$k3 = str_replace ( '+' , ' ' , $k2 ) ;
	print "<section style='border-top:1px solid #DDD;margin-top:10px;padding:5px'>" ;
	print "<p><a href='https://www.wikidata.org/w/index.php?search={$k2}+haswbstatement:P31=Q5&title=Special:Search&profile=default&fulltext=1' target='_blank'>" . htmlentities($key) . "</a> [<a target='_blank' href='http://mix-n-match.toolforge.org/#/search/{$k3}'>MnM</a>], normalised from: " ;
	asort ( $names[$key] ) ;
	$first = true ;
	foreach ( $names[$key] AS $name => $cnt ) {
		if ( !$first ) print "; " ;
		$first = false ;
		print "<i>" . htmlentities($name) . "</i> <tt>($cnt&times;)</tt>" ;
	}
	print "</p>" ;
	print "<p>Paintings: " ;
	foreach ( $qs AS $q ) print "<a href='https://www.wikidata.org/wiki/$q' target='_blank' style='margin-right:5px;'>$q</a> " ;
	print "</p>" ;
	print "<p><form method='post' target='_blank' action='https://tools.wmflabs.org/quickstatements/api.php' class='form-inline painter'>" ;
	print "<textarea style='display:none' name='data'>" ;
	foreach ( $qs AS $q ) print "$q\tP170\tREPLACEME\n" ;
	print "</textarea>" ;
	print "<input type='hidden' name='action' value='import' />" ;
	print "<input type='hidden' name='temporary' value='1' />" ;
	print "<input type='hidden' name='openpage' value='1' />" ;
	print "<input type='hidden' name='submit' value='1' />" ;
	print "<input type='text' placeholder='Q of creator' size='30' class='form-control creator-q' />" ;
	print "<input type='submit' value='Add this creator to " . count($qs) . " paintings' class='btn btn-outline-primary' title='This will launch QuickStatements in a new window.'/>" ;
	print "</form></p>" ;
	print "</section>" ;
}

?>
<p>End of list.</p>

<script>
$('form.painter').submit ( function (event) {
	var o = $(this) ;
	event.preventDefault(); //this will prevent the default submit
	var q = $(o.find('input.creator-q')).val().toUpperCase() ;
	if ( !q.match(/^Q\d+$/) ) {
		alert ( "Please use a creator item ID in the form of Qxxx" ) ;
		return false ;
	}
	var list = $(o.find('textarea')).val() ;
	list = list.replace ( /REPLACEME/g , q ) ;
	$(o.find('textarea')).val(list) ;
	o.unbind('submit').submit(); // continue the submit unbind preventDefault
} ) ;
</script>
<?PHP

//print "<pre>" ; print_r ( $names ) ; print "</pre>" ;
//print "<pre>" ; print_r ( $painters ) ; print "</pre>" ;

?>