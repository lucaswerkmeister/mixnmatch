<?PHP

#error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # |E_ALL
#ini_set('display_errors', 'On');

ini_set('memory_limit','2500M');
set_time_limit ( 60 * 5 ) ; // Seconds
ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '100M');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$api = new MixNMatch\API ;

$query = $api->get_request ( 'query' ) ;
$callback = $api->get_request ( 'callback' , '' ) ;
$api->log_use ( $query ) ;
$out = $api->query ( $query ) ;
$api->render ( $out , $callback ) ;

?>
