<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once dirname(__DIR__) . '/vendor/autoload.php';

$mnm = new MixNMatch\MixNMatch () ;

header('Content-type: application/json');

$callback = $_REQUEST['callback'] ;
$out = [] ;

if ( $_REQUEST['action'] == 'desc' ) {

	$in = 'in' ;
	$title = "Mix'n'match game" ;
	if ( $mnm->tfc->getRequest('mode','') == 'person' ) {
		$title = "Mix'n'match people game" ;
		$in = "of a person in" ;
	}

	$out = [
		"label" => [ "en" => $title ] ,
		"description" => [ "en" => "Verify that an entry $in an external catalog matches a given Wikidata item. Decisions count as mix'n'match actions!" ] ,
		"icon" => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Bipartite_graph_with_matching.svg/120px-Bipartite_graph_with_matching.svg.png' ,
		'options' => [
			[ 'name' => 'Entry type' , 'key' => 'type' , 'values' => [ 'any' => 'Any' , 'person' => 'Person' , 'not_person' => 'Not a person' ] ] #  , 'location' => 'Location'
		]
	] ;

} else if ( $_REQUEST['action'] == 'tiles' ) {

	// GET parameters
	$num = $mnm->tfc->getRequest('num',5)*1 ; // Number of games to return
	$lang = $mnm->tfc->getRequest('lang','en') ; // The language to use, with 'en' as fallback; ignored in this game
	$type = $mnm->tfc->getRequest('type','') ;
	
	$catalogs = [] ;
	$catalog_conditions = "wd_prop is not null and wd_qual is null AND `active`=1 AND id NOT IN (80,150)" ; // Hard-excluding some catalogs
	$sql = "SELECT * FROM catalog WHERE {$catalog_conditions}" ;
	$result = $mnm->getSQL ( $sql ) ;
	while ( $o = $result->fetch_object() ) $catalogs[''.$o->id] = $o ;
#	$cid = implode ( ',' , array_keys($catalogs) ) ;
	
	for ( $n = 1 ; $n <= $num ; $n++ ) {
		$r = rand() / getrandmax() ;
		$sql = "SELECT * FROM entry where user=0 AND ext_url!='' AND random>=$r AND " . $mnm->descriptionIsNotEmptySQL() ;
		if ( $type == 'person' ) $sql .= " AND entry.type='Q5'" ;
		if ( $type == 'not_person' ) $sql .= " AND entry.type!='Q5'" ;
#		if ( $type == 'location' ) $sql .= " AND entry.type='location'" ;
		$sql .= " HAVING catalog IN (SELECT id FROM catalog WHERE {$catalog_conditions})" ;
		$sql .= " ORDER BY random LIMIT 1" ;
		$result = $mnm->getSQL ( $sql ) ;
		$o = $result->fetch_object() ;
		$o->wd_prop = $catalogs[$o->catalog]->wd_prop ;
		$o->name = $catalogs[$o->catalog]->name ;

		$q = 'Q'.$o->q ;
		$p = 'P'.$o->wd_prop ;

		$g = [
			'id' => $o->id ,
			'sections' => [] ,
			'controls' => []
		] ;
		
		$g['sections'][] = [ 'type' => 'text' , 'title' => $o->ext_name , 'url' => $o->ext_url , 'text' => $o->ext_desc."\n[from ".$o->name." catalog]" ] ;
		$g['sections'][] = [ 'type' => 'item' , 'q' => $q ] ;
		$g['controls'][] = [
			'type' => 'buttons' ,
			'entries' => [
				[ 'type' => 'green' , 'decision' => 'yes' , 'label' => 'Yes' , 'api_action' => ['action'=>'wbcreateclaim','entity'=>$q,'property'=>$p,'snaktype'=>'value','value'=>json_encode($o->ext_id) ] ] ,
				[ 'type' => 'white' , 'decision' => 'skip' , 'label' => 'Skip' ] ,
				[ 'type' => 'yellow' , 'decision' => 'n_a' , 'label' => 'N/A' , 'shortcut' => 'n' ] ,
				[ 'type' => 'blue' , 'decision' => 'no' , 'label' => 'No' ]
			]
		] ;
		
		$out[] = $g ;
		
	}

} else if ( $_REQUEST['action'] == 'log_action' ) {

	$user = $mnm->tfc->getRequest('user','') ;
	$entry_id = $mnm->tfc->getRequest('tile',-1)*1 ;
	$decision = $mnm->tfc->getRequest('decision','') ;

	$uid = $mnm->getOrCreateUserID ( $user ) ;
	
	$sql = '' ;
	if ( $decision == 'yes' ) {
		$q = $mnm->getItemForEntryID ( $entry_id ) ;
		$mnm->setMatchForEntryID ( $entry_id , $q , $uid ) ;
	} else if ( $decision == 'no' ) {
		$mnm->removeMatchForEntryID ( $entry_id , $uid ) ;
	} else if ( $decision == 'n_a' ) {
		$mnm->setMatchForEntryID ( $entry_id , -1 , $uid ) ;
	}
} else {
	$out['error'] = "No valid action!" ;
}

print $callback . '(' ;
print json_encode ( $out ) ;
print ")\n" ;

?>