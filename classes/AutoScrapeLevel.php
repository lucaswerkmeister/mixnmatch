<?PHP

namespace MixNMatch ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class AutoScrapeLevel {
	var $autoscrape , $level_number , $is_initialised ;
	
	function __construct ( $autoscrape , $ln ) {
		$this->autoscrape = $autoscrape ;
		$this->level_number = $ln ;
		$this->l = $autoscrape->j->levels[$ln] ;
		$this->is_initialised = true ;
		$this->reset() ;
	}

	public static function get_level_for_mode ( $mode , $autoscrape , $lk ) {
		if ( $mode == 'keys' ) return new AutoScrapeLevelKeys ( $autoscrape , $lk ) ;
		else if ( $mode == 'range' ) return new AutoScrapeLevelRange ( $autoscrape , $lk ) ;
		else if ( $mode == 'follow' ) return new AutoScrapeLevelFollow ( $autoscrape , $lk ) ;
		else if ( $mode == 'mediawiki' ) return new AutoScrapeLevelMediaWiki ( $autoscrape , $lk ) ;
	}
	
	public function getCurrentValue () {
		return '' ;
	}

	public function getCurrentString () {
		return '' ;
	}

	protected function isLastLevel () {
		return ($this->level_number+1 >= count($this->autoscrape->j->levels)) ;
	}
	
	protected function getState () {
		$ret = (object) $this->l ;
		return $ret ;
	}

	protected function log ( $msg = '' ) {
		if ( !$this->autoscrape->testing ) return ;
		$s = "{$this->level_number} (" . get_class($this) . "): $msg [" . json_encode($this->getState()) . "]" ;
		$this->autoscrape->log ( $s ) ;
	}

	protected function reset () {
		if ( !$this->is_initialised ) return ;
		$this->log ( "Reset" ) ;
		if ( $this->isLastLevel() ) return ;
		$nl = $this->getNextLevel() ;
		if ( $nl ) $nl->reset() ;
	}
	
	protected function setDefault ( $k , $v ) {
		if ( isset($this->l->$k) ) return ;
		$this->l->$k = $v ;
	}
	
	public function modifyURL ( $url ) {
		return $url ;
	}
	
	protected function hasKnownEnd () {
		return true ;
	}
	
	protected function getNextLevel () {
		if ( !isset($this->autoscrape->levels[$this->level_number+1]) ) {
			return false ;
		}
		return $this->autoscrape->levels[$this->level_number+1] ;
	}
	
	protected function iterateOne () { // Returns true if everything is OK
		return true ;
	}

	public function next () { // Returns true if everything is OK
		$this->log ( "Next" ) ;
		if ( $this->isLastLevel() ) return $this->iterateOne() ;
		$nl = $this->getNextLevel() ;
		if ( $nl->next() ) return true ;
		$ret = $this->iterateOne() ;
//		$nl->reset() ;
		return $ret ;
	}
}


// __________________________________________________________________________________________________________________________________________________________________


class AutoScrapeLevelKeys extends AutoScrapeLevel {
	var $pos ;

	function __construct ( $autoscrape , $ln ) {
		parent::__construct($autoscrape,$ln);
	}
	
	protected function reset () {
		$this->pos = 0 ;
		parent::reset();
	}

	public function getCurrentValue () {
		return $this->pos ;
	}

	public function getCurrentString () {
		return $this->l->keys[$this->pos] ;
	}

	protected function getState () {
		$ret = parent::getState() ;
		$ret->pos = $this->pos ;
		return $ret ;
	}

	protected function iterateOne () { // Returns true if everything is OK
		$this->pos++ ;
//print "{$this->level_number} : {$this->pos} of " . count($this->l->keys) . "\n" ;
		if ( $this->pos >= count($this->l->keys) ) {
			$this->reset() ;
			return false ;
		}
		return true ;
	}

	public function modifyURL ( $url ) {
		$url = str_replace ( '$'.($this->level_number+1) , $this->l->keys[$this->pos] , $url ) ;
		return $url ;
	}
}

// __________________________________________________________________________________________________________________________________________________________________


class AutoScrapeLevelRange extends AutoScrapeLevel {
	var $pos ;

	function __construct ( $autoscrape , $ln ) {
		parent::__construct($autoscrape,$ln);
		$this->setDefault ( 'step' , 1 ) ;
	}

	public function getCurrentValue () {
		return $this->pos ;
	}

	protected function getState () {
		$ret = parent::getState() ;
		$ret->pos = $this->pos ;
		return $ret ;
	}

	public function getCurrentString () {
		return $this->pos ;
	}
	
	protected function reset () {
		$this->pos = $this->l->start*1 ;
		parent::reset();
	}

	protected function iterateOne () { // Returns true if everything is OK
		$this->pos += $this->l->step * 1 ;
		if ( $this->pos >= $this->l->end*1 ) {
			$this->reset() ;
			return false ;
		}
		return true ;
	}

	public function modifyURL ( $url ) {
		$url = str_replace ( '$'.($this->level_number+1) , $this->pos , $url ) ;
		return $url ;
	}
}

// __________________________________________________________________________________________________________________________________________________________________


class AutoScrapeLevelFollow extends AutoScrapeLevel {
	var $current_results ;
	var $pos ;

	function __construct ( $autoscrape , $ln ) {
		parent::__construct($autoscrape,$ln);
	}
	
	protected function reset () {
		$this->pos = 0 ;
		$this->current_results = [] ;
		$this->doDependentIteration() ;
		parent::reset();
	}

	public function getCurrentValue () {
		return $this->pos ;
	}

	public function getCurrentString () {
		return $this->current_results[$this->pos] ;
	}

	protected function getState () {
		$ret = parent::getState() ;
		$ret->pos = $this->pos ;
		return $ret ;
	}

	protected function iterateOne () { // Returns true if everything is OK
		$this->pos++ ;
		if ( $this->pos >= count($this->current_results) ) {
			$this->reset() ;
			return false ;
		}
		return true ;
	}

	public function modifyURL ( $url ) {
		if ( count($this->current_results) > $this->pos ) {
			$url = str_replace ( '$'.($this->level_number+1) , $this->current_results[$this->pos] , $url ) ;
		}
		return $url ;
	}


	protected function doDependentIteration () { // Depends on previous
		// Construct next URL
		$url = $this->l->url ;
		foreach ( $this->autoscrape->levels AS $num => $v ) {
			if ( $this->level_number == $num ) break ;
			$url = $v->modifyURL ( $url ) ;
		}


		$h = @file_get_contents ( $url ) ;
		$slash = '/' ;
		$r = $slash . str_replace ( $slash , '\\'.$slash , $this->l->rx ) . $slash  ;
		if ( !preg_match_all ( $r , $h , $m ) ) return false ;
	
		foreach ( $m[1] AS $v ) {
			$v = trim ( $v ) ;
			if ( $v == '' ) continue ;
			if ( in_array ( $v , $this->current_results ) ) continue ;
			$this->current_results[] = $v ;
		}

		return false ;
	}

}


// __________________________________________________________________________________________________________________________________________________________________

// TESTING: http://www.dreadnoughtproject.org/tfs/api.php?action=query&list=allpages&apnamespace=0&aplimit=500&format=jsonfm


class AutoScrapeLevelMediaWiki extends AutoScrapeLevel {
	var $current_results ;
	var $pos ;
	var $apfrom ;

	function __construct ( $autoscrape , $ln ) {
		parent::__construct($autoscrape,$ln);
		$this->internalIterate() ;
	}
	
	protected function reset () {
		$this->pos = 0 ;
		$this->current_results = [] ;
//		$this->doDependentIteration() ;
		parent::reset();
	}

	public function getCurrentValue () {
		return $this->pos ;
	}

	public function getCurrentString () {
		return $this->current_results[$this->pos] ;
	}

	protected function getState () {
		$ret = parent::getState() ;
		$ret->pos = $this->pos ;
		if ( isset($this->apfrom) ) $ret->apfrom = $this->apfrom ;
		else $ret->apfrom = '' ;
		return $ret ;
	}

	protected function iterateOne () { // Returns true if everything is OK
		$this->pos++ ;
		if ( $this->pos >= count($this->current_results) ) {
			return $this->internalIterate() ;
		}
		return true ;
	}

	public function modifyURL ( $url ) {
		$url = str_replace ( '$'.($this->level_number+1) , urlencode(str_replace(' ','_',$this->getCurrentString())) , $url ) ;
		return $url ;
	}


	protected function internalIterate () {
		$max_batch = 500 ;
		$this->reset() ;
		$url = $this->l->url ;
//		$view_url = preg_replace ( '/api\.php$/' , 'index.php' , $url ) ;
		$url .= '?action=query&list=allpages&apnamespace=0&aplimit='.$max_batch.'&format=json' ;
		$url .= '&apfilterredir=nonredirects';
		if ( isset($this->apfrom) ) $url .= "&apfrom=" . myurlencode($this->apfrom) ;

//print "{$url}\n" ;
		$h = @file_get_contents ( $url ) ;
		$this->current_results = [] ;
		$j = json_decode ( $h ) ;
		foreach ( $j->query->allpages AS $p ) $this->current_results[] = $p->title ; //$base_url . urlencode ( str_replace ( ' ' , '_' , $p->title ) ) ;
		
		$qc = 'query-continue' ;
		if ( isset ( $j->$qc ) and isset ( $j->$qc->allpages ) ) {
			if ( isset ( $j->$qc->allpages->apcontinue ) ) $this->apfrom = $j->$qc->allpages->apcontinue ;
			else if ( isset ( $j->$qc->allpages->apfrom ) ) $this->apfrom = $j->$qc->allpages->apfrom ;
		} else unset ( $this->apfrom ) ;
		
		if ( count($this->current_results) == 0 or count($this->current_results) < $max_batch ) return false ;
		return true ;
	}

}

?>