<?PHP

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class Jobs {
	public $mnm ;
	protected $script_dir = '/data/project/mix-n-match/scripts' ;
	protected $job_id_running = '' ;
	protected $max_concurrent_jobs = 4 ;

	function __construct( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		$this->max_concurrent_jobs = $this->mnm->use_persistent_connection ? 20 : 5 ;
	}

	public function get_running_job_id() {
		return $this->job_id_running ;
	}

	public function get_next_job() {
		if ( $this->count_running_jobs() >= $this->max_concurrent_jobs ) return ; # Too many jobs running already, next time
		$ret = $this->get_next_high_priority_job();
		if ( !isset($ret) ) $ret = $this->get_next_dependent_job() ;
		if ( !isset($ret) ) $ret = $this->get_next_initial_job() ;
		if ( !isset($ret) ) $ret = $this->get_next_low_priority_job() ;
		if ( !isset($ret) ) $ret = $this->get_next_scheduled_job() ;
		return $ret ;
	}

	public function run_job_command(&$job) {
		$parameters = '' ;
		if ( isset($job->json) and $job->json!='' ) {
			$j = json_decode($job->json) ;
			if ( is_array($j) ) $parameters = implode ( ' ' , $j ) ; # parameters in JSON array
			# TODO else
		}

		$script_path1 = "{$this->script_dir}/{$job->action}.php";

		if ( $job->action == 'update_person_dates' ) {
			$this->run_command ( $job , "{$this->script_dir}/person_dates/update_person_dates.php {$job->catalog} {$parameters}" ) ;
		} else if ( $job->action == 'autoscrape' ) {
			$this->run_command ( $job , "{$this->script_dir}/autoscrape.php run {$job->catalog} force {$parameters}" ) ;
		} else if ( $job->action == 'match_person_dates' ) {
			$this->run_command ( $job , "{$this->script_dir}/match_person_entries_by_name_and_dates.php {$job->catalog} {$parameters}" ) ;
		} else if ( file_exists($script_path1) ) { # Try standard ones
			$this->run_command ( $job , "{$script_path1} {$job->catalog} {$parameters}" ) ; # TODO is this really safe?
		} else { # Nope
			$this->mnm->set_job_status($job,'FAILED');
		}
	}


	protected function get_next_dependent_job() {
		$sql = "SELECT * FROM `jobs` WHERE `status`='TODO' AND `depends_on` IS NOT NULL AND `depends_on` IN (SELECT `id` FROM `jobs` WHERE `status`='DONE') ORDER BY last_ts LIMIT 1" ;
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}

	protected function get_next_initial_job() {
		$sql = "SELECT * FROM `jobs` WHERE `status`='TODO' AND `depends_on` IS NULL ORDER BY `last_ts` LIMIT 1" ;
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}

	protected function count_running_jobs() {
		$sql = "SELECT count(*) AS `cnt` FROM `jobs` where `status`='RUNNING'" ;
		return $this->mnm->getSQL($sql)->fetch_object()->cnt*1 ;
	}

	protected function get_next_scheduled_job() {
		$ts = $this->mnm->getCurrentTimestamp() ;
		$sql = "SELECT * FROM `jobs` WHERE `status`='DONE' AND `next_ts`!='' AND `next_ts`<='{$ts}' ORDER BY `next_ts` LIMIT 1" ;
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}

	protected function get_next_high_priority_job() {
		$sql = "SELECT * FROM `jobs` WHERE `status`='HIGH_PRIORITY' AND `depends_on` IS NULL ORDER BY `last_ts` LIMIT 1" ;
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}

	protected function get_next_low_priority_job() {
		$sql = "SELECT * FROM `jobs` WHERE `status`='LOW_PRIORITY' AND `depends_on` IS NULL ORDER BY `last_ts` LIMIT 1" ;
		return $this->mnm->getSQL($sql)->fetch_object() ;
	}

	protected function run_command(&$job,$cmd) {
		$this->mnm->set_job_status($job,'RUNNING');
		$ts = $this->mnm->getCurrentTimestamp();
		$this->mnm->dbmConnect(false);
		$this->log ( "{$ts} : RUN {$cmd}" ) ;
		$this->job_id_running = $job ;
		$result = exec(trim($cmd));
		$this->mnm->dbmConnect(true);
		$this->job_id_running = '' ;
		if ( $result === false ) {
			$this->mnm->set_job_status($job,'FAILED');
		} else {
			$this->mnm->set_job_status($job,'DONE');
		}
	}

	protected function log ( $msg ) {
		print "{$msg}\n" ;
	}

}

?>