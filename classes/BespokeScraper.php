<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

# ________________________________________________________________________________________________________________________
# BespokeScraper
#
# Code requires functions object_generator() and process_object($o,$html)
# function object_generator() yields "add entry" objects, NOT entry objects
# object_generator must be first, process_object second

final class BespokeScraper extends Helper {
	//protected $rewrite_functions = [ 'ml'=>'try_get_three_letter_month' , 'dp'=>'parse_date' ] ;
	protected $cf_function = 'BESPOKE_SCRAPER' ;
	private $php ;
	private $tmp_file ;
	private $reading_from_tmp_file = false ;
	private $list_len = 0 ;

	/**
	 * Constructor
	 *
	*/
	public function __construct ( /*int*/ $catalog , $mnm = '' ) {
		parent::__construct($catalog,$mnm);
		if (($key = array_search('file_get_contents', $this->dangerous_php)) !== false) unset($this->dangerous_php[$key]);
	}


	/**
	 * Tries to convert an entry object into birth/death dates
	 *
	 * @params object $o an "add entry" object, NOT an entry object
	 * @return array of Command
	*/
	public function processEntry ( $o , $existing_ids = [] ) {
		# Run code fragment
		$php = $this->ensurePHP() ;
		if ( !isset($php) ) throw new \Exception(__METHOD__.': No code fragment loaded' ) ;
		$catalog = $this->catalog ;
		$php = $php->process_object ;
		if ( FALSE === eval($php) ) throw new \Exception(__METHOD__.": Error in code fragment:\n{$php}\n");
		$ret = [] ;
		if ( !isset($entries) ) return $ret ;
		foreach ( $entries AS $entry ) {
			if ( !isset($entry->id) or !isset($entry->name) ) continue ;
			if ( isset($existing_ids[$entry->id]) ) continue ;
			if ( !isset($entry->catalog) ) $entry->catalog = $this->catalog ;
			$ret[] = Command::addEntry($entry) ;
		}
		return $ret ;
	}

	private function ensurePHP () {
		if ( isset($this->php) ) return $this->php ;
		$php = $this->rewriteGlobalMethods ( $this->code_fragment->php ) ;
		if ( !preg_match('|^(.*)(\bfunction\s+process_object.*)$|s',$php,$m) ) return ;
		$php = (object) [ 'object_generator'=>$m[1] , 'process_object'=>$m[2] ] ;

		$php->process_object = "if(!is_callable('process_object')){" .
		$php->process_object . '
}
if ( isset($html) ) unset($html) ;
if ( isset($o->html) ) $html = $o->html ;
else if ( isset($o->url) ) $html = file_get_contents ( $o->url ) ;
if ( isset($html) ) {
	$entries = process_object ( $o , $html ) ;
}';

		$php->object_generator = "if(!is_callable('object_generator')){" .
		$php->object_generator . '
}
foreach ( object_generator() AS $o ) {
	if ( is_array($o) ) $o = (object) $o ;
	if ( !is_object($o) ) continue ;
	if ( !isset($o->url) and !isset($o->html) ) continue ;
	if ( isset($o->id) && isset($existing_ids[$o->id]) ) continue ;
	$o->catalog = $this->catalog ;
	$this->addToList($o);
	#if ( isset($o->id) ) $existing_ids[$o->id] = 1 ;
}';

		$this->php = $php ;

		return $this->php ;
	}

	private function addToList ( $o ) {
		if ( !isset($this->tmp_file) ) {
			$this->tmp_file = tmpfile() ;
			$this->list_len = 0 ;
		}
		$line = json_encode($o) . "\n" ;
		fwrite ( $this->tmp_file , $line ) ;
		$this->list_len++ ;
	}

	private function getFromList() {
		if ( !isset($this->tmp_file) ) return ;
		if ( !$this->reading_from_tmp_file ) {
			fseek ( $this->tmp_file , 0 ) ;
			$this->reading_from_tmp_file = true ;
		}
		if (($line = fgets($this->tmp_file)) !== false) {
			$o = json_decode(trim($line)) ;
			return $o ;
		} else {
			fclose ( $this->tmp_file ) ;
			$this->reading_from_tmp_file = false ;
			unset ( $this->tmp_file ) ;
		}
	}

	private function getListLen() {
		return $this->list_len ;
	}

	public function processCatalog ( $testing = false ) {
		if ( !isset($this->code_fragment) ) throw new \Exception(__METHOD__.': No code fragment loaded' ) ;
		$php = $this->ensurePHP() ;
		$php = $php->object_generator ;

		$catalog = $this->catalog ;
		$to_check = [] ;
		$existing_ids = [] ;
		$sql = "SELECT ext_id FROM entry WHERE catalog={$catalog}" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while ($o = $result->fetch_object()) $existing_ids[$o->ext_id] = 1 ;


		if ( $testing ) print "Starting new scrape (" . count($existing_ids) . " will be skipped)\n" ;
		if ( isset($GLOBALS['object_generator']) ) unset($GLOBALS['object_generator']);
		if ( FALSE === eval($php) ) throw new \Exception(__METHOD__.": Error in code fragment:\n{$php}\n");

		$this->mnm->dbmConnect(true) ; # Reset DB connection, possible timeout

		print "Processing " . $this->getListLen() . " new entries\n"  ;
		$entries_were_changed = false ;
		while ( 1 ) {
			$o = $this->getFromList() ;
			if ( !isset($o) ) break ;
			try {
				$commands = $this->processEntry($o,$existing_ids);
			} catch (Exception $e) {
				print "{$e}\n" ;
			}
			if ( !isset($commands) or count($commands)==0 ) continue ;
			if ( $testing ) print_r($commands) ;
			else $this->enact ( $commands ) ;
			$entries_were_changed = true ;
		}
		if ( !$testing ) $this->touchCodeFragment() ;
		return $entries_were_changed ;
	}

}

?>