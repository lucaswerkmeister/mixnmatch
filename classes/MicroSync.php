<?PHP

namespace MixNMatch ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class MicroSync {
	public $mnm ;
	public $qs = [] ;
	protected $ext_cache = [] ;
	protected $logs = [] ;
	protected $last_catalog ;

	function __construct ( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
	}

	public function init_catalogs () {
		$sql_ignore_catalogs = " AND id NOT IN (506)" ; # Hard ignore, because data problem in source
		$this->catalogs = [] ;
		$sql = "SELECT * FROM catalog WHERE `active`=1" ;
		$sql .= $sql_ignore_catalogs ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $this->catalogs[$o->id] = $o ;

		# Check all catalogs with property
		$todo = [] ;
		$sql = "SELECT * FROM catalog WHERE `active`=1 AND wd_prop is not null and wd_qual is null" ;
		$sql .= $sql_ignore_catalogs ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $todo[$o->id] = $o->wd_prop ;
		return $todo ;
	}

	public function get_random_catalog () {
		return $this->catalogs[array_rand($this->catalogs)]->id ;
	}

	public function finalize ($specific_catalog) {
		# Run QS
		if ( $this->last_catalog != 150 ) {
			$msg = 'mixnmatch:microsync' ;
			if ( $specific_catalog ) $msg .= ' from catalog '.$this->last_catalog ;
			$this->mnm->tfc->getQS($msg,'',true) ;
		}

		$this->generateWikidataReportsFromLogs() ;
		$this->updateWikidataReports() ;
	}

	protected function getLinkedExternalID ( $catalog , $extid ) {
		if ( !isset ( $this->ext_cache[$catalog] ) ) {
			$this->ext_cache = [] ; # Clears all old catalogs
			$this->ext_cache[$catalog] = [] ;
			$sql = "SELECT ext_id,ext_url FROM entry WHERE catalog=$catalog" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()){
				if ( !isset($o->ext_url) or $o->ext_url == null ) continue ;
				$this->ext_cache[$catalog][$o->ext_id] = $o->ext_url ;
			}
		}
		
		$extid_nolink = preg_replace ( '/[\[\]]/' , '' , $extid ) ;
		if ( isset ( $this->ext_cache[$catalog][$extid] ) ) return "[" . $this->ext_cache[$catalog][$extid] . " $extid_nolink]" ;
		return "''$extid''" ; # Fallback
	}

	protected function formatLabel ( $label ) {
		$label = trim ( $label ) ;
		if ( !preg_match ( '/[a-z]/' , $label ) ) $label = ucwords ( strtolower ( $label )  ) ; # First letter uppercase, if no lower-case letters detected
		$label = preg_replace ( '/ +/' , ' ' , $label ) ; # Remove multiple spaces
		return '"' . $label . '"' ;
	}

	public function checkCatalog ( $catalog , $prop ) {
		$this->last_catalog = $catalog ;
		$case_insensitive = in_array ( $prop , Auxiliary::$props_also_use_lowercase ) ;
		$dbwd = $this->mnm->tfc->openDB ( 'wikidata' , 'wikidata' , true ) ;
		$lang = $this->catalogs[$catalog]->search_wp ;
		$extid2q = [] ;
		$sql = "SELECT * FROM entry WHERE catalog=$catalog" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			if ( $case_insensitive ) $o->ext_id = strtolower($o->ext_id) ;
			$extid2q[$o->ext_id] = $o ;
		}

		
		$hadthat = [] ;
		$sparql = "SELECT ?item ?value ?p31 { ?item wdt:P$prop ?value OPTIONAL { ?item wdt:P31 ?p31 } } ORDER BY ?item" ;
		$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
		if ( !isset($j) or !isset($j->results) or !isset($j->results->bindings) ) throw new Exception(__METHOD__.": No/bad SPARQL reply for\n{$sparql}" ) ; 
		
		# Fix deleted/redirected items
		$items = [] ;
		foreach ( $j->results->bindings AS $d ) {
			if ( !preg_match ( '/\/Q(\d+)$/' , $d->item->value , $m ) or $d->item->type != 'uri' or $d->value->type != 'literal' ) continue ;
			$q = $m[1] ;
			$items[] = "Q$q" ;
		}
		
		$use_q = [] ;
		if ( count($items) > 0 ) {
			$sql = "SELECT DISTINCT page_title FROM page WHERE page_namespace=0 AND page_is_redirect=0 AND page_title IN ('" . implode("','",$items)."')" ;
			unset($items) ;
			$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
			while($o = $result->fetch_object()) $use_q[$o->page_title] = $o->page_title ;
		}
		
		$wd_p31 = [] ;
		foreach ( $j->results->bindings AS $d ) {
			if ( !preg_match ( '/\/Q(\d+)$/' , $d->item->value , $m ) or $d->item->type != 'uri' or $d->value->type != 'literal' ) continue ;
			$q = $m[1] ;
			$v = $d->value->value ;
			if ( $case_insensitive ) $v = strtolower($v) ;
			if ( !isset($use_q["Q$q"]) ) continue ;
			$hadthat[$v][$q] = $q ;

			if ( isset($d->p31) ) $wd_p31[$v] = preg_replace ( '/^.+\//' , '' , $d->p31->value ) ;
			
			if ( !isset($extid2q[$v]) ) {
				$this->logs['Unknown external ID'][$catalog][] = "External ID [[Q$q|" . $this->getLinkedExternalID($catalog,$v) . "]] is not in Mix'n'match" ;
				continue ;
			}

			if ( $q == 0 or $q == -1 ) continue ; # Paranoia
			
			if ( $extid2q[$v]->q == $q ) { # All right!
				if ( $extid2q[$v]->user == 0 ) { # Confirmed in Wikidata, set in mix'n'match
					$this->mnm->setMatchForEntryID ( $extid2q[$v]->id , $q , 4 , true ) ;
				}
				continue ;
			}
			
			if ( $extid2q[$v]->q == -1 ) { # Set in Wikidata, but not in Mix'n'match.
				$this->mnm->setMatchForEntryID ( $extid2q[$v]->id , $q , 4 , true ) ;
				$extid2q[$v]->q = $q ; # To not create a new item additionally later
				continue ;
			}

			if ( $extid2q[$v]->q != $q ) { # Mismatch
				if ( $extid2q[$v]->q== null or $extid2q[$v]->q <= 0 or $extid2q[$v]->user == 0 ) { # Just the automatcher, overwrite
					$this->mnm->setMatchForEntryID ( $extid2q[$v]->id , $q , 4 , true ) ;
				} else {
					$this->logs["Mismatch between Wikidata and Mix'n'match"][$catalog][] = "Wikidata says the external ID " . $this->getLinkedExternalID($catalog,$v) . " belongs to {{Q|$q}}, but [{$this->mnm->root_url}/#/entry/".$extid2q[$v]->id." mix'n'match] says {{Q|" . $extid2q[$v]->q . "}}" ;
				}
			}
			
		}
		
		# Find multiple ID usage
		foreach ( $hadthat AS $v => $h ) {
			if ( count($h) == 1 ) continue ;
			$this->logs['Multiple items with same external ID'][$catalog][] = "Multiple Wikidata items with external ID " . $this->getLinkedExternalID($catalog,$v) . " : {{Q|" . implode('}}, {{Q|',$h) . "}}" ;
		}

		# Find missing usage
		$to_check = [] ;
		foreach ( $extid2q AS $extid => $o ) {
			$q = $o->q ;
			if ( preg_match ( '/^fake_id/' , $extid ) ) continue ;
			if ( $o->user == 0 ) continue ;
			if ( !isset($q) or $q == null or $q <= 0 ) continue ;
			if ( isset($hadthat[$extid]) ) continue ;
			if ( isset($wd_p31[$extid]) and $o->type == 'Q5' and $wd_p31[$extid] != 'Q5' ) continue ;
			$to_check["Q$q"] = $extid ;
		}

		if ( count($to_check) > 0 ) {
			$to_check2 = "'" . implode ( "','" , array_keys($to_check) ) . "'" ;
			$skip = [] ;
			
			$exists = [] ;
			$sql = "SELECT page_title FROM page WHERE page_title IN ($to_check2) AND page_namespace=0" ;
			$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
			while($o = $result->fetch_object()) $exists[$o->page_title] = 1 ;
			
			$sql = "select page_title,rev_comment from revision_compat,page where page_title IN ($to_check2) and rev_page=page_id AND page_namespace=0 AND rev_comment LIKE '%[[Property:P$prop]]%'" ;
			$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
			while($o = $result->fetch_object()) {
				$q = $o->page_title ;
				$c = $o->rev_comment ;
				if ( !isset($to_check[$q]) ) continue ;
				$check = ']]: ' . $to_check[$q] ;
				if ( FALSE === stristr ( $c , $check ) ) continue ;
				$skip[$q] = 1 ;
			}
			
			# Single value only, enforced for all properties here!
			$sql = "SELECT page_title FROM page,pagelinks WHERE page_title IN ($to_check2) AND page_namespace=0 AND pl_from=page_id AND pl_namespace=120 AND pl_title='P$prop'" ;
			$result = $this->mnm->tfc->getSQL ( $dbwd , $sql ) ;
			while($o = $result->fetch_object()) {
				$q = $o->page_title ;
				$skip[$q] = 1 ;
			}

			$msg = "/* mixnmatch:microsync for catalog {$catalog} */" ;
			
			foreach ( $to_check AS $q => $extid ) {
				if ( isset($skip[$q]) ) continue ;
				if ( !isset($exists[$q]) ) continue ;
				$this->qs[] = "$q\tP$prop\t\"$extid\"$msg" ;
			}
		}

		if ( 0 ) { # Create items for entries marked as "not in Wikidata"
			foreach ( $extid2q AS $o ) {
				if ( $o->q != -1 ) continue ;
				if ( preg_match ( '/^fake_id/' , $o->ext_id ) ) continue ;
				$this->qs[] = "CREATE" ;
				$this->qs[] = "LAST\tL$lang\t" . $this->formatLabel($o->ext_name) . $msg ;
				if ( $lang != 'en' and $o->type == 'Q5' ) $this->qs[] = "LAST\tLen\t" . $this->formatLabel($o->ext_name) . $msg;
				if ( $o->type == 'Q5' ) $this->qs[] = "LAST\tP31\tQ5$msg" ;
				$this->qs[] = "LAST\tP$prop\t\"{$o->ext_id}\"$msg" ;
			}
		}
	}

	# Update Wikidata report
	protected function generateWikidataReportsFromLogs () {
		global $wiki_texts , $wiki_header , $timestamp ;
		$max_lines = 400 ;
		$timestamp = date ( 'Ymd' ) ;
		$wiki_texts = [] ;
		$wiki_header = "A report for the [{$this->mnm->root_url}/ Mix'n'match] tool. '''This page will be replaced regularly!'''\n" ;
		foreach ( $this->logs as $title => $v0 ) {
			$first = true ;
			foreach ( $v0 AS $catalog => $lines ) {
				if ( !isset($wiki_texts[$catalog]) ) {
					$wiki_texts[$catalog] = $wiki_header ;
					$wiki_texts[$catalog] .= "''Please note: If you fix something from this list on Wikidata, please fix it on Mix'n'match as well, if applicable. Otherwise, the error might be re-introduced from there.''\n" ;
					$wiki_texts[$catalog] .= "==[{$this->mnm->root_url}/#/catalog/$catalog " . $this->catalogs[$catalog]->name . "]==" ;
					$wiki_texts[$catalog] .= "\n" . $this->catalogs[$catalog]->desc ;
				}
				if ( $first ) { $wiki_texts[$catalog] .= "\n== $title ==\n" ; $first = false ; }
				if ( count($lines) > $max_lines ) {
					$wiki_texts[$catalog] .= "* " . count($lines) . " entries for this, not showing\n" ;
				} else {
					foreach ( $lines AS $l ) $wiki_texts[$catalog] .= "# $l\n" ;
				}
			}
		}
	}

	# Update single catalog report page on Wikidata
	protected function updateCatalog ( $new_wikitext , $catalog , $timestamp ) {
		$ini = parse_ini_file ( '/data/project/mix-n-match/bot.ini' ) ;
		$wiki_user = $ini['user'] ;
		$wiki_pass = $ini['pass'] ;

		$page_title = "User:Magnus Manske/Mix'n'match report" ;
		if ( $catalog > 0 ) $page_title .= '/' . $catalog ;

		$revison_comment = "Update {$timestamp}" ;
		$api_url = 'https://www.wikidata.org/w/api.php' ;

		$this->mnm->loginToWiki ( $api_url , $wiki_user , $wiki_pass ) ;
		$this->mnm->setWikipageText ( $api_url , $page_title , $new_wikitext , $revison_comment ) ;
	}

	# Update all reports and the main report page
	protected function updateWikidataReports () {
		global $wiki_header , $wiki_texts , $timestamp ;
		$wiki = $wiki_header ;
		$wiki .= "\n''red links indicate there never was an issue with that catalog''\n" ;
		$wiki .= "\n{| class='wikitable'" ;
		$wiki .= "\n!Catalog!!Report!!Wikidata Property" ;
		foreach ( $this->catalogs AS $catalog => $v ) {
			if ( !isset($v->wd_prop) or isset($v->wd_qual) ) continue ;
			$wiki .= "\n|-" ;
			$wiki .= "\n|[{$this->mnm->root_url}/#/catalog/catalog=$catalog #$catalog]" ;
			$wiki .= "\n|[[/$catalog|{$v->name}]]" ;
			if ( isset($v->wd_prop) and !isset($v->wd_qual) ) $wiki .= "\n|".'{{'."P|{$v->wd_prop}".'}}' ;
			else $wiki .= "||" ;
			if ( !isset($wiki_texts[$catalog]) ) continue ;
			$this->updateCatalog ( $wiki_texts[$catalog] , $catalog , $timestamp ) ;
		}
		$wiki .= "\n|}\n" ;
		$this->updateCatalog ( $wiki , 0 , $timestamp ) ;
	}


}

?>