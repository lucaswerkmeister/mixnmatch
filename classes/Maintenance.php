<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class Maintenance {
	public $mnm ;
	private $testing = false ;

	function __construct ( $mnm = '', $testing = false ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		$this->testing = $testing ;
	}

	public function fixHTMLentitiesForNamesInCatalog ( $catalog ) {
		# Find all suitable catalogs:
		# SELECT * FROM catalog WHERE active=1 AND exists (SELECT * FROM entry WHERE entry.catalog=catalog.id AND ext_name like "%&%;%")
		$catalog *= 1 ;
		if ( $catalog <= 0 ) return ;
		$sql = "SELECT * FROM `entry` WHERE `catalog`={$catalog} AND `ext_name` LIKE '%&%;%'" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$name = trim ( html_entity_decode ( $o->ext_name ) ) ;
			if ( $name == $o->ext_name ) continue ;
			$sql = "UPDATE `entry` SET `ext_name`='" . $this->mnm->escape($name) . "' WHERE `id`={$o->id}" ;
			$this->mnm->getSQL ( $sql ) ;
		}
	}

	public function removeMultiMatchForManualMatches () {
		$sql = "DELETE FROM multi_match WHERE EXISTS (SELECT * FROM entry WHERE entry_id=entry.id AND q IS NOT NULL AND user IS NOT NULL and user>0)" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	public function fixupWdMatches () {
		# matches in deactivated catalogs must go
		$sql = "DELETE FROM wd_matches WHERE catalog IN (SELECT id FROM catalog WHERE active!=1)" ;
		$this->mnm->getSQL ( $sql ) ;

		# In case any matches were added with catalog=0
		$sql = "UPDATE wd_matches SET catalog=(SELECT entry.catalog FROM entry WHERE entry.id=entry_id) WHERE wd_matches.catalog=0" ;
		$this->mnm->getSQL ( $sql ) ;

		# matches in catalogs without property/with qualifier
		$sql = "UPDATE wd_matches SET status='N/A' WHERE status!='N/A' AND catalog IN (SELECT id FROM catalog WHERE wd_prop IS NULL or wd_qual IS NOT NULL)" ;
		$this->mnm->getSQL ( $sql ) ;

		# TODO remove matches for deleted entries
	}


	public function createItemFromMultipleEntries ( $entry_ids , $entry_name = '' , $min_commands = 7 , $min_cnt = 10 ) {
		global $lc ;

		$entries = [] ;
		foreach ( $entry_ids AS $entry_id ) {
			try {
				$entry = new MixNMatch\Entry ( $entry_id , $this->mnm ) ;
				$e = $entry->core_data() ;
			} catch (\Exception $e) {
				continue ;
			}

			if ( $entry_name == '' ) $entry_name = $e->ext_name ;
			if ( isset($e->q) AND isset($e->user) AND $e->q !== null AND $e->q !== FALSE AND $e->user>0 ) {
				throw new \Exception(__METHOD__." {$this->mnm->root_url}/#/entry/{$entry->id} ({$entry_name}) already has a match" ) ;
			}
			$entries[] = $e ;
		}
		
		$merged_commands = [] ;
		foreach ( $entries AS $entry ) {
			if ( isset($lc) ) $commands = $this->mnm->getCreateItemForEntryCommands ( $entry , $lc ) ;
			else $commands = $this->mnm->getCreateItemForEntryCommands ( $entry ) ;
			if ( !is_array($commands) ) {
				throw new \Exception(__METHOD__." Failed to assemble commands for an entry of {$entry_name}" ) ;
			}
			foreach ( $commands AS $command ) {
				if ( $command == 'CREATE' ) continue ;
				if ( !preg_match ( '|^LAST\t(\S+)\t(.+)$|' , $command , $m ) ) continue ;
				$cmd1 = $m[1] ;
				$cmd2 = $m[2] ;
				if ( preg_match ( '|^(.+?)\t(.+)$|' , $cmd2 , $m ) ) {
					$cmd1 .= "\t" . trim($m[1]) ;
					$cmd2 = $m[2] ;
				}
				if ( !isset($merged_commands[$cmd1]) ) $merged_commands[$cmd1] = [] ;
				if ( preg_match ( '|^[LDA].*$|' , $cmd1) ) $merged_commands[$cmd1] = [ $cmd1 => $cmd2 ] ; # Only one
				else $merged_commands[$cmd1][$cmd2] = $cmd2 ;
			}
		}

		$mc2 = [ "CREATE" ] ;
		foreach ( $merged_commands AS $command => $parts ) {
			$mc2[] = trim ( "LAST\t{$command}\t" . implode ( "\t" , $parts ) ) ;
		}
		$merged_commands = $mc2 ;

		if ( count($merged_commands) < $min_commands ) { # Skip small ones
			throw new \Exception(__METHOD__." < {$min_commands} commands from {$entry_name}" ) ;
		}

		// Create and run commands
		$q = $this->mnm->tfc->runCommandsQS ( $merged_commands ) ;
		if ( isset($q) and $q!==FALSE and $q!=null and $q!='' ) {
			foreach ( $entries AS $entry ) {
				$this->mnm->setMatchForEntryID ( $entry->id , $q , 4 , true ) ;
				$touched_catalogs[$entry->catalog] = 1 ;
			}
		} else {
			throw new \Exception(__METHOD__." Creation of {$entry_name} failed" ) ;
		}
		return $q ;
	}


	// unused but should
	public function fixJobsWithNextTsButNoRepeat () {
		$sql = 'UPDATE jobs SET next_ts="" WHERE repeat_after_sec IS NULL AND next_ts!=""' ;
		$this->mnm->getSQL ( $sql ) ;
	}
}

?>