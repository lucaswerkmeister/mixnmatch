<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class Auxiliary {
	public $catalog_blacklist = [
		506
	] ;
	public $catalog_property_blacklist = [ # Catalog ID, property ID
		[2099,"P428"]
	] ;
	public $property_blacklist = [ 
		'P233' ,
		'P235' ,  # See https://www.wikidata.org/wiki/Topic:Ue8t23abchlw716q
		'P846' ,
		'P2528' ,
		'P4511'
	] ;
	public static $props_also_use_lowercase = [2002] ;

	public $mnm ;
	private $testing = false ;
	public $properties_that_have_external_ids ;
	private $properties_using_items ;

	function __construct ( $testing = false , $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		$this->testing = $testing ;
	}

	public function isCatalogPropertyCombinationSuspect ( $catalog_id , $property ) {
		$this->mnm->sanitizeQ ( $catalog_id ) ;
		$this->mnm->sanitizeQ ( $property ) ;
		$property = "P{$property}" ;
		foreach ( $this->catalog_property_blacklist AS $cp ) {
			if ( $catalog_id == $cp[0] and $property == $cp[1] ) return true ;
		}
		return false ;
	}

	public function matchEntriesViaAuxiliary ( $catalog , $use_random = false , $batch_size = 0 ) {
		$r = rand()/getrandmax()  ;
		$sql = "SELECT entry_id,aux_p,aux_name,catalog,auxiliary.id AS aux_id FROM entry,auxiliary WHERE entry.id=entry_id AND (q is null or user=0)";
		if ( $use_random ) $sql .= " AND random>=$r" ;
		if ( $catalog != 0 ) $sql .= " AND catalog={$catalog}" ;
		if ( count($this->catalog_blacklist) > 0 ) $sql .= " AND catalog NOT IN (" . implode(',',$this->catalog_blacklist) . ")" ;
		if ( $use_random ) $sql .= " ORDER BY random" ;
		if ( $batch_size > 0 ) $sql .= " LIMIT {$batch_size}" ;

		$results = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $results[] = $o ;

		$batchsize = 30 ;
		$grouped = [] ;
		foreach ( $results AS $o ) {
			$p = $o->aux_p ;
			if ( $this->isCatalogPropertyCombinationSuspect ( $catalog , $p ) ) continue ;

			if ( !isset($grouped[$o->catalog]) ) $grouped[$o->catalog] = [] ;
			if ( !isset($grouped[$o->catalog][$p]) ) $grouped[$o->catalog][$p] = [ [] ] ;
			$lg = count($grouped[$o->catalog][$p])-1 ;
			if ( count($grouped[$o->catalog][$p][$lg]) >= $batchsize ) $grouped[$o->catalog][$p][++$lg] = [] ;
			$grouped[$o->catalog][$p][$lg][] = $o ;
		}


		$catalogs_updated = [] ;
		$ts = $this->mnm->getCurrentTimestamp() ;

		foreach ( $grouped AS $catalog => $prop_groups ) {
			foreach ( $prop_groups AS $prop => $groups ) {
				$also_use_lowercase = in_array($prop,self::$props_also_use_lowercase) ;
				foreach ( $groups AS $group ) {
					$ids = [] ;
					$aux2entry = [] ;
					foreach ( $group AS $o ) {
						if ( preg_match ( '/"/' , $o->aux_name ) ) continue ;
						$an = trim($o->aux_name) ;
						$ids[] = '"' . $an . '"' ;
						$aux2entry[$an] = $o->entry_id ;
						if ( $also_use_lowercase and strtolower($an)!=$an ) {
							$an = strtolower($an);
							$ids[] = '"' . $an . '"' ;
							$aux2entry[$an] = $o->entry_id ;
						}
					}
					$sparql = "SELECT ?q ?id WHERE { VALUES ?id { " . implode(' ',$ids) . " } ?q wdt:P$prop ?id }" ;
					$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
					foreach ( $j->results->bindings AS $b ) {
						$id = $b->id->value ;
						$q = $this->mnm->tfc->parseItemFromURL ( $b->q->value ) ;

						if ( !isset($aux2entry[$id]) ) {
							$this->log ( "Failed lookup aux2entry for {$prop}:'{$id}'\n" ) ;
							continue ;
						}
						$entry_id = $aux2entry[$id] ;

						# Check if this is the only item with that property/value
						$search_result_items = $this->mnm->getCachedWikidataSearch ( '' , $prop , $id ) ;
						if ( count($search_result_items) > 1 ) {
							$this->log ( "WD items with {$prop}:'{$id}': " . json_encode($search_result_items) , 2 ) ;
							//$this->mnm->addIssue ( $o->id , 'WD_DUPLICATE' , $search_result_items ) ;
							continue ;
						}

						# Set new match
						$this->log ( $this->mnm->getEntryURL($entry_id) . " => https://www.wikidata.org/wiki/{$q} because {$prop}:'{$id}'" , 2 ) ;
						$this->mnm->setMatchForEntryID ( $entry_id , $q , 4 , true , false ) ;
						$catalogs_updated[$catalog] = 1 ;
					}
				}
			}

		}

		foreach ( $catalogs_updated AS $catalog_id => $dummy ) {
			$catalog = new Catalog ( $catalog_id , $this->mnm ) ;
			$catalog->updateStatistics() ;
			$this->mnm->queue_job($catalog_id,'aux2wd');
		}
	}

	public function addAuxiliaryToWikidata ( int $catalog ) {
		$this->initializePropertyLists() ;

		$id2q = [] ;
		$sql = '' ;
		if ( $catalog > 0 ) {
			$sql = "SELECT id,q FROM entry WHERE catalog=$catalog AND user>0 AND q IS NOT NULL" ;
			$this->mnm->loadCatalog($catalog,true);
		} else {
			$r = rand()/getrandmax()  ;
			$sql = "SELECT id,q FROM entry WHERE random>=$r AND user>0 AND q IS NOT NULL AND catalog NOT IN (655) ORDER BY random LIMIT 20000" ;
		}
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $id2q[$o->id] = $o->q ;

		$this->log ( "ID2Q : ".count($id2q) , 2 ) ;
		if ( count($id2q) == 0 ) return ; // Nothing to do

		$aux = [] ;
		$sources = [] ;
		$prop_ids = [] ;
		foreach ( $this->property_blacklist AS $prop ) {
			$this->mnm->sanitizeQ ( $prop ) ;
			$prop_ids[] = $prop ;
		}
		$sql = "SELECT * FROM vw_aux WHERE id IN (" . implode(',',array_keys($id2q)) . ") AND in_wikidata=0" ;
		$sql .= " AND aux_p NOT IN (" . implode ( ',' , $prop_ids ) . ")" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$q = 'Q'.$id2q[$o->id] ;
			if ( $q == 'Q0' ) continue ;

			if ( $this->isCatalogPropertyCombinationSuspect ( $o->catalog , $o->aux_p ) ) continue ;
			$aux[$q][] = [ 'p' => 'P'.$o->aux_p , 'v' => $o->aux_name , 'aux_id' => $o->aux_id , 'entry_id' => $o->id ] ;
			if ( $o->ext_url != '' ) $sources[$q] = $o->ext_url ;
		}
		unset ( $id2q ) ;

		$this->log ( "AUX : ".count($aux) , 2 ) ;
		if ( count($aux) == 0 ) return ; // Nothing to do

		$aux_chunked = array_chunk ( $aux , 50 , true ) ;

		foreach ( $aux_chunked AS $chunk_num => $aux ) {
			$this->log ( "Chunk #{$chunk_num}" , 2 ) ;
			$wil = new \WikidataItemList ;
			$wil->loadItems ( array_keys ( $aux ) ) ;

			$commands = [] ;
			foreach ( $aux AS $q => $values ) {
				$i = $wil->getItem ( $q ) ;
				if ( !isset($i) ) {
					$this->mnm->addIssue ( $values['entry_id'] , 'ITEM_DELETED' , [$q] ) ;
					continue ;
				}

				$source = '' ;
				if ( isset($sources[$q]) ) {
					$source = "\tS854\t\"" . $sources[$q] . '"' ;
					# Deactivated as per https://www.wikidata.org/wiki/Topic:V2tl70hbgsvundhx
					#$source .= "\tS813\t" . $this->mnm->date2expression(date('Y-m-d')) ;
				}

				foreach ( $values AS $v ) {
					$p = $v['p'] ;
					if ( $i->hasClaims($p) ) continue ;


					$value = trim($v['v']) ;
					$alt = array ( $value ) ;
					if ( $p == 'P856' ) {
						$value = preg_replace ( '/\/$/' , '' , $value ) ;
						$alt[] = preg_replace('/^.+?:/','http:',$value) ;
						$alt[] = preg_replace('/^.+?:/','https:',$value) ;
						$alt[] = preg_replace('/^.+?:/','http:',"$value/") ;
						$alt[] = preg_replace('/^.+?:/','https:',"$value/") ;
					}

					$exists = false ;
					foreach ( $alt AS $a ) {
						if ( in_array ( strtolower($a) , $existing ) ) $exists = true ;
					}

					if ( !$exists and in_array ( $p , $this->properties_that_have_external_ids) ) {
						$search_results = $this->mnm->getSearchResults ( '' , $p , $value ) ;
						if ( isset($search_results) and count($search_results) > 0 ) {
							$sql = '' ;
							if ( count($search_results) == 1 and $search_results[0]->title == $q ) {
								// Already have that
								$sql = "UPDATE auxiliary SET in_wikidata=1 WHERE id={$v['aux_id']} AND in_wikidata=0" ;
							} else {
								$items = [] ;
								foreach ( $search_results AS $sr ) {
									if ( $q != $sr->title ) $items[] = $sr->title ;
								}
								if ( count($items) == 1 ) {
									$this->mnm->addIssue($v['entry_id'],'MISMATCH',[$q,$items[0]]);
								} else if ( count($items) > 0 ) {
									$this->mnm->addIssue($v['entry_id'],'WD_DUPLICATE',$items);
								}
							}
							if ( $sql != '' ) $this->mnm->getSQL($sql) ;
							$exists = true ;
						}
					}

					if ( $exists ) continue ;

					if ( $this->mnm->hasPropertyEverBeenRemovedFromItem ( $q , $p ) ) {
						$this->log ( "hasPropertyEverBeenRemovedFromItem: {$q}/{$p}" , 2 ) ;
						continue ;
					}

					if ( in_array ( $p , $this->properties_using_items ) ) {
						$commands[] = "{$q}\t{$p}\t{$value}{$source}" ;
					} else if ( in_array ( $p , $this->property_blacklist ) ) {
						// Don't use
					} else if ( $p == 'P571' ) {
						$commands[] = "{$q}\t{$p}\t{$value}{$source}" ;
						$this->log ( json_encode($commands) , 2 ) ; // TODO what is this?
						return;
					} else if ( in_array ( $p , ['P625'] ) ) {
						if ( preg_match ( '/^\@[0-9\.\-]+\/[0-9\.\-]+$/' , $value ) ) $commands[] = "{$q}\t{$p}\t{$value}{$source}" ;
						else $commands[] = "{$q}\t{$p}\t@" . str_replace ( ',' , '/' , $value ) . $source ;
					} else if ( $p == 'P973' ) {
						if ( isset($sources[$q]) and $sources[$q] == $value ) {
							$commands[] = "{$q}\t{$p}\t\"{$value}\"" ; # No source, is its own source
						} else {
							$commands[] = "{$q}\t{$p}\t\"{$value}\"{$source}" ;
						}
					} else $commands[] = "{$q}\t{$p}\t\"{$value}\"{$source}" ;
				}
			}
		}

		if ( count($commands) == 0 ) return ; // Nothing to do

		$qs = $this->mnm->tfc->getQS('mixnmatch:aux2wd','/data/project/mix-n-match/bot.ini',true) ;
		$this->mnm->tfc->runCommandsQS ( $commands , $qs ) ;
	}

	public function initializePropertyLists () {
		# Get properties with item type
		if ( !isset($this->properties_using_items) ) {
			$sparql = 'SELECT ?p WHERE { ?p rdf:type wikibase:Property; wikibase:propertyType wikibase:WikibaseItem }' ;
			$this->properties_using_items = $this->mnm->tfc->getSPARQLitems($sparql,'p');
		}

		# Get properties with ExternalId type
		if ( !isset($this->properties_that_have_external_ids) ) {
			$sparql = 'SELECT ?p WHERE { ?p rdf:type wikibase:Property; wikibase:propertyType wikibase:ExternalId }' ;
			$this->properties_that_have_external_ids = $this->mnm->tfc->getSPARQLitems($sparql,'p');
		}
	}

	private function log ( $message , $level = 1 ) {
		if ( !$this->testing and $level > 1 ) return ;
		print "{$message}\n" ;
	}
}

?>