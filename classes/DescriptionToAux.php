<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

# ________________________________________________________________________________________________________________________
# DescriptionToAux

final class DescriptionToAux extends Helper {
	protected $rewrite_functions = [
		'setCity'=>'setCity' , 
		'checkAgainstList'=>'checkAgainstList' , 
		'checkGermanOccupations'=>'checkGermanOccupations' ,
		'setPersonDates' => 'setPersonDates' ,
		'setLocation' => 'setLocation' ,
		'setAlias' => 'setAlias' ,
		'setMatch' => 'setMatch' ,
		'setAux' => 'setAux'
	] ;
	protected $cf_function = 'AUX_FROM_DESC' ;
	private $city_cache = [
		"Amsterdam" => [ "Q727" ] ,
		"Rome" => [ "Q220" ] ,
		"Cologne" => [ "Q365" ] ,
		"New York" => [ "Q60" ] ,
		"Milan" => [ "Q490" ] ,
		"Berlin" => [ "Q64" ] ,
		"Dusseldorf" => [ "Q1718" ] ,
	] ;
	private $labels = [] ;
	private $misc = [] ; # Generic key-value; catalog-dependent
	private $lists = [
		'countries'=>['sparql'=>'SELECT DISTINCT ?q ?label { ?q wdt:P31 wd:Q7275 ; rdfs:label ?label }','property'=>'P27'] ,
		'occupations'=>['sparql'=>'SELECT DISTINCT ?q ?label { ?q wdt:P31|wdt:P279 wd:Q28640 ; rdfs:label ?label }','property'=>'P106'] ,
		'nationalities'=>['sparql'=>'SELECT ?q ?label {  VALUES ?instance { wd:Q3624078 wd:Q6256 } .  ?q wdt:P31 ?instance ; wdt:P1549 ?label .  FILTER ( lang(?label) = "en" )  }','property'=>'P27'],
	#	'ioc_codes'=>['sparql'=>'SELECT ?q ?label { ?q wdt:P984 ?label }','property'=>'P27'],
	] ;
	private $p_nationality = 27 ;
	private $p_gender = 21 ;
	private $p_birth_place = 19 ;
	private $p_death_place = 20 ;
	private $p_occupation = 106 ;
	private $p_isni = 213 ;
	private $p_viaf = 214 ;
	private $q_male = 'Q6581097' ;
	private $q_female = 'Q6581072' ;
	private $q_artist = 'Q483501' ;
	private $german_occupations = [
		'|\bPolitiker|i' => 'Q82955' ,
		'|\bArchitekt|i' => 'Q42973' ,
		'|\bSchauspieler|i' => 'Q33999' ,
		'|\bMusiker|i' => 'Q639669' ,
		'|\bHistoriker|i' => 'Q201788' ,
		'|\bKünstler|i' => 'Q483501' ,
		'|\bSänger|i' => 'Q177220' ,
		'|\bSchriftsteller|i' => 'Q36180' ,
		'|\bJournalist|i' => 'Q1930187' ,
		'|\bKomponist|i' => 'Q36834' ,
		'|\bLehrer|i' => 'Q37226' ,
		'|\bMaler|i' => 'Q1028181' ,
		'|\b(Arzt|Ärztin)|i' => 'Q39631' ,
		'|\bNotar|i' => 'Q189010' ,
		'|\bDesigner|i' => 'Q5322166' ,
		'|\bBildhauer|i' => 'Q1281618' ,	
	] ;


	/**
	 * Constructor
	 *
	*/
	public function __construct ( int $catalog , $mnm = '' ) {
		parent::__construct($catalog,$mnm);
		$this->initializeLists();
	}

	public function initializeLists() {
		foreach ( $this->lists AS $group => $list_data ) {
			$this->labels[$group] = [] ;
			$j = $this->mnm->tfc->getSPARQL ( $list_data['sparql'] ) ;
			foreach ( $j->results->bindings AS $b ) {
				$label = strtolower ( $b->label->value ) ;
				$q = $this->mnm->tfc->parseItemFromURL ( $b->q->value ) ;
				if ( isset($this->labels[$group][$label]) and $this->labels[$group][$label] != $q ) $this->labels[$group][$label] = '' ;
				else $this->labels[$group][$label] = $q ;
			}
		}
	}

	public function rewriteGlobals ( $php ) {
		$php = $this->rewriteGlobalMethods ( $this->code_fragment->php ) ;
		$php = preg_replace ( '|\$([pq]_[a-z_]+)|' , '$this->$1' , $php ) ;
		return $php ;
	}

	public function processEntry ( $o ) {
		if ( !isset($this->code_fragment) ) throw new \Exception(__METHOD__.': No code fragment loaded' ) ;
		$this->current_results = [] ;
		$php = $this->rewriteGlobals ( $this->code_fragment->php ) ;
		if ( FALSE === eval($php) ) throw new \Exception(__METHOD__.": Error in code fragment:\n{$this->code_fragment->php}\n");
		return $this->current_results ;
	}

	private function setCity ( $o , $property , $text ) {
		$text = trim ( $text ) ;
		$qs = [] ;
		if ( isset($this->city_cache[$text]) ) {
			$qs = $this->city_cache[$text] ;
		} else {
			$p31s = ['Q5119','Q1549591','Q515','Q1093829','Q484170','Q493522'] ;
			foreach ( $p31s AS $p31 ) {
				$qs = $this->mnm->getCachedWikidataSearch("inlabel:{$text} haswbstatement:P31={$p31}") ;
				if ( count($qs) > 0 ) break ;
			}
			$this->city_cache[$text] = $qs ;
		}
		if ( count($qs) != 1 ) return ;
		$this->setAux ( $o->id , $property , $qs[0] ) ;
	}

	private function checkAgainstList ( $entry_id , $list , $text , $property = '' ) {
		$text = trim ( strtolower ( $text ) ) ;
		if ( $property == '' ) $property = $this->lists[$list]['property'] ; # Default property
		if ( isset($this->labels[$list][$text]) and $this->labels[$list][$text] != '' ) {
			$this->setAux ( $entry_id , $property , $this->labels[$list][$text] ) ;
		}
	}

	private function checkGermanOccupations ( $o ) {
		if ( $o->type != 'Q5' ) return ; # Paranoia
		foreach ( $this->german_occupations AS $pattern => $q_occupation ) {
			if ( preg_match ( $pattern , $o->ext_desc , $m ) ) $this->setAux ( $o->id , 106 , $q_occupation ) ;
		}
	}

	private function logTest ( $msg ) {
		print "{$msg}\n" ; # TODO record for tests
	}

}


?>