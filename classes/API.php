<?PHP

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class SetCompare {
	protected $mnm ;
	protected $table_name ;
	protected $catalog ;
	protected $wd_cache = [] ;
	protected $bad_qs_to_sync = [] ;

	function __construct ( $mnm , $catalog ) {
		$this->catalog = $catalog * 1 ;
		$this->mnm = $mnm ;
		$this->table_name = "MnM_SetCompare_{$this->catalog}" ;

		# Create temporary table
		$this->mnm->getSQL ( "DROP TEMPORARY TABLE IF EXISTS {$this->table_name}") ;
		$sql = "CREATE TEMPORARY TABLE {$this->table_name} ( " .
			"q INT UNSIGNED NOT NULL DEFAULT 0," .
			"ext_id varchar(128) DEFAULT ''," .
			"in_wd  INT UNSIGNED NOT NULL DEFAULT 0," .
			"in_mnm INT UNSIGNED NOT NULL DEFAULT 0," .
			"PRIMARY KEY (q,ext_id)," .
			"INDEX wd_mnm (in_wd,in_mnm)" .
			")" ;
		$this->mnm->getSQL($sql);
	}

	function __destruct() {
		$this->mnm->getSQL ( "DROP TEMPORARY TABLE IF EXISTS {$this->table_name}") ;
	}

	public function addWD ( $q , $ext_id ) {
		if ( !isset($q) or !isset($ext_id) ) return ;
		$ext_id = trim($ext_id) ;
		if ( $ext_id=='' ) return ;
		$q = preg_replace ( "|\D|" , "" , "$q" ) ;
		if ( $q=='' or $q=='0' ) return ;
		$q = $q*1;
		$ext_id = $this->mnm->escape($ext_id) ;
		$this->wd_cache[] = "{$q},\"{$ext_id}\",1" ;
		if ( count($this->wd_cache)>=10000 ) $this->flushWD() ;
	}

	public function flushWD() {
		if ( count($this->wd_cache)==0 ) return ;
		$sql = "INSERT IGNORE INTO {$this->table_name} (q,ext_id,in_wd) VALUES (" . implode("),(",$this->wd_cache) . ")" ;
		$this->mnm->getSQL($sql);
		$this->wd_cache = [] ;
	}

	public function addFromMnM() {
		$sql = "SELECT entry.q,entry.ext_id,1 FROM entry WHERE entry.q IS NOT NULL AND entry.q>0 AND entry.user!=0 AND entry.user IS NOT NULL AND entry.catalog={$this->catalog} AND entry.ext_id NOT LIKE 'fake_id_%'" ;
		$sql = "INSERT INTO {$this->table_name} (q,ext_id,in_mnm) {$sql} ON DUPLICATE KEY UPDATE in_mnm=1" ;
		$this->mnm->getSQL($sql);
	}

	public function get_mnm_dupes() {
		$ret = [] ;
		$sql = "SELECT q,group_concat(ext_id SEPARATOR \"\\n\") AS ext_ids,count(*) AS cnt FROM {$this->table_name} WHERE in_mnm=1 GROUP BY q HAVING cnt>1" ;
		$result = $this->mnm->getSQL($sql);
		while($o = $result->fetch_object()) {
			$ret[] = [ $o->q*1 , explode("\n",utf8_encode($o->ext_ids)) ] ;
			$this->bad_qs_to_sync[$o->q] = 1 ;
		}
		return $ret ;
	}

	public function get_diff() {
		$qs = [] ;
		$sql = "SELECT * FROM {$this->table_name} WHERE in_wd!=in_mnm" ;
		$result = $this->mnm->getSQL($sql);
		while($o = $result->fetch_object()) {
			$q = "{$o->q}" ;
			if ( !isset($qs[$q]) ) $qs[$q] = [ $q , [] , [] ] ;
			if ( $o->in_wd ) $qs[$q][1][] = utf8_encode($o->ext_id) ;
			if ( $o->in_mnm ) $qs[$q][2][] = utf8_encode($o->ext_id) ;
		}
		$ret = [] ;
		foreach ( $qs AS $k => $v ) $ret[] = $v ;
		return $ret ;
	}

	public function compare_wd_mnm ( $wd_value ) {
		$ret = [] ;
		$mnm_value = 1 - $wd_value ;
		$sql = "SELECT * FROM {$this->table_name} WHERE in_wd={$wd_value} AND in_mnm={$mnm_value}" ;
		$result = $this->mnm->getSQL($sql);
		while($o = $result->fetch_object()) {
			$q = "{$o->q}" ;
			if ( isset($this->bad_qs_to_sync[$q]) ) continue ;
			$ret[] = [ $q , utf8_encode($o->ext_id) ] ;
		}
		return $ret ;
	}
}

class API {
	protected $mnm ;
	protected $user ;
	protected $headers = [] ;
	protected $testing = false ;
	protected $prevent_callback = false ;
	protected $large_properties = ['214','268'] ;
	protected $content_type = 'Content-type: application/json; charset=UTF-8' ;
	const CONTENT_TYPE_TEXT_PLAIN = 'Content-type: text/plain; charset=UTF-8';
	const CONTENT_TYPE_TEXT_HTML = 'Content-type: text/html; charset=UTF-8';
	const CONTENT_TYPE_ATOM_XML = 'Content-type: application/atom+xml; charset=UTF-8';
	protected $query_blacklist = [ '' , 'catalog' , 'catalogs' , 'catalog_details' , 'get_user_info' , 'get_entries_by_q_or_value' , 'update_overview' , 'random' , 'redirect' , 'get_sync' , 'get_jobs' , 'get_entry' ] ;
	protected $code_fragment_allowed_user_ids = [ 2 ] ;

	function __construct ( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
	}

	public function query ( $query ) {
		$this->testing = isset($_REQUEST['testing']) ;
		$fn = 'query_'.$query ;
		if ( method_exists($this,$fn) ) {
			$this->user = $this->get_request ( 'tusc_user' , -1 ) ;
			$out = [ 'status' => 'OK' , 'data' => [] ] ;
			try {
				$this->$fn ( $out ) ;
			} catch (\Exception $e) {
				$out = $this->error($e->getMessage()) ;
			}
			return $out ;
		} else {
			if ( isset($_REQUEST['oauth_verifier']) ) return $this->oauth_validation() ;
			return $this->error ( "Unknown query '{$query}'" ) ;
		}
	}

	public function render ( $out , $callback = '' ) {
		header($this->content_type);
		foreach ( $this->headers AS $h ) header($h) ;
		if ( !$this->prevent_callback && $callback != '' ) print $callback.'(' ;
		if ( is_array($out) ) print json_encode ( $out ) ;
		else print $out ; # Raw string
		if ( !$this->prevent_callback && $callback != '' ) print ')' ;
		$this->mnm->tfc->flush();
		ob_end_flush() ;
		exit(0);
	}

	public function log_use ( $query ) {
		if ( preg_match('|^["\)\/]|',$query) or preg_match('/^(procedure|redirect|http)/',$query) ) exit(0);
		if ( !in_array ( $query , $this->query_blacklist ) ) $this->mnm->tfc->logToolUse ( '' , $query ) ;
	}

	################################################################################
	# Internal helper functions

	protected function error ( $error_message ) {
		return [ 'status' => $error_message ] ;
	}

	protected function get_escaped_request ( $varname , $default = '' ) {
		$ret = $this->get_request ( $varname , $default ) ;
		return $this->mnm->escape ( $ret ) ;
	}

	protected function get_catalog () {
		$catalog = $this->get_request ( 'catalog' , 0 ) * 1 ;
		if ( $catalog <= 0 ) throw new \Exception('Invalid catalog ID');
		return $catalog ;
	}

	protected function add_sql_to_out ( &$out , $sql , $subkey = '' , $out_key = '' ) {
		$result = $this->mnm->getSQL ( $sql ) ;
		if ( !isset($out['data']) ) $out['data'] = [] ;
		if ( $subkey != '' and !isset($out['data'][$subkey]) ) $out['data'][$subkey] = [] ;
		while($o = $result->fetch_object()) {
			if ( $out_key == '' ) {
				if ( $subkey == '' ) $out['data'][] = $o ;
				else $out['data'][$subkey][] = $o ;
			} else {
				if ( $subkey == '' ) $out['data'][$o->$out_key] = $o ;
				else $out['data'][$subkey][$o->$out_key] = $o ;
			}
		}
	}

	protected function add_entries_and_users_from_sql ( &$out , $sql , $associative = true ) {
		$users = [] ;
		$out['data']['entries'] = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			if ( $associative ) $out['data']['entries'][$o->id] = $o ;
			else $out['data']['entries'][] = $o ;
			if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
		}
		$out['data']['users'] = $this->get_users ( $users ) ;
	}

	protected function render_atom ( &$out ) {
		require_once ( "/data/project/mix-n-match/public_html/php/wikidata.php" ) ;

		$time = new DateTime;
		$ts = $time->format(DateTime::ATOM);
		$xml = '<?xml version="1.0" encoding="utf-8"?>

		<feed xmlns="http://www.w3.org/2005/Atom">
		<title>Mix\'n\'match</title>
		<subtitle>Recent updates by humans (auto-matching not shown)</subtitle>
		<link href="'.$this->mnm->root_url.'/api.php?query=rc_atom" rel="self" />
		<link href="'.$this->mnm->root_url.'/" />
		<id>urn:uuid:' . $this->mnm->getUUID() . '</id>
		<updated>' . $ts . '</updated>' ;


		# Get catalogs
		$catalogs = [] ;
		foreach ( $out['data']['events'] AS $k => $e ) $catalogs[$e->catalog] = $e->catalog ;
		if ( count($catalogs) > 0 ) {
			$tmp_catalog = new Catalog ( 0 , $this->mnm ) ;
			$catalogs = $tmp_catalog->loadCatalogs ( $catalogs ) ;
		}

		# Get wikidata items
		$items = [] ;
		foreach ( $out['data']['events'] AS $k => $e ) {
			if ( $e->event_type == 'match' and $e->q > 0 ) $items['Q'.$e->q] = 'Q'.$e->q ;
		}
		$wil = new WikidataItemList ;
		$wil->loadItems ( $items ) ;

		foreach ( $out['data']['events'] AS $k => $e ) {
			$xml .= "<entry>\n" ;
			$xml .= '<title>' ;
			if ( $e->event_type == 'match' ) $xml .= "New match for " ;
			else if ( $e->event_type == 'remove_q' ) $xml .= "Match was removed for " ;
			$xml .= '"' . htmlspecialchars($e->ext_name, ENT_XML1, 'UTF-8') . '"' ;
			$xml .= "</title>\n" ;
			$xml .= "<link rel=\"alternate\" href=\"{$this->mnm->root_url}/#/entry/{$e->id}\" />\n" ;
			$xml .= "<id>urn:uuid:" . $this->mnm->getUUID() . "</id>\n" ;
			$ts = new DateTime ( $e->timestamp ) ;
			$xml .= '<updated>' . $ts->format(DateTime::ATOM) . '</updated>' ;
			$xml .= "<content type=\"xhtml\"><div xmlns=\"http://www.w3.org/1999/xhtml\">" ;
	#		$xml .= "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/ANZAC_Parade_from_the_Australian_War_Memorial%2C_Canberra_ACT.jpg/320px-ANZAC_Parade_from_the_Australian_War_Memorial%2C_Canberra_ACT.jpg'/>" ;
			if ( $e->event_type == 'match' ) {
				if ( $e->q == 0 ) {
					$xml .= "<p>Entry was marked as <i>NOT ON WIKIDATA</i></p>" ;
				} else if ( $e->q == -1 ) {
					$xml .= "<p>Entry was marked as <i>NOT APPLICABLE</i></p>" ;
				} else {
					$img = '' ;
					$label = 'Q'.$e->q ;
					$i = $wil->getItem ( $label ) ;
					if ( isset($i) ) {
						$label = htmlspecialchars($i->getLabel(), ENT_XML1, 'UTF-8') ;

						if ( $i->hasClaims('P18') ) {
							$fn = $this->mnm->tfc->urlEncode($i->getFirstString('P18')) ;
							$url = "https://commons.wikimedia.org/wiki/Special:Redirect/file/{$fn}?width=160&height=160" ;
							$url = htmlspecialchars($url, ENT_XML1, 'UTF-8') ;
							$page_url = htmlspecialchars("https://commons.wikimedia.org/wiki/File:{$fn}", ENT_XML1, 'UTF-8') ;
							$img = "<a href='{$page_url}'><img border='0' src='{$url}' /></a>" ;
	#						$img = "<div style='float:right;margin-left:10px;'>{$img}</div>" ;
						}
					}
					$xml .= "<p>Entry was matched to <a href='https://www.wikidata.org/wiki/Q{$e->q}'>{$label}</a>" ;
					if ( $label != 'Q'.$o->q ) $xml .= " <small>[Q{$e->q}]</small>" ;
					$xml .= ".</p>" ;
					if ( $img != '' ) $xml .= "<p>{$img}</p>" ;
				}
			}
			if ( $e->ext_desc != '' ) {
				$xml .= '<p>Description: <i>' . htmlspecialchars($e->ext_desc, ENT_XML1, 'UTF-8') . "</i></p>\n" ;
			}
			$xml .= '<p>External ID: ' ;
			if ( $e->ext_url != '' ) $xml .= "<a href='{$e->ext_url}'>" ;
			$xml .= htmlspecialchars($e->ext_id, ENT_XML1, 'UTF-8') ;
			if ( $e->ext_url != '' ) $xml .= "</a>" ;
			$xml .= "</p>\n" ;
			if ( isset($catalogs[$e->catalog]) ) {
				$cat = $catalogs[$e->catalog] ;
				$xml .= "<p>In catalog #{$e->catalog}: <a href='{$this->mnm->root_url}/#/catalog/{$e->catalog}'>{$cat->name}</a>" ;
				if ( isset($cat->wd_prop) and !isset($cat->wd_qual) ) $xml .= ", set as property <a href='https://www.wikidata.org/wiki/P{$cat->wd_prop}'>P{$cat->wd_prop}</a>" ;
				$xml .= "</p>" ;
			}
			$xml .= "</div></content>\n" ;

			if ( isset($out['data']['users'][$e->user]) ) {
				$username = $out['data']['users'][$e->user]->name ;
				$xml .= '<author>' ;
				$xml .= '<name>' . htmlspecialchars($username, ENT_XML1, 'UTF-8') . '</name>' ;
				$xml .= "<uri>https://www.wikidata.org/wiki/User:" . urlencode($username) . "</uri>\n" ;
				$xml .= '</author>' ;
			}
			$xml .= "</entry>\n" ;
		}

		$xml .= '</feed>' ;

		$this->content_type = self::CONTENT_TYPE_ATOM_XML ;
		$out = $xml ;
	}


	protected function generateOverview ( &$out ) {
		$sql = "SELECT overview.* FROM overview,catalog WHERE catalog.id=overview.catalog and catalog.active>=1" ; // overview is "manually" updated, but fast; vw_overview is automatic, but slow
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			foreach ( $o AS $k => $v ) $out['data'][$o->catalog][$k] = $v ;
		}

		$tmp_catalog = new Catalog ( 0 , $this->mnm ) ;
		$catalogs = $tmp_catalog->loadCatalogs([],true) ;
		foreach ( $catalogs AS $catalog_id => $o ) {
			foreach ( $o AS $k => $v ) $out['data'][$catalog_id][$k] = $v ;
		}


		$sql = "SELECT user.name AS username,catalog.id from catalog,user where owner=user.id AND active>=1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			foreach ( $o AS $k => $v ) $out['data'][$o->id][$k] = $v ;
		}
		
		$sql = "select catalog.id AS id,last_update,do_auto_update,autoscrape.json AS json from catalog,autoscrape WHERE active>=1 AND catalog.id=autoscrape.catalog" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$out['data'][$o->id]['has_autoscrape'] = 1 ;
			if ( $o->do_auto_update != 1 ) continue ;
			$out['data'][$o->id]['scrape_update'] = 1 ;
			$out['data'][$o->id]['autoscrape_json'] = $o->json ;
			$lu = $o->last_update ;
			$lu = substr($lu,0,4).'-'.substr($lu,4,2).'-'.substr($lu,6,2).' '.substr($lu,8,2).':'.substr($lu,10,2).':'.substr($lu,12,2) ;
			$out['data'][$o->id]['last_scrape'] = $lu ;
		}
	}

	protected function oauth_validation () {
		require_once '/data/project/magnustools/public_html/php/Widar.php' ;
		$widar = new \Widar ( 'mix-n-match' ) ;
		$widar->attempt_verification_auto_forward ( $this->mnm->root_url ) ;
		throw new \Exception("No valid OAuth validation") ;
	}



	################################################################################
	# Queries

	protected function query_get_source_headers ( &$out ) {
		//$user_id = $this->check_and_get_user_id ( $this->get_request ( 'username' , '' ) ) ;
		$update_info = json_decode ( $this->get_request ( 'update_info' , '' ) ) ;
		$uc = new UpdateCatalog ( 0 , $this->mnm , $update_info ) ;
		$uc->setTesting ( true , false ) ;
		#print_r ( $uc->update_catalog() ) ;
		$out['data'] = $uc->get_header_row() ;
	}

	protected function query_test_import_source ( &$out ) {
		$update_info = json_decode ( $this->get_request ( 'update_info' , '' ) ) ;
		$uc = new UpdateCatalog ( 0 , $this->mnm , $update_info ) ;
		$uc->setTesting ( true , false ) ;
		$out['data'] = $uc->update_catalog() ;
	}

	protected function query_import_source ( &$out ) {
		#error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
		#ini_set('display_errors', 'On');
		$catalog_id = $this->get_request('catalog',0)*1 ;
		$user_id = $this->check_and_get_user_id ( $this->get_request ( 'username' , '' ) ) ;
		$seconds = $this->get_request ( 'seconds' , 0 ) * 1 ;
		$update_info = json_decode ( $this->get_request ( 'update_info' , '{}' ) ) ;
		$meta = json_decode ( $this->get_request ( 'meta' , '{}' ) ) ;

		if ( $catalog_id == 0 ) {
			if ( $update_info->default_type??'' == 'Q5' ) $meta->type = 'biography' ;
			$meta->note = 'Created via import' ;
			$catalog = new Catalog ( 0 , $this->mnm ) ;
			$catalog_id = $catalog->createNew ( $meta , $user_id ) ;
		}

		$uc = new UpdateCatalog ( $catalog_id , $this->mnm , $update_info ) ;
		$uc->store_update_info ( $user_id , 'Via web interface' ) ;
		$this->mnm->queue_job($catalog_id,'update_from_tabbed_file',0,'',$seconds,$user_id);
		$out['catalog_id'] = $catalog_id ;
	}

	protected function query_upload_import_file ( &$out ) {
		$user_id = $this->check_and_get_user_id ( $this->get_request ( 'username' , '' ) ) ;
		$data_format = strtolower ( $this->get_escaped_request ( 'data_format' , 'CSV' ) );
		$uuid = $this->mnm->getUUID() ;
		$out['uuid'] = $uuid ;
		$uc = new UpdateCatalog ( 0 , $this->mnm , [] ) ;
		$target_file = $uc->import_file_path($uuid) ;
		$uploaded_file = $_FILES["import_file"]["tmp_name"] ;
		if ( !move_uploaded_file ( $uploaded_file , $target_file ) ) {
			$post_max_size = ini_get('post_max_size');
			$upload_max_filesize = ini_get('upload_max_filesize');
			throw new \Exception("Could not move uploaded file. File size probably too large ({$post_max_size}/{$upload_max_filesize}).") ;
		}
		$ts = $this->mnm->getCurrentTimestamp() ;
		$sql = "INSERT IGNORE INTO `import_file` (`uuid`,`user`,`timestamp`,`type`) VALUES ('{$uuid}',{$user_id},'{$ts}','{$data_format}')" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	protected function query_get_entry_by_extid ( &$out ) {
		$catalog = $this->get_catalog() ;
		$ext_id = $this->get_escaped_request ( 'extid' , '' ) ;
		$this->add_sql_to_out ( $out , "SELECT * FROM entry WHERE catalog={$catalog} AND ext_id='{$ext_id}'" , 'entries' , 'id' ) ;
		$this->add_extended_entry_data($out) ;
	}

	protected function query_catalogs ( &$out ) {
		$this->generateOverview ( $out ) ;
	}

	protected function query_widar ( &$out ) {
		require_once '/data/project/magnustools/public_html/php/Widar.php' ;
		$widar = new \Widar ( 'mix-n-match' ) ;
		$widar->authorization_callback = $this->mnm->root_url.'/api.php' ;
		$widar->render_reponse ( true ) ;
		exit(0);
	}

	protected function query_catalog_overview ( &$out ) {
		$catalogs = explode ( ',' , $this->get_request ( 'catalogs' , '' ) ) ;
		foreach ( $catalogs AS $catalog_id ) {
			$catalog = new Catalog ( $catalog_id , $this->mnm ) ;
			try {
				$catalog->outputOverview ( $out ) ;
			} catch (\Exception $e) {
				$this->mnm->last_error = $e->getMessage() ;
			}
		}
	}

	protected function query_get_user_info ( &$out ) {
		$username = $this->get_escaped_request ( 'username' , '' ) ;
		$username = str_replace ( '_' , ' ' , $username ) ;
		$sql = "SELECT * FROM user WHERE name='{$username}' LIMIT 1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) $out['data'] = $o ;
		else throw new \Exception("No user '{$username}' found") ;
	}

	protected function query_add_aliases ( &$out ) {
		$user_id = $this->check_and_get_user_id ( $this->get_request ( 'username' , '' ) ) ;
		$text = trim ( $this->get_request ( 'text' , '' ) ) ;
		$catalog_id = $this->get_request ( 'catalog' , 0 ) * 1 ;
		if ( $catalog_id <= 0 or $text == '' ) throw new \Exception("Catalog ID or text missing") ;
		$catalog = $this->mnm->loadCatalog($catalog_id,true) ;
		$rows = explode ( "\n" , $text ) ;
		unset ( $text ) ;
		foreach ( $rows as $row ) {
			$parts = explode ( "\t" , trim ( $row ) ) ;
			if ( count($parts) < 2 or count($parts) > 3 ) continue ;
			if ( count($parts) < 3 ) $parts[] = '' ;
			if ( $parts[2] == '' ) $parts[2] = $catalog->data()->search_wp ; # Default language
			$ext_id = $this->mnm->escape ( trim($parts[0]) ) ;
			$label = $this->mnm->escape ( trim(str_replace('|','',$parts[1])) ) ;
			$language = $this->mnm->escape ( trim(strtolower($parts[2])) ) ;
			$subquery = "SELECT id FROM entry WHERE catalog={$catalog_id} AND ext_id='{$ext_id}' LIMIT 1" ;
			$sql = "INSERT IGNORE INTO `aliases` (entry_id,language,label,added_by_user) VALUES (({$subquery}),'{$language}','{$label}',{$user_id})" ;
			$this->mnm->getSQL($sql) ;
		}
	}

	protected function query_get_jobs ( &$out ) {
		$catalog_id = $this->get_request('catalog',0)*1 ; // 0 is valid here
		$max = $this->get_request('max',50)*1 ;
		$conditions = [] ;
		if ( $catalog_id > 0 ) $conditions[] = "catalog={$catalog_id}" ;
		$sql = "SELECT jobs.*,(SELECT user.name FROM user WHERE user.id=`jobs`.`user_id`) AS user_name FROM jobs" ;
		if ( count($conditions) > 0 ) $sql.= " WHERE (" . implode ( ") AND (" , $conditions ) . ")" ;
		$sql .= " ORDER BY FIELD(status,'RUNNING','FAILED','TODO','LOW_PRIORITY','PAUSED','DONE'), last_ts DESC,next_ts DESC" ;
		if ( $max > 0 ) $sql .= " LIMIT {$max}" ;
		$this->add_sql_to_out ( $out , $sql , '' ) ;
	}

	protected function query_start_new_job ( &$out ) {
		$catalog_id = $this->get_catalog();
		$action = trim(strtolower($this->get_request('action','')));
		if ( !preg_match('/^[a-z_]+$/',$action) ) throw new \Exception("Bad action: '{$action}'") ;
		$user_id = $this->check_and_get_user_id ( $this->get_request ( 'username' , '' ) ) ;

		# Default seconds
		$seconds = 0 ;
		if ( $action == 'autoscrape' ) $seconds = 2629800*3 ; # Three months

		# Save info from previous job, if any
		$job = $this->mnm->get_job($catalog_id,$action) ;
		if ( isset($job) ) $seconds = $job->seconds ;

		$this->mnm->queue_job ( $catalog_id , $action , 0 , '' , $seconds , $user_id ) ;
	}

	protected function query_update_overview ( &$out ) {
		$catalog = $this->get_request ( 'catalog' , '' ) ;
		if ( $catalog == '' ) $catalogs = $this->mnm->getAllCatalogIDs() ;
		else $catalogs = explode ( ',' , $catalog ) ;
		$this->mnm->updateCatalogs ( $catalogs ) ;
	}

	protected function query_sparql_list ( &$out ) {
		$out['data'] = ['entries'=>[],'users'=>[] ] ;

		$label2q = [] ;
		$labels = [] ;
		$sparql = $this->get_request ( 'sparql' , '' ) ;
		$j = $this->mnm->tfc->getSPARQL ( $sparql ) ;
		$vars = $j->head->vars ;
		$var1 = $vars[0] ;
		$var2 = $vars[1] ;
		foreach ( $j->results->bindings AS $b ) {
			$v1 = $b->$var1 ;
			$v2 = $b->$var2 ;
			if ( $v1->type == 'uri' and $v2->type == 'literal' ) {
				$q = preg_replace ( '/^.+\/Q/' , 'Q' , $v1->value ) ;
				$label = $v2->value ;
			} else if ( $v2->type == 'uri' and $v1->type == 'literal' ) {
				$q = preg_replace ( '/^.+\/Q/' , 'Q' , $v2->value ) ;
				$label = $v1->value ;
			} else continue ;
			$label2q[$label] = $q ;
			$labels[] = $this->mnm->escape ( $label ) ;
		}

		$sql = "SELECT * FROM entry WHERE (user=0 OR q is null) AND ext_name IN ('" . implode("','",$labels) . "')" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			if ( !isset($label2q[$o->ext_name]) ) continue ; // Paranoia
			$o->user = 0 ;
			$o->q = substr ( $label2q[$o->ext_name] , 1 ) * 1 ;
			$o->timestamp = '20180304223800' ;
			$out['data']['entries'][$o->id] = $o ;
		}


		$out['data']['users'] = $this->get_users ( [0] ) ;
	}

	protected function query_catalog_details ( &$out ) {
		$catalog = $this->get_catalog();
		$this->add_sql_to_out ( $out , "SELECT `type`,count(*) AS cnt FROM entry WHERE catalog=$catalog GROUP BY type ORDER BY cnt DESC" , 'type' ) ;
		$this->add_sql_to_out ( $out , "SELECT substring(timestamp,1,6) AS ym,count(*) AS cnt FROM entry WHERE catalog=$catalog AND timestamp IS NOT NULL AND user!=0 GROUP BY ym ORDER BY ym" , 'ym' ) ;		
		$this->add_sql_to_out ( $out , "SELECT name AS username,entry.user AS uid,count(*) AS cnt FROM entry,user WHERE catalog=$catalog AND entry.user=user.id AND user!=0 AND entry.user IS NOT NULL GROUP BY uid ORDER BY cnt DESC" , 'user' ) ;
	}

	protected function query_remove_all_multimatches ( &$out ) {
		$user_id = $this->check_and_get_user_id ( $this->user ) ;
		$entry_id = $this->get_request ( 'entry' , -1 ) * 1 ;
		$sql = "DELETE FROM `multi_match` WHERE entry_id={$entry_id}" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	protected function query_match_q ( &$out ) {
		$user_id = $this->check_and_get_user_id ( $this->user ) ;
		$entry = $this->get_request ( 'entry' , -1 ) * 1 ;
		$q = $this->get_request ( 'q' , -1 ) * 1 ;

		if ( !$this->mnm->setMatchForEntryID ( $entry , $q , $user_id , false ) ) throw new \Exception("Problem with setting the match: {$this->mnm->last_error}") ;
		$sql = "SELECT *,entry.type AS entry_type FROM entry,catalog WHERE entry.id=$entry and entry.catalog=catalog.id" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $out['entry'] = $o ;
	}

	protected function query_match_q_multi ( &$out ) {
		$catalog = $this->get_catalog() ;
		$user_id = $this->check_and_get_user_id ( $this->user ) ;
		$data = json_decode ( $this->get_request('data','[]') ) ;
		
		$out['not_found'] = 0 ;
		$out['no_changes_written'] = [] ;
		foreach ( $data AS $d ) {
			if ( $this->mnm->setMatchForCatalogExtID ( $catalog , $d[1] , $d[0] , $user_id , true , false ) ) continue ;
			if ( preg_match ( '/^External ID .* not found\.$/' , $this->mnm->last_error ) ) $out['not_found']++ ;
			else if ( preg_match ( '/^No changes written\.$/' , $this->mnm->last_error ) ) $out['no_changes_written'][] = [ 'ext_id' => $d[1] , 'new_q' => $d[0] , 'entry' => $this->mnm->last_entry ] ;
			else throw new \Exception("Problem with setting the match: {$this->mnm->last_error}") ;
		}
	}

	protected function query_remove_q ( &$out ) {
		$user_id = $this->check_and_get_user_id ( $this->user ) ;
		$entry_id = $this->get_request ( 'entry' , -1 ) * 1 ;
		if ( !$this->mnm->removeMatchForEntryID ( $entry_id ,  $user_id ) ) throw new \Exception($this->mnm->last_error) ;
	}

	protected function get_entry_object_from_id ( $entry_id ) {
		try {
			$entry = new Entry ( $entry_id , $this->mnm ) ;
			return $entry->core_data() ;
		} catch (Exception $e) {
			# Ignore, return undef
		} 
	}

	protected function query_remove_all_q ( &$out ) {
		$user_id = $this->check_and_get_user_id ( $this->user ) ;
		$entry_id = $this->get_request ( 'entry' , -1 ) * 1 ;
		$entry = $this->get_entry_object_from_id($entry_id);
		$catalog = $entry->catalog*1 ;
		$q = $entry->q*1 ;
		if ( !isset($q) or $q == null ) return ;
		$sql = "UPDATE `entry` SET `q`=NULL,`user`=NULL,`timestamp`=NULL WHERE `catalog`={$catalog} AND `user`=0 AND `q`={$q}" ;
		$this->mnm->getSQL ( $sql ) ;
	}

	# This returns a list of "not in wikidata" entries that could be creates. Likely not used.
	protected function query_create ( &$out ) {
		$catalog = $this->get_catalog() ;
		$out['total'] = [] ;
		$sql = "SELECT ext_id,ext_name,ext_desc,ext_url,type FROM entry WHERE " ;
		if ( 1 ) $sql .= "q=-1 AND user>0 AND catalog={$catalog} ORDER BY ext_name" ; // Blank
		else $sql .= "q IS null AND catalog={$catalog} ORDER BY ext_name" ; // non-assessed, careful!
		$this->add_sql_to_out ( $out , $sql ) ;
	}

	protected function query_sitestats ( &$out ) {
		$out['total'] = [] ;
		$catalog = $this->get_request ( 'catalog' , '' ) ; # Blank is a valid option
		
		$sql = "SELECT DISTINCT catalog,q FROM entry WHERE user>0 AND q IS NOT NULL" ;
		if ( $catalog != '' ) $sql .= " AND catalog=" . $this->mnm->escape ( $catalog ) ;
		$catalogs = [] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $catalogs[$o->catalog][] = $o->q ;
		
		$dbwd = $this->mnm->openWikidataDB() ;
		foreach ( $catalogs AS $cat => $qs ) {
			$qs = implode ( ',' , $qs ) ;
			$sql = "SELECT ips_site_id,count(*) AS cnt FROM wb_items_per_site WHERE ips_item_id IN ($qs) GROUP BY ips_site_id" ;
			$result = $this->mnm->tfc->getSQL ( $dbwd , $sql , 5 ) ;
			while($o = $result->fetch_object()){
				$out['data'][$o->ips_site_id][$cat] = $o->cnt ;
			}
		}
	}

	protected function query_get_common_names ( &$out ) {
		$catalog = $this->get_catalog();
		$limit = $this->get_request ( 'limit' , 50 ) * 1 ;
		$offset = $this->get_request ( 'offset' , 0 ) * 1 ;
		$min = $this->get_request ( 'min' , 3 ) * 1 ;
		$max = $this->get_request ( 'max' , 15 ) * 1 + 1 ;
		$other_cats_desc = $this->get_request ( 'other_cats_desc' , 0 ) * 1 ;

		$cond1 = $other_cats_desc ? " AND e2.ext_desc!=''" : '' ;
		$not_like = "ext_name NOT LIKE '_. %' AND ext_name NOT LIKE '%?%' AND ext_name NOT LIKE '_ %'" ;
		$sql = "SELECT (SELECT count(*) FROM entry e2 WHERE e1.ext_name=e2.ext_name {$cond1}) AS cnt,e1.* FROM entry e1 WHERE catalog={$catalog} AND q IS NULL AND {$not_like} HAVING cnt>{$min} AND cnt<{$max} LIMIT {$limit} OFFSET {$offset}" ;
		$this->add_sql_to_out ( $out , $sql , 'entries' , 'id' ) ;
	}

	protected function query_get_wd_props ( &$out ) {
		$props = [] ;
		$sql = "SELECT DISTINCT wd_prop FROM catalog WHERE wd_prop!=0 AND wd_prop IS NOT NULL AND wd_qual IS NULL AND active=1 ORDER BY wd_prop" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) $props[] = $o->wd_prop*1 ;
		$out = $props ;
	}

	# Might not be in use
	protected function query_same_names ( &$out ) {
		$out['data']['entries'] = [] ;
		$sql = "SELECT ext_name,count(*) AS cnt,SUM(if(q IS NOT NULL OR q=0, 1, 0)) AS matched FROM entry " ;
		if ( rand(0,10) > 5 ) $sql .= " WHERE ext_name>'M' " ; // Hack to get more results
		$sql .= " GROUP BY ext_name HAVING cnt>1 AND cnt<10 AND matched>0 AND matched<cnt LIMIT 10000" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$tmp = [] ;
		while($o = $result->fetch_object()) $tmp[] = $o ;
		
		$ext_name = $tmp[array_rand($tmp)]->ext_name ;
		$out['data']['name'] = $ext_name ;
		
		$sql = "SELECT * FROM entry WHERE ext_name='" . $this->mnm->escape($ext_name) . "'" ;
		$this->add_entries_and_users_from_sql ( $out , $sql , false ) ;
	}

	protected function query_top_missing ( &$out ) {
		$catalogs = $this->get_request ( 'catalogs' , '' ) ;
		$catalogs = preg_replace ( '/[^0-9,]/' , '' , $catalogs ) ;
		if ( $catalogs == '' ) throw new \Exception("No catalogs given") ;
		$this->add_sql_to_out ( $out , "/*top_missing*/ SELECT ext_name,count(DISTINCT catalog) AS cnt FROM entry WHERE catalog IN ({$catalogs}) AND (q IS NULL or user=0) GROUP BY ext_name HAVING cnt>1 ORDER BY cnt DESC LIMIT 500" ) ;
	}

	# Might not be in use
	protected function query_disambig ( &$out ) {
		$catalog = $this->get_request ( 'catalog' , 0 ) ; # Empty is an option; TODO 0?
		if ( $catalog != '' ) $catalog *= 1 ;
		
		$qs = '' ;
		$sql = "SELECT DISTINCT q FROM entry WHERE q IS NOT NULL and q>0 and user!=0" ;
		if ( $catalog != '' ) $sql .= " AND catalog=$catalog" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $qs != '' ) $qs .= "," ;
			$qs .= "'Q{$o->q}'" ;
		}
		$out['data']['qs'] = count($qs) ; # TODO count of string?
		
		$out['data']['entries'] = [] ;
		if ( $qs == '' ) return ; # No candidates

		$sql = "SELECT DISTINCT page_title FROM page,pagelinks WHERE page_namespace=0 AND page_title IN ($qs) and pl_from=page_id AND pl_namespace=0 AND pl_title='Q4167410' ORDER BY rand() LIMIT 50" ;

		$qs = [] ;
		$dbwd = $this->mnm->openWikidataDB() ;
		$result = $this->mnm->tfc->getSQL ( $dbwd , $sql , 5 ) ;
		while($o = $result->fetch_object()) $qs[] = $o->epp_entity_id ;
		if ( count($qs) == 0 ) return ; # No candidates
		
		$sql = "SELECT * FROM entry WHERE q IN (" . implode(',',$qs) . ") and user!=0" ;
		$this->add_entries_and_users_from_sql ( $out , $sql , false ) ;
	}

	protected function query_locations ( &$out ) {
		$bbox = $this->get_request ( 'bbox' , '' ) ;
		$bbox = preg_replace ( '/[^0-9,\.\-]/' , '' , $bbox ) ;
		$bbox = explode ( ',' , $bbox ) ;
		$out['bbox'] = $bbox ;
		if ( count($bbox) != 4 ) throw new \Exception("Required parameter bbox does not have 4 comma-separated numbers") ;
		$sql = "SELECT entry.*,location.entry_id,location.lat,location.lon FROM entry,location WHERE location.entry_id=entry.id" ;
		$sql .= " AND lon>={$bbox[0]} AND lon<={$bbox[2]} AND lat>={$bbox[1]} AND lat<={$bbox[3]} LIMIT 5000" ;
		$this->add_sql_to_out ( $out , $sql ) ;
	}

	protected function query_get_catalog_info ( &$out ) {
		$catalog_id = $this->get_catalog();
		$sql = "SELECT * FROM catalog WHERE id={$catalog_id}" ;
		$this->add_sql_to_out ( $out , $sql ) ;
	}

	# Likely not used anymore, superseded by query_download2()
	protected function query_download ( &$out ) {
		$out = '' ;
		$catalog = $this->get_catalog();
		$filename = '' ;
		$result = $this->mnm->getSQL ( "SELECT * FROM catalog WHERE id=$catalog" ) ;
		while($o = $result->fetch_object()) $filename = str_replace ( ' ' , '_' , $o->name ) . ".tsv" ;
		$users = [] ;
		$result = $this->mnm->getSQL ( "SELECT * FROM user" ) ;
		while($o = $result->fetch_object()) $users[$o->id] = $o->name ;
		$this->content_type = self::CONTENT_TYPE_TEXT_PLAIN ;
		$this->headers[] = 'Content-Disposition: attachment;filename="' . $filename . '"';
		$out = "Q\tID\tURL\tName\tUser\n" ;
		$sql = "SELECT * FROM entry WHERE catalog=$catalog AND q IS NOT NULL AND q > 0 AND user!=0" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$user = isset($o->user) ? $users[$o->user] : '' ;
			$out .= "{$o->q}\t{$o->ext_id}\t{$o->ext_url}\t{$o->ext_name}\t{$user}\n" ;
		}
	}

	protected function query_download2 ( &$out ) {
		$catalogs = preg_replace ( '/[^0-9,]/' , '' , $this->get_request ( 'catalogs' , '' ) ) ;
		$format = $this->get_request ( 'format' , 'tab' ) ;
		$columns = (object) json_decode($this->get_request('columns','{}')) ;
		$hidden = (object) json_decode($this->get_request('hidden','{}')) ;
		$as_file = $this->get_request ( 'as_file' , 0 ) ;


		$sql = 'SELECT entry.id AS entry_id,entry.catalog,ext_id AS external_id' ;
		if ( $columns->exturl ) $sql .= ',ext_url AS external_url,ext_name AS `name`,ext_desc AS description,`type` AS entry_type,entry.user AS mnm_user_id' ;
		$sql .= ',(CASE WHEN q IS NULL THEN NULL else concat("Q",q) END) AS q' ;
		$sql .= ',`timestamp` AS matched_on' ;

		if ( $columns->username ) $sql .= ',user.name AS matched_by_username' ;
		if ( $columns->aux ) $sql .= ',(SELECT group_concat(concat("{`P",aux_p,"`,`",aux_name,"`,`",in_wikidata,"`}") separator "|") FROM auxiliary WHERE auxiliary.entry_id=entry.id GROUP BY auxiliary.entry_id) AS auxiliary_data' ;
		if ( $columns->dates ) $sql .= ',person_dates.born,person_dates.died,person_dates.in_wikidata AS dates_in_wikidata' ;
		if ( $columns->location ) $sql .= ',location.lat,location.lon' ;
		if ( $columns->multimatch ) $sql .= ',multi_match.candidates AS multi_match_candidates' ;

		$sql .= ' FROM entry' ;

		if ( $columns->dates ) $sql .= ' LEFT JOIN person_dates ON (entry.id=person_dates.entry_id)' ;
		if ( $columns->location ) $sql .= ' LEFT JOIN location ON (entry.id=location.entry_id)' ;
		if ( $columns->multimatch ) $sql .= ' LEFT JOIN multi_match ON (entry.id=multi_match.entry_id)' ;
		if ( $columns->username ) $sql .= ' LEFT JOIN user ON (entry.user=user.id)' ;

		$sql .= " WHERE entry.catalog IN ({$catalogs})" ;
		if ( $hidden->any_matched ) $sql .= " AND entry.q IS NULL" ;
		if ( $hidden->firmly_matched ) $sql .= " AND (entry.q IS NULL OR entry.user=0)" ;
		if ( $hidden->user_matched ) $sql .= " AND (entry.user IS NULL OR entry.user IN (0,3,4))" ;
		if ( $hidden->unmatched ) $sql .= " AND entry.q IS NOT NULL" ;
		if ( $hidden->no_multiple ) $sql .= " AND EXISTS (SELECT * FROM multi_match WHERE entry.id=multi_match.entry_id)" ;
		if ( $hidden->name_date_matched ) $sql .= " AND entry.user!=3" ;
		if ( $hidden->automatched ) $sql .= " AND entry.user!=0" ;
		if ( $hidden->aux_matched ) $sql .= " AND entry.user!=4" ;

		if ( $format != 'json' ) $this->content_type = self::CONTENT_TYPE_TEXT_PLAIN ;

		if ( $as_file ) {
			$filename = "mix-n-match.{$catalogs}." . date('YmdHis') . ".{$format}" ;
			$this->headers[] = 'Content-Disposition: attachment;filename="' . $filename . '"' ;
		}

		$out = '' ;

		$first_row = true ;

		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_assoc()){
			if ( $first_row ) {
				if ( $format == 'tab' ) $out .= '#' . implode ( "\t" , array_keys ( $o ) ) . "\n" ;
				if ( $format == 'json' ) $out .= "[\n" ;
			} else {
				if ( $format == 'json' ) $out .= ",\n" ;
			}
			$first_row = false ;

			if ( $format == 'json' ) {
				$out .= json_encode ( $o ) ;
			} else { # Default: tab
				$p = [] ;
				foreach ( $o AS $k => $v ) $p[] = preg_replace ( '/\s/' , ' ' , $v ) ; # Ensure no tabs/newlines in value
				$out .= implode ( "\t" , $p ) . "\n" ;
			}
		}

		if ( $first_row ) { // Nothing was written
			if ( $format == 'json' ) $out .= "[\n" ;
		}

		if ( $format == 'json' ) $out .= "\n]" ;
		}

	protected function query_redirect ( &$out ) {
		$catalog = $this->get_catalog();
		$ext_id = $this->get_escaped_request ( 'ext_id' , '' ) ;
		$url = '' ;
		$sql = "SELECT ext_url FROM entry WHERE catalog=$catalog AND ext_id='{$ext_id}'" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $url = $o->ext_url ;
		$this->prevent_callback = true ;
		$this->content_type = self::CONTENT_TYPE_TEXT_HTML ;
		$out = '<html><head><META http-equiv="refresh" content="0;URL='.$url.'"></head><body></body></html>' ;
	}

	protected function query_proxy_entry_url ( &$out ) {
		$entry_id = $this->get_request ( 'entry_id' , '' ) ;
		$entry = $this->get_entry_object_from_id ( $entry_id ) ;
		if ( !isset($entry) ) throw new \Exception("No such entry ID '{$entry_id}'") ;
		$this->prevent_callback = true ;
		$this->content_type = self::CONTENT_TYPE_TEXT_HTML ;
		$out = file_get_contents ( $entry->ext_url ) ;
	}

	protected function query_get_property_cache ( &$out ) {
		$out['data']['prop2item'] = [] ;
		$out['data']['item_label'] = [] ;

		$sql = "SELECT DISTINCT prop_group,property,item FROM `property_cache` WHERE `property` in (SELECT DISTINCT `wd_prop` FROM catalog WHERE `active`=1 AND `wd_prop` IS NOT NULL AND `wd_qual` IS NULL)" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$out['data']['prop2item']["{$o->prop_group}"][] = [$o->property*1,$o->item*1] ;
		}

		$sql = "SELECT DISTINCT `item`,`label` FROM `property_cache`" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $out['data']['item_label']["{$o->item}"] = $o->label ;

	}

	protected function query_get_code_fragments ( &$out ) {
		$catalog = $this->get_catalog();
		$out['data'] = ['user_allowed'=>0,'all_functions'=>[]] ;
		$username = $this->get_request ( 'username' , '' ) ;
		if ( $username != '' ) { // Optional; check user name
			$user_id = $this->mnm->getOrCreateUserID ( $username ) ;
			$out['data']['user_allowed'] = in_array($user_id, $this->code_fragment_allowed_user_ids) ? 1 : 0 ;
		}
		$this->add_sql_to_out ( $out , "SELECT * FROM `code_fragments` WHERE `catalog`={$catalog} ORDER BY `function`" , 'fragments' ) ;
		$result = $this->mnm->getSQL ( "SELECT DISTINCT `function` FROM `code_fragments`" ) ;
		while($o = $result->fetch_object()) $out['data']['all_functions'][] = $o->function ;
	}

	protected function query_save_code_fragment ( &$out ) {
		$username = $this->get_request ( 'username' , '' ) ;
		$user_id = $this->check_and_get_user_id ( $username ) ;
		if ( !in_array($user_id, $this->code_fragment_allowed_user_ids) ) throw new \Exception("Not allowed, ask Magnus") ;
		$fragment = json_decode ( $this->get_request ( 'fragment' , '{}' ) ) ;

		$catalog = $fragment->catalog * 1 ;
		if ( $catalog <= 0 ) return ;
		$cfid = $this->mnm->saveCodeFragment ( $fragment , $catalog ) ;
		if ( $fragment->function == 'PERSON_DATE' ) {
			$job_id = $this->mnm->queue_job($catalog,'update_person_dates');
			$this->mnm->queue_job($catalog,'match_person_dates',$job_id);
		} else if ( $fragment->function == 'AUX_FROM_DESC' ) {
			$this->mnm->queue_job($catalog,'generate_aux_from_description');
		} else if ( $fragment->function == 'DESC_FROM_HTML' ) {
			$this->mnm->queue_job($catalog,'update_descriptions_from_url');
		}
	}

	protected function query_test_code_fragment ( &$out ) {
		$out['data'] = [] ;
		$username = $this->get_request ( 'username' , '' ) ;
		$user_id = $this->check_and_get_user_id ( $username ) ;
		if ( !in_array($user_id, $this->code_fragment_allowed_user_ids) ) throw new \Exception("Not allowed, ask Magnus") ;
		$entry_id = $this->get_request ( 'entry_id' , 0 ) * 1 ;
		if ( $entry_id <= 0 ) throw new \Exception("No entry_id") ;
		$entry = $this->get_entry_object_from_id ( $entry_id ) ;
		$fragment = json_decode ( $this->get_request ( 'fragment' , '{}' ) ) ;

		if ( $fragment->function == 'DESC_FROM_HTML' ) $test_harness = new HTMLtoDescription ( $entry->catalog , $this->mnm ) ;
		else if ( $fragment->function == 'PERSON_DATE' ) $test_harness = new PersonDates ( $entry->catalog , $this->mnm ) ;
		else if ( $fragment->function == 'AUX_FROM_DESC' ) $test_harness = new DescriptionToAux ( $entry->catalog , $this->mnm ) ;
		else if ( $fragment->function == 'BESPOKE_SCRAPER' ) $test_harness = new BespokeScraper ( $entry->catalog , $this->mnm ) ;
		else throw new \Exception("Bad fragment function '{$fragment->function}'") ;
		$fragment->success = true ;
		$fragment->is_active = true ;
		if ( isset($fragment->json) and $fragment->json !== null and $fragment->json != '' ) $fragment->json = json_decode ( $fragment->json ) ;
		else $fragment->json = json_decode ( '{}' ) ;

		$test_harness->setCodeFragment ( $fragment ) ;
		if ( $fragment->function == 'BESPOKE_SCRAPER' ) $entry = (object) [ 'id' => $entry->ext_id , 'url' => $entry->ext_url , 'catalog' => $entry->catalog ] ;
		$commands = $test_harness->processEntry ( $entry ) ;
		$out['data'] = json_decode ( json_encode ( $commands ) ) ;
	}

	protected function query_suggest ( &$out ) {
		$this->content_type = self::CONTENT_TYPE_TEXT_PLAIN ;
		$out = '' ;
		$ts = date ( 'YmdHis' ) ;
		$catalog = $this->get_request('catalog',0) * 1 ;
		$overwrite = $this->get_request('overwrite',0) * 1 ;
		$suggestions = $this->get_request ( 'suggestions' , '' ) ;
		$suggestions = explode ( "\n" , $suggestions ) ;
		$cnt = 0 ;
		foreach ( $suggestions AS $s ) {
			if ( trim($s) == '' ) continue ;
			$s = explode ( '|' , $s ) ;
			if ( count($s) != 2 ) {
				$out .= "Bad row : " . implode('|',$s) . "\n" ;
				continue ;
			}
			$extid = trim ( $s[0] ) ;
			$q = preg_replace ( '/\D/' , '' , $s[1] ) ;
			$sql = "UPDATE entry SET q=$q,user=0,`timestamp`='$ts' WHERE catalog=$catalog AND ext_id='" . $this->mnm->escape($extid) . "'" ;
			if ( $overwrite == 1 ) $sql .= " AND (user=0 OR q IS NULL)" ;
			else $sql .= " AND (q IS NULL)" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			$cnt += $this->mnm->dbm->affected_rows ;
		}
		$out .= "$cnt entries changed" ;
	}

	protected function query_random ( &$out ) {
		$catalogs = [] ;
		$sql = "SELECT `id` FROM `catalog` WHERE `active`=1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $catalogs[$o->id] = $o->id ;

		$submode = $this->get_request ( 'submode' , '' ) ;
		$catalog = $this->get_request ( 'catalog' , 0 ) * 1 ;
		$id = $this->get_request ( 'id' , 0 ) * 1 ; # For testing
		$type = $this->mnm->escape ( $this->get_request ( 'type' , '' ) ) ;

		$noq_in_catalog = 0 ;
		if ( $catalog > 0 ) {
			$sql = "SELECT * FROM overview WHERE catalog={$catalog}" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $noq_in_catalog = $o->noq ;
		}

		$cnt = 0 ;
		$fail = 0 ;
		unset($out['data']);
		while ( 1 ) {
			if ( $fail ) break ; # No more results
			$r = $this->mnm->rand() ;
			if ( $cnt > 10 ) {
				$r = 0 ;
				$fail = 1 ;
			}
			$sql = "SELECT * FROM entry" ;
			if ( $noq_in_catalog > 2000 ) $sql .= " FORCE INDEX (random_2)" ;
			$sql .= " WHERE random>=$r " ;
			if ( $submode == 'prematched' ) $sql .= " AND user=0" ;
			else if ( $submode == 'no_manual' ) $sql .= " AND ( user=0 or q is null )" ;
			else $sql .= " AND q IS NULL" ; // Default: unmatched
			if ( $catalog > 0 ) $sql .= " AND catalog=$catalog" ;
			if ( $type != '' ) $sql = " AND `type`='$type'" ;
			$sql .= " ORDER BY random" ;
			if ( $catalog == 0 ) $sql .= " LIMIT 10" ;
			else $sql .= " LIMIT 1" ;
			if ( $id!=0 ) $sql = "SELECT * FROM entry WHERE id=" . ($_REQUEST['id']*1) ; # For testing
			if ( $this->testing ) {
				$out['sql'] = $sql ;
				break ;
			}
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) {
				if ( !isset($catalogs[$o->catalog]) ) continue ; // Make sure catalog is active
				$out['data'] = $o ;
				break ;
			}
			if ( isset ( $out['data'] ) ) break ;
			$cnt++ ;
		}

		if ( isset($out['data']) ) {
			$id = $out['data']->id ;
			$sql = "SELECT * FROM person_dates WHERE entry_id=$id" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()){
				if ( $o->born != '' ) $out['data']->born = $o->born ;
				if ( $o->died != '' ) $out['data']->died = $o->died ;
			}
		}
	}

	protected function query_get_entries_by_q_or_value ( &$out ) {
		$q = $this->get_request ( 'q' , '' ) ;
		$json = (array) json_decode ( $this->get_request ( 'json' , '[]' ) ) ;
		$sql_parts = [ "q=" . preg_replace('/\D/','',$q) ] ;

		if ( count($json) > 0 ) {
			$props = implode ( ',' , array_keys($json) ) ;
			$props = preg_replace ( '/[^0-9,]/' , '' , $props ) ;
			$sql = "SELECT id,wd_prop FROM catalog WHERE wd_qual IS NULL AND active=1 AND wd_prop IN ($props)" ;
			$prop2catalog = [] ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while ( $o = $result->fetch_object() ) $prop2catalog['P'.$o->wd_prop][] = $o->id ;
			foreach ( $json AS $prop => $values ) {
				if ( count($values) == 0 ) continue ;
				if ( !isset($prop2catalog[$prop]) or count($prop2catalog[$prop]) == 0 ) continue ;
				foreach ( $values AS $k => $v ) $values[$k] = $this->mnm->escape ( $v ) ;
				$sql_parts[] = "catalog IN (".implode(',',$prop2catalog[$prop]).") AND ext_id IN ('".implode("','",$values)."')" ;
			}
		}
		
		$sql = "SELECT * FROM entry WHERE ((" . implode ( ') OR (' , $sql_parts ) . '))' ;
		$sql .= ' AND catalog NOT IN (SELECT id FROM catalog WHERE active!=1) ORDER BY catalog,id' ;
		$this->add_sql_to_out ( $out , $sql , 'entries' , 'id' ) ;
		if ( count($out['data']['entries']) == 0 ) return ;

		$catalog = [] ;
		foreach ( $out['data']['entries'] AS $e ) $catalogs[$e->catalog] = 1 ;
		$sql = "SELECT * FROM catalog WHERE id IN (" . implode(',',array_keys($catalogs)) . ")" ;
		$this->add_sql_to_out ( $out , $sql , 'catalogs' , 'id' ) ;
	}

	protected function query_missingpages ( &$out ) {
		$out['data'] = ['entries'=>[],'users'=>[]] ;
		$catalog = $this->get_catalog();
		$site = trim ( $this->get_escaped_request ( 'site' , '' ) ) ;
		if ( $site == '' ) throw new \Exception("site parameter required") ;
		
		$sql = "SELECT DISTINCT q FROM entry WHERE q>0 AND user>0 AND catalog=$catalog AND q IS NOT NULL" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $qs[''.$o->q] = $o->q ;

		if ( count($qs) > 0 ) {
			$dbwd = $this->mnm->openWikidataDB() ;
			$sql = "SELECT DISTINCT ips_item_id FROM wb_items_per_site WHERE ips_item_id IN (" . implode(',',$qs) . ") AND ips_site_id='$site'" ;
			$result = $this->mnm->tfc->getSQL ( $dbwd , $sql , 5 ) ;
			while($o = $result->fetch_object()) unset ( $qs[''.$o->ips_item_id] ) ;
		}
		if ( count($qs) == 0 ) return ;

		$sql = "SELECT * FROM entry WHERE user>0 AND catalog=$catalog AND q IN (" . implode(',',$qs) . ")" ;
		$this->add_entries_and_users_from_sql ( $out , $sql ) ;
	}

	protected function query_catalog ( &$out ) {
		$catalog = $this->get_catalog() ;
		$entry = $this->get_request ( 'entry' , -1 ) ;
		$meta = json_decode ( $this->get_request ( 'meta' , '{}' ) ) ;
		if ( !is_object($meta) ) throw new \Exception("meta needs to be a JSON object") ;
		if ( !isset($meta->show_nowd) ) $meta->show_nowd = 0 ;
		
		$sql = "SELECT * FROM entry WHERE catalog={$catalog}" ;
		if ( $meta->show_multiple == 1 ) {
			$sql .= " AND EXISTS ( SELECT * FROM multi_match WHERE entry_id=entry.id ) AND ( user<=0 OR user is null )" ;
		} else if ( $meta->show_noq+$meta->show_autoq+$meta->show_userq+$meta->show_nowd == 0 and $meta->show_na == 1 ) {
			$sql .= " AND q=0" ;
		} else if ( $meta->show_noq+$meta->show_autoq+$meta->show_userq+$meta->show_na == 0 and $meta->show_nowd == 1 ) {
			$sql .= " AND q=-1" ;
		} else {
			if ( $meta->show_noq != 1 ) $sql .= " AND q IS NOT NULL" ;
			if ( $meta->show_autoq != 1 ) $sql .= " AND ( q is null OR user!=0 )" ;
			if ( $meta->show_userq != 1 ) $sql .= " AND ( user<=0 OR user is null )" ;
			if ( $meta->show_na != 1 ) $sql .= " AND ( q!=0 or q is null )" ;
//			if ( $meta->show_nowd != 1 ) $sql .= " AND ( q=-1 )" ;
		}

		if ( isset($_REQUEST['type']) ) $sql .= " AND `type`='" . $this->mnm->escape($_REQUEST['type']) . "'" ;
		if ( isset($_REQUEST['title_match']) ) $sql .= " AND `ext_name` LIKE '%" . $this->mnm->escape($_REQUEST['title_match']) . "%'" ;
		$sql .= " LIMIT " . $this->mnm->escape(''.($meta->per_page??50)) ;
		$sql .= " OFFSET " . $this->mnm->escape(''.($meta->offset??0)) ;
		
		$this->add_entries_and_users_from_sql ( $out , $sql ) ;
		$this->add_extended_entry_data($out) ;
	}

	protected function query_get_entry ( &$out ) {
		$catalog = $this->get_request( 'catalog' , 0 ) * 1 ; # Optional
		$entry_ids = $this->get_request ( 'entry' , '' ) ; # Optional
		$sql = "SELECT * FROM entry WHERE " ;
		$ext_ids = $this->get_request ( 'ext_ids' , '' ) ;
		if ( $ext_ids != '' ) {
			if ( $catalog <= 0 ) throw new \Exception("catalog is required when using ext_ids") ;
			$ext_ids = json_decode ( $ext_ids ) ;
			$x = [] ;
			foreach ( $ext_ids AS $eid ) $x[] = $this->mnm->escape($eid) ;
			$ext_ids = '"' . implode ( '","' , $x ) . '"' ;
			$sql .= "catalog={$catalog} AND ext_id IN ($ext_ids)" ;
		} else {
			$entry_ids = trim ( preg_replace ( '|[^0-9,]|' , '' , $entry_ids ) ) ;
			if ( $entry_ids == '' ) throw new \Exception("entry is required") ;
			$sql .= "id IN ({$entry_ids})" ;
		}
		$this->add_entries_and_users_from_sql ( $out , $sql ) ;
		$this->add_extended_entry_data($out) ;
	}

	protected function query_edit_catalog ( &$out ) {
		$catalog = $this->get_catalog();
		$data = json_decode ( $this->get_request ( 'data' , '' ) ) ;
		if ( !isset($data) or $data == null or !isset($data->name) ) throw new \Exception("Bad data") ;
		$username = $this->get_request ( 'username' , '' ) ;
		$username = str_replace ( '_' , ' ' , $username ) ;
		$sql = "SELECT * FROM user WHERE name='".$this->mnm->escape($username)."' LIMIT 1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$found = ($o = $result->fetch_object()) ;
		if ( !$found ) throw new \Exception("No such user '{$username}'") ;
		if ( !$o->is_catalog_admin ) throw new \Exception("'{$username}'' is not a catalog admin") ;
		$sql = "UPDATE catalog SET " ;
		$sql .= "`name`='" . $this->mnm->escape($data->name) . "'," ;
		$sql .= "`url`='" . $this->mnm->escape($data->url) . "'," ;
		$sql .= "`desc`='" . $this->mnm->escape($data->desc) . "'," ;
		$sql .= "`type`='" . $this->mnm->escape($data->type) . "'," ;
		$sql .= "`search_wp`='" . $this->mnm->escape($data->search_wp) . "'," ;
		$sql .= "`wd_prop`=" . ((isset($data->wd_prop) and $data->wd_prop*1>0)?$data->wd_prop*1:'null') . "," ;
		$sql .= "`wd_qual`=" . ((isset($data->wd_qual) and $data->wd_qual*1>0)?$data->wd_qual*1:'null') . "," ;
		$sql .= "`active`='" . ($data->active?1:0) . "'" ;
		$sql .= " WHERE id={$catalog}" ;
		$this->mnm->getSQL ( $sql ) ;
		$this->mnm->updateCatalogs ( [$catalog] ) ;
	}

	protected function query_resolve_issue ( &$out ) {
		$issue_id = $this->get_request ( 'issue_id' , 0 ) * 1 ;
		if ( $issue_id <= 0 ) throw new \Exception("Bad issue ID") ;
		$user_id = $this->check_and_get_user_id ( $this->get_request('username','') ) ;
		$ts = $this->mnm->getCurrentTimestamp() ;
		$sql = "UPDATE issues SET `status`='DONE',`user_id`={$user_id},`resolved_ts`='{$ts}' WHERE id={$issue_id}" ;
		if ( !$this->mnm->getSQL($sql) ) throw new \Exception("SQL update failed") ;
	}

	protected function query_prep_new_item ( &$out ) {
		require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;
		require_once ( '/data/project/mix-n-match/manual_lists/large_catalogs/shared.php' ) ;

		$qs = new \QuickStatements () ;
		$lc = new \largeCatalog ( 2 ) ;

		$entry_ids = explode ( ',' , $this->get_request ( 'entry_ids' , '' ) ) ;
		if ( count($entry_ids) == 0 ) throw new \Exception("No entry_ids parameter") ;
		$all_commands = [] ;
		foreach ( $entry_ids AS $num => $entry_id ) {
			$entry = $this->get_entry_object_from_id ( $entry_id ) ;
			if ( !isset($entry) ) throw new \Exception("Entry '{$entry_id}' not in database") ;
			$commands = $this->mnm->getCreateItemForEntryCommands ( $entry , $lc ) ; // QS commands
			if ( !isset($commands) ) {
				throw new \Exception($this->mnm->last_error) ;
			}
			array_shift ( $commands ) ; // Remove CREATE
			$out['commands'][] = $commands ;
			foreach ( $commands AS $k => $v ) {
				if ( $num > 0 and preg_match ( '/^LAST\t[LADS]/' , $v ) ) continue ; // Labels etc. from first entry only
				$key = $v ;
				if ( preg_match ( '/^(LAST\t.+?\t.+?)(\t.+)$/' , $v , $m ) ) {
					$key = $m[1] ;
					if ( isset($all_commands[$key]) ) $v = $all_commands[$key] . $m[2] ;
				}
				$all_commands[$key] = $v ;
			}
		}
		$all_commands = array_unique ( $all_commands ) ;
		array_unshift ( $all_commands , 'CREATE' ) ; // Prefix CREATE

		# Filter same name/description
		$label_in_lang = [] ;
		foreach ( $all_commands AS $command ) {
			$c = explode ( "\t" , $command ) ;
			if ( preg_match('|^L(.+)$|',$c[1]??'',$m) ) $label_in_lang[$m[1]] = $c[2] ;
		}
		$tmp = $all_commands ;
		$all_commands = [] ;
		foreach ( $tmp AS $command ) {
			$c = explode ( "\t" , $command ) ;
			if ( preg_match('|^D(.+)$|',$c[1]??'',$m) ) {
				if ( $label_in_lang[$m[1]]??'' == $c[2] ) continue ;
			}
			$all_commands[] = $command ;
		}

		$out['all_commands'] = $all_commands ;
		$commands = implode ( "\n" , $all_commands ) ;
		$qs->use_command_compression = true ;
		$commands = $qs->importData ( $commands , 'v1' , false ) ;
		$commands = $qs->compressCommands ( $commands['data']['commands'] ) ;
		$out['data'] = json_encode ( $commands[0]['data'] , JSON_HEX_QUOT|JSON_HEX_APOS ) ;
	}

	protected function query_get_flickr_key ( &$out ) {
		$out['data'] = file_get_contents("/data/project/mix-n-match/flickr.key");
	}

	protected function query_creation_candidates ( &$out ) {
		$max_tries = 250 ;
		$run_expensive_query = false ;

		$min = $this->get_request ( 'min' , 0 ) * 1 ;
		if ( $min < 3 ) $min = 3; # OVERRIDE
		$mode = trim ( $this->get_request ( 'mode' , '' ) ) ;
		$ext_name_required = trim ( $this->get_request ( 'ext_name' , '' ) ) ;
		$birth_year = trim ( $this->get_request('birth_year','') ) ;
		$death_year = trim ( $this->get_request('death_year','') ) ;
		$require_unset = $this->get_request ( 'require_unset' , 0 ) * 1 ;
		$require_catalogs = preg_replace ( '/[^0-9,]/' , '' , $this->get_request('require_catalogs','') ) ;
		$catalogs_required = $this->get_request ( 'min_catalogs_required' , 0 ) * 1 ;
		$table = 'common_names' ;
		if ( $mode != '' ) $table .= '_' .  $this->mnm->escape ( $mode ) ;

		$tries = 0 ;
		$users = [] ;
		while ( 1 ) {
			if ( $tries++ >= $max_tries ) throw new \Exception("No results after {$max_tries} attempts, giving up") ;
			$out['data'] = ['entries'=>[]] ;
			$users = [] ;
			
			if ( $ext_name_required != '' ) {
				$ext_name_safe = $this->mnm->escape ( $ext_name_required ) ;
				$sql = "SELECT '{$ext_name_safe}' AS ext_name,20 AS cnt" ;
			} else if ( $require_catalogs != '' ) {
				if ( $run_expensive_query ) $sql = "SELECT ext_name,count(DISTINCT catalog) AS cnt FROM entry WHERE catalog IN ({$require_catalogs}) AND (q IS NULL or user=0) GROUP BY ext_name HAVING cnt>=3 ORDER BY rand() LIMIT 1" ;
				else throw new \Exception("require_catalogs but not running expensive query") ;
			} else $sql = "SELECT name AS ext_name,cnt FROM $table WHERE " . ($min>0?" cnt>=$min AND":'') . " cnt<15 ORDER BY rand() LIMIT 1" ;
			

			$result = $this->mnm->getSQL ( $sql ) ;
			$tmp = [] ;
			while($o = $result->fetch_object()) $tmp[] = $o ;
			
			$ext_name = $tmp[array_rand($tmp)]->ext_name ;
			$out['data']['name'] = $ext_name ;

			$names = [ $ext_name ] ;
			if ( preg_match ( '/^(\S+) (.+) (\S+)$/' , $ext_name , $m ) ) {
				$names[] = $m[1].'-'.$m[2].' '.$m[3] ;
				$names[] = $m[1].' '.$m[2].'-'.$m[3] ;
			}
			foreach ( $names AS $k => $v ) $names[$k] = $this->mnm->escape ( $v ) ;

			$sql = "/*creation_candidates*/ SELECT * FROM entry WHERE catalog IN (SELECT id FROM catalog WHERE catalog.active=1) AND ext_name IN ('" . implode("','",$names) . "') AND (q is null OR q!=-1)" ;
			if ( $mode == 'taxon' ) $sql .= " AND `type`='Q16521'" ;
			if ( $birth_year.$death_year!='' ) {
				$parts = [ "entry_id=entry.id" ] ;
				if ( $birth_year!='' ) $parts[] = "year_born={$birth_year}" ;
				if ( $death_year!='' ) $parts[] = "year_died={$death_year}" ;
				$sql .= " AND EXISTS (SELECT * FROM person_dates WHERE " . implode(" AND ",$parts) . ")" ;
			}
			$out["sql"] = $sql ;
			$result = $this->mnm->getSQL ( $sql ) ;
			$required_catalogs_found = [] ;
			$found_unset = 0 ;
			while($o = $result->fetch_object()) {
				$out['data']['entries'][$o->id] = $o ;
				if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
				if ( in_array($o->catalog, explode(',',$require_catalogs)) ) $required_catalogs_found[$o->catalog]++ ;
				if ( $o->user == 0 or !isset($o->q) or $o->q === null ) $found_unset++ ;
			}
			if ( $ext_name_required == '' ) {
				if ( $found_unset < $require_unset ) continue ;
				if ( count($required_catalogs_found) < $catalogs_required ) continue ;
			}
			
			if ( $min == 0 or count($out['data']['entries']) >= $min ) break ;
			if ( $ext_name != '' ) break ; // Only one possible query to run
		}

		$out['data']['entries'] = array_values ( $out['data']['entries'] ) ;
		$out['data']['users'] = $this->get_users ( $users ) ;
		$this->add_extended_entry_data($out) ;
	}

	protected function query_search ( &$out ) {
		$max_results = $this->get_request('max',100) * 1 ;
		$what = $this->get_request ( 'what' , '' ) ;
		$description_search = $this->get_request ( 'description_search' , 0 ) * 1 ;
		$no_label_search = $this->get_request ( 'no_label_search' , 0 ) * 1 ;
		$exclude = preg_replace ( '/[^0-9,]/' , '' , $this->get_request ( 'exclude' , '' ) ) ;
		$include = preg_replace ( '/[^0-9,]/' , '' , $this->get_request ( 'include' , '' ) ) ;
		
		$exclude = ( $exclude == '' ) ? []  : explode ( ',' , $exclude ) ;
		$include = ( $include == '' ) ? []  : explode ( ',' , $include ) ;
		$sql = "SELECT id FROM catalog WHERE `active`!=1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $exclude[] = $o->id ;
		$exclude = implode ( ',' , $exclude ) ;
		$include = implode ( ',' , $include ) ;
		
		$what = preg_replace ( '/[\-]/' , ' ' , $what ) ;
		if ( preg_match ( '/^\s*[Qq]{0,1}(\d+)\s*$/' , $what , $m ) ) $sql = "SELECT * FROM entry WHERE q=".$m[1] ;
		else {
			$s = [] ;
			$what2 = explode ( " " , $what ) ;
			foreach ( $what2 AS $w ) {
				$w = $this->mnm->escape ( trim ( $w ) ) ;
				if ( strlen($w)>=3 and strlen($w)<=84 ) $s[] = $w ;
			}
			$sql_parts = [] ;
			if ( !$no_label_search ) $sql_parts[] = "MATCH(ext_name) AGAINST('+".implode(",+",$s)."' IN BOOLEAN MODE)" ;
			if ( $description_search ) $sql_parts[] = "MATCH(ext_desc) AGAINST('+".implode(",+",$s)."' IN BOOLEAN MODE)" ;
			$sql = "SELECT * FROM entry WHERE ((" . implode(") OR (",$sql_parts) . "))" ;
			if ( $exclude != '' ) $sql .= " AND catalog NOT IN ($exclude)" ;
			if ( $include != '' ) $sql .= " AND catalog IN ($include)" ;
			$sql .= " LIMIT $max_results" ;
		}
		
		$this->mnm->dbm->set_charset('utf8mb4');  // Say what?!?
		$this->add_entries_and_users_from_sql ( $out , $sql ) ;
	}

	protected function query_rc ( &$out ) {
		$limit = 100 ;
		$ts = $this->get_request ( 'ts' , '' ) ;
		$catalog = $this->get_request ( 'catalog' , 0 ) * 1 ;
		$events = [] ;
		
		$sql = "SELECT * FROM entry WHERE 1=1" ;
		if ( $ts != '' ) $sql .= " AND timestamp >= '" . $this->mnm->escape($ts) . "'" ;
		if ( $catalog != 0 ) $sql .= " AND catalog={$catalog}" ;
		$sql .= " HAVING user!=0 and user!=3 and user!=4 and timestamp is not null" ;
		$sql .= " ORDER BY timestamp DESC LIMIT $limit" ;
		$min_ts = '' ;
		$max_ts = $ts ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$users = [] ;
		while($o = $result->fetch_object()){
			$o->event_type = 'match' ;
			$events[$o->timestamp.'-'.$o->id] = $o ;
			if ( $min_ts == '' ) $min_ts = $o->timestamp ;
			$max_ts = $o->timestamp ;
			$users[$o->user] = 1 ;
		}

		$sql = "SELECT entry.id AS id,catalog,ext_id,ext_url,ext_name,ext_desc,action AS event_type,log.user AS user,log.timestamp AS timestamp FROM log,entry WHERE log.entry_id=entry.id AND log.timestamp BETWEEN '$max_ts' AND '$min_ts'" ;
		if ( $catalog != 0 ) $sql .= " AND catalog={$catalog}" ;
		$out['sql'] = $sql ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$events[$o->timestamp.'-'.$o->id] = $o ;
			$users[$o->user] = 1 ;
		}
		krsort ( $events ) ;
		while ( count ( $events ) > $limit ) array_pop ( $events ) ;
		$out['data']['events'] = $events ;
		$out['data']['users'] = $this->get_users ( $users ) ;
	}

	protected function query_rc_atom ( &$out ) {
		$this->query_rc ( $out ) ;
		$this->render_atom ( $out ) ;
	}

	protected function query_get_issues ( &$out ) {
		$issue_type = trim ( strtoupper ( $this->get_request ( 'type' , '' ) ) ) ;
		$limit = $this->get_request ( 'limit' , 50 ) * 1 ;
		$offset = $this->get_request ( 'offset' , 0 ) * 1 ;
		$catalogs = preg_replace ( '/[^0-9,]/' , '' , $this->get_request ( 'catalogs' , '' ) ) ;
		if ( $catalogs != '' ) $sql_catalogs = " AND `catalog` IN ({$catalogs})" ;
		else $sql_catalogs = '' ;
		$sql = "SELECT count(*) AS `cnt` FROM `issues` WHERE `status`='OPEN' {$sql_catalogs}" ;
		$out['sql'] = [$sql] ;
		$result = $this->mnm->getSQL ( $sql ) ;
		$o = $result->fetch_object() ;
		$open_issues = $o->cnt * 1 ;
		if ( $open_issues == 0 ) return ; # No open issues
		$min_open_issues = $limit * 2 ;
		$out['data'] = ['open_issues'=>$open_issues,'issues'=>[],'entries'=>[]] ;
		$entries = [] ;
		while ( count($out['data']['issues']) < $limit ) {
			if ( $open_issues < $min_open_issues ) $r = 0 ;
			else $r = $this->mnm->rand() ;
			$sql = "SELECT * FROM `issues` WHERE `status`='OPEN' AND random>={$r}" ;
			if ( $issue_type != '' ) $sql .= " AND `type`='" . $this->mnm->escape($issue_type) . "'" ;
			$sql .= $sql_catalogs ;
			$sql .= " ORDER BY `random` LIMIT {$limit} OFFSET {$offset}" ;
			$out['sql'][] = $sql ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) {
				if ( isset($o->json) and $o->json !== null and $o->json != '' ) $o->json = json_decode ( $o->json ) ;
				$out['data']['issues'][$o->id] = $o ;
				$entries[$o->entry_id] = $o->entry_id ;
			}
			if ( $open_issues < $min_open_issues ) break ;
		}
		if ( count($entries) == 0 ) return ;
		$sql = "SELECT * FROM entry WHERE id IN (".implode(',',$entries).")" ;
		$this->add_entries_and_users_from_sql ( $out , $sql ) ;
	}

	protected function query_autoscrape_test ( &$out ) {
		$json = $this->get_request ( 'json' , '' ) ;
		$as = new AutoScrape ;
		
		if ( !$as->loadFromJSON ( $json , 0 ) ) throw new \Exception($as->error) ;
		$as->max_urls_requested = 1 ;
		$as->log2array = true ;
		$as->runTest() ;
		if ( isset($as->error) AND $as->error != null ) $out['status'] = $as->error ;
		$out['data']['html'] = $as->last_html ;
		$out['data']['log'] = $as->logs ;
		$out['data']['results'] = $as->entries ;
		$out['data']['last_url'] = $as->getLastURL() ;
		$out['data']['html'] = utf8_encode ( $out['data']['html'] ) ;
		foreach ( $out['data']['results'] AS $k => $v ) {
			// TODO?
		}
	}

	protected function query_save_scraper ( &$out ) {
		$scraper = json_decode ( $this->get_request ( 'scraper' , '{}' ) ) ;
		$levels = json_decode ( $this->get_request ( 'levels' , '{}' ) ) ;
		$options = json_decode ( $this->get_request ( 'options' , '{}' ) ) ;
		$meta = json_decode ( $this->get_request ( 'meta' , '{}' ) ) ;
		$user_id = $this->check_and_get_user_id ( $this->user ) ;
		
		$cid = trim($meta->catalog_id) ;
		$exists = false ;
		if ( !preg_match ( '/^\d+$/' , $cid ) ) { // Create new catalog
			if ( $scraper->resolve->type->use == 'Q5' ) $meta->type = 'biography' ;
			$meta->note = 'Created via scraper import' ;
			$catalog = new Catalog ( 0 , $this->mnm ) ;
			$cid = $catalog->createNew ( $meta , $user_id ) ;
			$exists = true ;
		} else { // Check if catalog exists
			$sql = "SELECT * FROM catalog WHERE id=$cid" ;
			$result = $this->mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $exists = true ;
		}
		if ( !$exists ) throw new \Exception("Catalog #{$cid} does not exist") ;

		$out['data']['catalog'] = $cid ;
		$j = [ 'levels' => $levels , 'options' => $options , 'scraper' => $scraper ] ;
		$j = $this->mnm->escape(json_encode($j)) ;
		
		$existing = (object) ['bla'=>'test'] ;
		$sql = "SELECT * FROM autoscrape WHERE catalog=$cid" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $existing = $o ;
		
		if ( isset($existing->owner) ) {
			if ( $existing->owner != $user_id ) throw new \Exception("A different user created the existing scraper") ;
			$sql = "UPDATE autoscrape SET `json`='$j',`status`='IMPORT' WHERE catalog=$cid" ;
			$result = $this->mnm->getSQL ( $sql ) ;
		} else {
			$sql = "INSERT INTO autoscrape (`catalog`,`json`,`owner`,`notes`,`status`) VALUES ($cid,\"$j\",$user_id,'Created via scraper import','IMPORT') ON DUPLICATE KEY UPDATE json='$j',status='IMPORT'" ;
			$result = $this->mnm->getSQL ( $sql ) ;
		}
		$this->mnm->queue_job ( $cid , 'autoscrape' , 0 , '' , 0 , 0 , 'HIGH_PRIORITY' ) ;
	}

	protected function query_get_top_groups ( &$out ) {
		$result = $this->mnm->getSQL ( "SELECT top_missing_groups.*,user.name AS user_name FROM top_missing_groups,user WHERE top_missing_groups.user=user.id AND current=1 ORDER BY name" ) ;
		while($o = $result->fetch_object()) $out['data'][] = $o ;
	}

	protected function query_set_top_group ( &$out ) {
		$username = trim($this->get_request ( 'username' , '' )) ;
		$user_id = $this->check_and_get_user_id ( $username ) ;
		$group_name = trim($this->get_escaped_request ( 'group_name' , '' )) ;
		$group_id = $this->get_escaped_request ( 'group_id' , 0 ) * 1 ;
		$catalogs = trim($this->get_escaped_request ( 'catalogs' , '' )) ;
		if ( $group_id > 0 ) $this->mnm->getSQL ( "UPDATE `top_missing_groups` SET `current`=0 WHERE `id`={$group_id}" ) ;
		$ts = $this->mnm->getCurrentTimestamp() ;
		$sql = "INSERT IGNORE INTO `top_missing_groups` (`name`,`catalogs`,`user`,`timestamp`,`current`,`based_on`) VALUES ('{$group_name}','{$catalogs}','{$user_id}','{$ts}',1,{$group_id})" ;
		$out['sql'] = $sql ;
		$this->mnm->getSQL ( $sql ) ;
	}

	protected function query_get_locations_in_catalog ( &$out ) {
		$catalog_id = $this->get_catalog();
		$out['data'] = [] ;
		$sql = "SELECT * FROM `vw_location` WHERE `catalog`={$catalog_id}" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $out['data'][] = $o ;
	}

	protected function query_get_sync ( &$out ) {
		$catalog = $this->get_catalog();
		set_time_limit(600); # 10min

		// Load prop/qual
		$result = $this->mnm->getSQL ( "SELECT * FROM catalog WHERE id=$catalog" ) ;
		while($o = $result->fetch_object()){
			$prop = $o->wd_prop ;
			$qual = $o->wd_qual ;
		}
		if ( !isset($prop) ) $prop = '' ;
		if ( !isset($qual) ) $qual = '' ;
		if ( $prop == '' ) throw new \Exception("No Wikidata property defined for this catalog") ;
		if ( $qual != '' ) throw new \Exception("This does not work for the old, qualifier-based catalogs") ;

		$sc = new SetCompare ( $this->mnm , $catalog );

		// Get Wikidata state
		$query = "SELECT ?q ?prop { ?q wdt:P$prop ?prop }" ;
		foreach ( $this->mnm->tfc->getSPARQL_TSV($query) as $o ) {
			$q = $this->mnm->tfc->parseItemFromURL($o['q']) ;
			$sc->addWD ( $q , $o['prop'] ) ;
		}
		$sc->flushWD() ;

		// Get Mix-n-match state
		$sc->addFromMnM() ;

		// Report
		if ( $catalog == 3296 ) { # TESTING
			# OK $out['data']['mm_dupes'] = $sc->get_mnm_dupes() ; # More than one ext_id for the same q in MnM
			# BROKEN $out['data']['different'] = $sc->get_diff() ; # Different ext_ids for the same q between mnm and wd
			# BROKEN $out['data']['wd_no_mm'] = $sc->compare_wd_mnm ( 1 ) ;
			# BROKEN $out['data']['mm_no_wd'] = $sc->compare_wd_mnm ( 0 ) ;
		} else {
			$out['data']['mm_dupes'] = $sc->get_mnm_dupes() ; # More than one ext_id for the same q in MnM
			$out['data']['different'] = $sc->get_diff() ; # Different ext_ids for the same q between mnm and wd
			$out['data']['wd_no_mm'] = $sc->compare_wd_mnm ( 1 ) ;
			$out['data']['mm_no_wd'] = $sc->compare_wd_mnm ( 0 ) ;
		}

		$out['data']['mm_double'] = [] ;
		$sql = "SELECT q,count(*) AS cnt,group_concat(id) AS ids FROM entry WHERE q>0 AND catalog=$catalog AND q IS NOT NULL AND user IS NOT NULL AND user>0 GROUP BY q HAVING cnt>1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $out['data']['mm_double']["{$o->q}"] = explode ( ',' , $o->ids ) ;
	}

	################################################################################
	# Public helper functions

	public function get_request ( $varname , $default = '' ) {
		return $this->mnm->tfc->getRequest ( $varname , $default ) ;
	}

	public function check_and_get_user_id ( $username ) {
		$user_id = $this->mnm->getOrCreateUserID ( $username ) ;
		if ( $user_id <= 0 ) throw new \Exception("OAuth login failure, please log in again") ;
		$user = new User ( $user_id , $this->mnm );
		if ( $user->isBlocked() ) throw new \Exception("You are blocked on Wikidata") ;
		return $user_id ;
	}

	public function get_users ( $users ) {
		if ( count ( $users ) == 0 ) return [] ;
		$ret = [] ;
		$sql = "SELECT * FROM user WHERE id IN (" . implode(',',array_keys($users)) . ")" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $ret[$o->id] = $o ;
		return $ret ;
	}

	public function add_extended_entry_data ( &$out ) {
		if ( !isset($out) or !isset($out['data']) or !isset($out['data']['entries']) ) return ;
		if ( count ( $out['data']['entries'] ) == 0 ) return ;

		$keys = implode ( ',' , array_keys ( $out['data']['entries'] ) ) ;

		// Person birth/death dates
		$sql = "SELECT * FROM person_dates WHERE entry_id IN ($keys)" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			if ( $o->born != '' ) $out['data']['entries'][$o->entry_id]->born = $o->born ;
			if ( $o->died != '' ) $out['data']['entries'][$o->entry_id]->died = $o->died ;
		}

		// Location data
		$sql = "SELECT * FROM location WHERE entry_id IN ($keys)" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$out['data']['entries'][$o->entry_id]->lat = $o->lat ;
			$out['data']['entries'][$o->entry_id]->lon = $o->lon ;
		}

		// Multimatch
		$sql = "SELECT * FROM multi_match WHERE entry_id IN ($keys)" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$a = [] ;
			foreach ( explode(',',$o->candidates) AS $c ) $a[] = 'Q'.$c ;
			$out['data']['entries'][$o->entry_id]->multimatch = $a ;
		}

		// Aux
		$sql = "SELECT * FROM auxiliary WHERE entry_id IN ($keys)" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $out['data']['entries'][$o->entry_id]->aux[] = $o ;

		// Aliases
		$sql = "SELECT * FROM aliases WHERE entry_id IN ($keys) ORDER BY entry_id,language,label" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $out['data']['entries'][$o->entry_id]->aliases[] = $o ;

		// Language descriptions
		$sql = "SELECT * FROM descriptions WHERE entry_id IN ($keys) ORDER BY entry_id,language,label" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $out['data']['entries'][$o->entry_id]->descriptions[] = $o ;

		// Relations
		$sql = "SELECT property,mnm_relation.entry_id AS source_entry_id,entry.* FROM mnm_relation,entry WHERE entry.id=mnm_relation.target_entry_id AND mnm_relation.entry_id IN ($keys)" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $out['data']['entries'][$o->source_entry_id]->relation[] = $o ;
	}

} ;

?>