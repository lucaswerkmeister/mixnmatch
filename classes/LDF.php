<?PHP

# LINKED DATA FRAGMENTS ON WIKIDATA SEEMS TO BE INCOMPLETE/BROKEN, DO NOT USE THIS!

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class LDF extends Helper {
	public $mnm ;
	protected $expected_results_per_page = 100 ;
	protected $prefixes = [
		"rdf:" => "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
		"xsd:" => "http://www.w3.org/2001/XMLSchema#",
		"ontolex:" => "http://www.w3.org/ns/lemon/ontolex#",
		"dct:" => "http://purl.org/dc/terms/",
		"rdfs:" => "http://www.w3.org/2000/01/rdf-schema#",
		"owl:" => "http://www.w3.org/2002/07/owl#",
		"skos:" => "http://www.w3.org/2004/02/skos/core#",
		"schema:" => "http://schema.org/",
		"cc:" => "http://creativecommons.org/ns#",
		"geo:" => "http://www.opengis.net/ont/geosparql#",
		"prov:" => "http://www.w3.org/ns/prov#",
		"wikibase:" => "http://wikiba.se/ontology#",
		"wdata:" => "http://www.wikidata.org/wiki/Special:EntityData/",
		"bd:" => "http://www.bigdata.com/rdf#",
		"wd:" => "http://www.wikidata.org/entity/",
		"wdt:" => "http://www.wikidata.org/prop/direct/",
		"wdtn:" => "http://www.wikidata.org/prop/direct-normalized/",
		"wds:" => "http://www.wikidata.org/entity/statement/",
		"p:" => "http://www.wikidata.org/prop/",
		"wdref:" => "http://www.wikidata.org/reference/",
		"wdv:" => "http://www.wikidata.org/value/",
		"ps:" => "http://www.wikidata.org/prop/statement/",
		"psv:" => "http://www.wikidata.org/prop/statement/value/",
		"psn:" => "http://www.wikidata.org/prop/statement/value-normalized/",
		"pq:" => "http://www.wikidata.org/prop/qualifier/",
		"pqv:" => "http://www.wikidata.org/prop/qualifier/value/",
		"pqn:" => "http://www.wikidata.org/prop/qualifier/value-normalized/",
		"pr:" => "http://www.wikidata.org/prop/reference/",
		"prv:" => "http://www.wikidata.org/prop/reference/value/",
		"prn:" => "http://www.wikidata.org/prop/reference/value-normalized/",
		"wdno:" => "http://www.wikidata.org/prop/novalue/",
		"hint:" => "http://www.bigdata.com/queryHints#", 
	] ;

	function __construct( $mnm = '' ) {
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		$this->use_curl = true ;
	}


	public function query ( $subject='' , $predicate='' , $object='' ) {
		$subject = trim($subject) ;
		$predicate = trim($predicate) ;
		$object = trim($object) ;
		$subject_escaped = urlencode($this->fix_prefix($subject));
		$predicate_escaped = urlencode($this->fix_prefix($predicate));
		$object_escaped = urlencode($this->fix_prefix($object));
		$page = 1 ;
		while ( 1 ) {
			$received = 0 ;
			$url = "https://query.wikidata.org/bigdata/ldf?subject={$subject_escaped}&predicate={$predicate_escaped}&object={$object_escaped}&page={$page}" ;
			$result = $this->get_contents_from_url($url) ;
			$rows = explode ( "\n" , $result ) ;
			foreach ( $rows AS $row ) {
				$row = trim($row) ;
				if ( $row=='' or preg_match ( '|^[\<\@\s]|' , $row) ) continue ;
				if ( !preg_match ( '|^(\S+)\s+(\S+)\s+(.+?)[ \.]+$|' , $row , $m ) ) continue ;
				if ( preg_match('|^"(.+)"$|',$m[3],$n) ) $m[3] = $n[1] ; # Remove quotes
				if ( $subject!="" and strtolower($m[1])!=strtolower($subject) ) continue ;
				if ( $predicate!="" and strtolower($m[2])!=strtolower($predicate) ) continue ;
				if ( $object!="" and strtolower($m[3])!=strtolower($object) ) continue ;
				array_shift($m) ;
				yield $m ;
				$received += 1 ;
			}
			if ( $received < $this->expected_results_per_page ) break ;
			$page += 1 ;
		}
	}

	protected function fix_prefix ( $value ) {
		foreach ( $this->prefixes AS $k => $v ) {
			if ( !str_starts_with ( strtolower($value) , strtolower($k) ) ) continue ;
			$value = $v . substr($value,strlen($k)) ;
			return $value ;
		}
		return $value ;
	}

}