<?php

namespace MixNMatch;

require_once dirname(__DIR__) . '/vendor/autoload.php';

# ________________________________________________________________________________________________________________________
# PersonDates

final class PersonDates extends Helper {
	private $month_in_other_languages ;
	private $q2month = [ 'Q108'=>'jan','Q109'=>'feb','Q110'=>'mar','Q118'=>'apr','Q119'=>'may','Q120'=>'jun','Q121'=>'jul','Q122'=>'aug','Q123'=>'sep','Q124'=>'oct','Q125'=>'nov','Q126'=>'dec' ] ;
	protected $rewrite_functions = [ 'ml'=>'try_get_three_letter_month' , 'dp'=>'parse_date' ] ;
	protected $cf_function = 'PERSON_DATE' ;

	/**
	 * Constructor
	 *
	*/
	public function __construct ( /*int*/ $catalog , $mnm = '' ) {
		parent::__construct($catalog,$mnm);
	}

	/**
	 * Tries to convert a month name into a three-letter month code
	 *
	 * @param $month string name of a month, in any language
	 * @return string three-letter standard form of the month, or the original string if unrecognized
	*/
	public function try_get_three_letter_month ( $month ) {
		$month = strtolower(trim($month)) ;
		if ( !isset($this->month_in_other_languages) ) $this->load_month_labels() ;
		if ( isset($this->month_in_other_languages[$month]) ) return $this->month_in_other_languages[$month] ;
		return $month ;
	}

	/**
	 * Tries to fix up some partially broken date formats
	 *
	 * @param &$d string A date, to be converted to an ISO date if possible
	*/
	public function fix_date_format ( &$d ) {
		$d = trim ( $d ) ;
		if ( $d == '' ) return ;
		$d = preg_replace ( '/^0+/' , '' , $d ) ; // Leading zeros
		if ( preg_match ( '/^(\d{3,4})-(\d)-(\d)$/' , $d , $m ) ) $d = $m[1].'-0'.$m[2].'-0'.$m[3] ;
		else if ( preg_match ( '/^(\d{3,4})-(\d\d)-(\d)$/' , $d , $m ) ) $d = $m[1].'-'.$m[2].'-0'.$m[3] ;
		else if ( preg_match ( '/^(\d{3,4})-(\d)-(\d\d)$/' , $d , $m ) ) $d = $m[1].'-0'.$m[2].'-'.$m[3] ;
		while ( preg_match ( '/^\d{1,3}-/' , $d ) ) $d = "0$d" ;
		while ( preg_match ( '/^\d{1,3}$/' , $d ) ) $d = "0$d" ;
		while ( preg_match ( '/^(.+)-00$/' , $d  , $m ) ) $d = $m[1] ;
	}


	/**
	 * Removes the existing person_dates for the current catalog, if applicable
	 *
	 * @throws Exception if no catalog or code fragment set
	*/
	public function clearOldDates() {
		if ( !isset($this->catalog) ) throw new \Exception(__METHOD__.': no catalog set');
		if ( !isset($this->code_fragment) ) throw new \Exception(__METHOD__.': No code fragment loaded' ) ;
		$j = $this->code_fragment->json ;
		if ( isset($j->clear_old_dates) and !$j->clear_old_dates ) return ;
		$this->mnm->clearPersonDates($this->catalog) ;
	}

	/**
	 * Tries to convert an entry object into birth/death dates
	 *
	 * @params object $o an entry object
	 * @return array of Command
	*/
	public function processEntry ( $o ) {
		# Run code fragment
		$born = '' ;
		$died = '' ;
		if ( !isset($this->code_fragment) ) throw new \Exception(__METHOD__.': No code fragment loaded' ) ;
		$php = $this->rewriteGlobalMethods ( $this->code_fragment->php ) ;
		if ( FALSE === eval($php) ) throw new \Exception(__METHOD__.": Error in code fragment:\n{$this->code_fragment->php}\n");
		
		# Cleanup
		$this->fix_date_format ( $born ) ;
		$this->fix_date_format ( $died ) ;
		
		# Year paranoia
		if ( $born!='' and $died!='' and (int)preg_replace('/-.*$/','',$born)*1 == (int)preg_replace('/-.*$/','',$died)*1 ) return [] ;
		if ( preg_match ( '/^\d+$/' , $born) and $born*1>2050 ) return [] ;
		if ( preg_match ( '/^\d+$/' , $died) and $died*1>2050 ) return [] ;
		if ( preg_match ( '/^(\d+)/' , $born , $m ) and preg_match ( '/^(\d+)/' , $died , $n ) ) {
			if ( $n[1]*1 - $m[1]*1 > 120 ) return [] ; // Older than 120
			if ( $m[1]*1 > $n[1]*1 ) return [] ; // born after death
		}

		if ( $born . $died == '' ) return [] ; // No need to update

		// Paranoia
		if ( preg_match ( '/-00-00$/' , $born ) or preg_match ( '/-00-00$/' , $died ) ) return [] ;
		$born = preg_replace ( '/-00-\d\d$/' , '' , $born ) ;
		$died = preg_replace ( '/-00-\d\d$/' , '' , $died ) ;
		if ( $born != '' and !preg_match ( '/^[0-9]+-[0-9]{1,2}-[0-9]{1,2}$/' , $born ) and !preg_match ( '/^[0-9]+-[0-9]{1,2}$/' , $born ) and !preg_match ( '/^[0-9]+$/' , $born ) ) return [] ;
		if ( $died != '' and !preg_match ( '/^[0-9]+-[0-9]{1,2}-[0-9]{1,2}$/' , $died ) and !preg_match ( '/^[0-9]+-[0-9]{1,2}$/' , $died ) and !preg_match ( '/^[0-9]+$/' , $died ) ) return [] ;

		if ( preg_match('/^(\d+)-(\d+)-(\d+)$/',$born,$m) ) {
			if ( $m[2] * 1 < 1 ) return [] ;
			if ( $m[2] * 1 > 12 ) return [] ;
		}
		if ( preg_match('/^(\d+)-(\d+)-(\d+)$/',$died,$m) ) {
			if ( $m[2] * 1 < 1 ) return [] ;
			if ( $m[2] * 1 > 12 ) return [] ;
		}
		if ( $born == $died ) return [] ; // Yeah no
	
		$is_matched = false ;
		if ( isset($o->user) and $o->user!=null and $o->user>0 and isset($o->q) and $o->q!=null and $o->q>0 ) $is_matched = true ;

		return [ Command::setPersonDates ( $o->id , $born , $died , $is_matched ) ] ;
	}

	/**
	 * Loads months labels mapping via SPARQL
	 * Invoked when first necessary by try_get_three_letter_month()
	 *
	*/	
	private function load_month_labels() {
		$this->month_in_other_languages = [] ;
		$sparql = 'SELECT DISTINCT ?q ?label (lang(?label) AS ?lang) { ?q wdt:P31 wd:Q47018901 ; rdfs:label ?label }' ;
		$j = $this->mnm->tfc->getSPARQL($sparql) ;
		foreach ( $j->results->bindings AS $b ) {
			$q = $this->mnm->tfc->parseItemFromURL($b->q->value) ;
			$label = trim(strtolower($b->label->value)) ;
			$language = trim(strtolower($b->lang->value)) ;
			if ( !isset($this->q2month[$q]) ) continue ;
			if ( $label == '' ) continue ;
			/* Duplicate key warnings, for debugging
			if ( isset($this->month_in_other_languages[$label]) and $this->month_in_other_languages[$label]!=$this->q2month[$q] ) {
				print "MONTHS WARNING: month_in_other_languages[{$label}] is '{$this->month_in_other_languages[$label]}' but {$language}:q2month[{$q}] is '{$this->q2month[$q]}'!\n" ;
			}
			*/
			$this->month_in_other_languages[$label] = $this->q2month[$q] ;
		}
	}

	public function updateHasPersonDates ( $value ) {
		$value = $this->mnm->escape ( $value ) ;
		$sql = "UPDATE catalog SET has_person_date='{$value}' WHERE id={$this->catalog} AND has_person_date!='{$value}'" ;
		$this->mnm->getSQL ( $sql ) ;
	}

}


?>