<?PHP

namespace MixNMatch ;

require_once dirname(__DIR__) . '/vendor/autoload.php';

class AutoScrape extends Helper {
	
	public $db , $catalog , $json , $id ;
	public $mnm ;
	public $test_size = 5 ;
	public $testing = false ;
	public $verbose = false ;
	public $levels ;
	public $log2array = false ;
	public $logs = [] ;
	public $max_urls_requested = 100 ; // Testing only
	public $last_html = '' ;
	protected $autoscrape , $fp ;
	protected $out_dir = '/data/project/mix-n-match/autoscrape_out' ;
	protected $last_url = '' ;
	protected $finished = false ;
	protected $urls_requested = 0 ;
	protected $just_scrape_again = false ;
	protected $ch ; // CURL session
	protected $had_that_id = [] ;

	function __construct ( $mnm = '' ) {
		# TODO Helper constructor
		$this->mnm = is_object($mnm) ? $mnm : new MixNMatch ;
		ini_set('user_agent','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0'); # Fake user agent
		ini_set('default_socket_timeout', 900); // 900 Seconds = 15 Minutes; timeout for file_get_contents
		$this->use_curl = false ;
	}
	
	public function log ( $msg ) {
		if ( $this->log2array ) $this->logs[] = $msg ;
		else print "$msg\n" ;
	}
	
	public function getLastURL () {
		return $this->last_url ;
	}
	
	public function loadFromJSON ( $json , $catalog ) {
		$j = json_decode ( $json ) ;
		if ( $j === null ) {
			$this->error = 'Bad JSON' ;
			return false ;
		}
		$o = (object) [
			'catalog' => $catalog * 1 ,
			'json' => $json
		] ;
		$this->autoscrape = $o ;
		$this->j = json_decode ( $o->json ) ;

		return true ;
	}
	
	public function loadByCatalog ( $catalog , $ok_only = true ) {
		$ok = false ;
		$catalog *= 1 ;
		$sql = "SELECT * FROM `autoscrape` WHERE `catalog`=$catalog" ;
		if ( $ok_only ) $sql .= " AND `status` IN ('OK','','IMPORT')" ;
		$sql .= " LIMIT 1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$this->autoscrape = $o ;
			$this->j = json_decode ( $o->json ) ;
			if ( $this->j == null ) {
				$this->error = "Bad JSON: " . $o->json ;
				return false ;
			}
			$ok = true ;
		}
		if ( !$ok ) $this->error = "Can't find catalog $catalog in autoscrape table, or is running already" ;
		return $ok ;
	}
	
	public function getID () { return $this->autoscrape->id ; }
	public function getCatalog () { return $this->autoscrape->catalog ; }
	
	public function runTest() {
		$this->testing = true ;
		$this->entries = [] ;
		$this->initializeAllLevels() ;
		while ( $this->scrapeNext() && count($this->entries) < $this->test_size && $this->urls_requested <= $this->max_urls_requested ) {
		}
		$cnt = count ( $this->entries ) ;
		$this->log ( "Found $cnt entries." ) ;
		if ( $cnt > 0 && !$this->log2array ) {
			$this->log ( "Examples:" ) ;
			$this->log ( json_encode ( $this->entries , JSON_PRETTY_PRINT ) ) ;
		}
	}

	public function scrapeAll () {
		$catalog_id = $this->getCatalog() ;
		if ( !isset($catalog_id) or !isset($this->autoscrape) or !isset($this->autoscrape->id) ) return false ;
		$this->logStatus('RUNNING') ;
		$this->testing = false ;
		$starttime = microtime(true);
		$this->fn = $this->out_dir . "/" . $catalog_id . ".json" ;
		$this->fp = fopen ( $this->fn , 'w' ) ;
		fwrite ( $this->fp , '{"catalog":'.$catalog_id.',"entries":['."\n" ) ;
		$this->finished = false ;
		$this->entries = [] ;
		$this->initializeAllLevels() ;
		$first = true ;
		$again = true ;
		while ( $again ) {
			$again = $this->scrapeNext() ;
			if ( $this->just_scrape_again ) continue ;
			$out = '' ;
			foreach ( $this->entries AS $entry ) {
				$j = json_encode ( $entry ) ;
				if ( $j == '' ) continue ; // Paranoia
				if ( $first ) $first = false ;
				else $out .= ",\n" ;
				$out .= $j ;
			}
			fwrite ( $this->fp , $out ) ;
			$this->entries = [] ; // Just wrote these to file
		}
		fwrite ( $this->fp , ']}' ) ;
		fclose ( $this->fp ) ;

		if ( !isset($this->autoscrape->id) ) return true ;
		if ( isset($this->error) ) return $this->logStatus () ;
		if ( $this->testing ) return true ;
		$this->importOrUpdate() ;
		$endtime = microtime(true);
		$timediff = $endtime - $starttime;
		$sec = $timediff * 0.001 ;
		$min = ceil ( $sec / 60 ) ;
		$ts = date ( 'YmdHis' ) ;
		$this->log ( "$starttime - $endtime: $sec sec or $min min" ) ;
		$sql = "UPDATE autoscrape SET last_run_min=$min,last_run_urls={$this->urls_requested},last_update='$ts' WHERE id={$this->autoscrape->id}" ;
		$result = $this->mnm->getSQL ( $sql ) ;		

		$catalog = new Catalog ( $catalog_id , $this->mnm ) ;
		$catalog->updateStatistics() ;

		$this->mnm->queue_job($catalog_id,'automatch_by_search');
		$this->mnm->queue_job($catalog_id,'automatch_from_other_catalogs');

		$this->logStatus ( "OK" ) ;
		return true ;
	}
	
	protected function logJobStatus ( $status = '' ) {
		$catalog = $this->getCatalog() ;
		if ( !isset($catalog) or !isset($this->autoscrape) or !isset($this->autoscrape->id) ) return false ;
		$note = '' ;
		if ( $status == 'OK' ) $status = 'DONE' ;
		else if ( $status == 'RUNNING' ) {}
		else {
			$status = 'FAILED' ;
			$note = $this->error ;
		}


		$job = $this->mnm->get_job($catalog,'autoscrape') ;
		if ( isset($job) ) { // and $job->status == 'RUNNING' 
			$this->mnm->set_job_status($job,'DONE',$note);
		}
	}
	
	protected function logStatus ( $status = '' ) {
		if ( $this->testing ) return ;
		if ( !isset($this->autoscrape->id) ) return true ;
		$this->logJobStatus($status);
		if ( $status == '' ) $status = $this->error ;
		
		$sql = "UPDATE autoscrape SET status='" . $this->mnm->escape($status) . "' WHERE id={$this->autoscrape->id}" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		if ( $status == 'OK' ) return true ;
		return false ;
	}
	
	protected function importOrUpdate () {
		$this->mnm->dbmConnect(); # Reconnect
		$import = new ImportJSON( $this->mnm ) ;
		$import->process_file ( $this->fn ) ;
	}
	

	protected function getCurl () {
//	print sys_get_temp_dir()."\n" ; exit(0);
		if ( !isset($this->ch) ) {
			$this->ch = curl_init(); 
			curl_setopt($this->ch, CURLOPT_COOKIESESSION, true);
			curl_setopt ($this->ch, CURLOPT_COOKIEJAR, 'test123'); 
			curl_setopt ($this->ch, CURLOPT_COOKIEFILE, '/tmp/mnm_cookies.tmp'); 
		}
		return $this->ch ;
	}

	protected function getUrlAsBrowser ( $url ) {
		$options = [
		  'http'=>[
		    'method'=>"GET",
		    'header'=>"Accept-language: en\r\n" .
		              "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n" // i.e. An iPad 
		  ]
		];

		$context = stream_context_create($options);
		$file = file_get_contents($url, false, $context);
		return $file ;
	}

	protected function getUrlFromShellCurl ( $url ) {
		$cmd = "curl -g '{$url}'" ;
		return shell_exec($cmd);
	}
	
	protected function getContentFromURL ( $url ) {
		if ( isset($this->j->options->delay) ) {
			sleep ( $this->j->options->delay ) ;
		}

		$ret = '' ;
		if ( isset($this->j->scraper->post_url) ) {
			try {
				$params = json_decode($url) ;
				$ret = do_post_request ( $this->j->scraper->post_url , $params ) ;
			} catch ( Exception $e ) {}
		}
		if ( !isset($ret) or $ret=='' or $ret==null ) try { $ret = $this->get_contents_from_url ( $url ) ; } catch ( Exception $e ) {}
		if ( !isset($ret) or $ret=='' or $ret==null ) try { $ret = $this->getUrlAsBrowser($url); } catch ( Exception $e ) {}
		if ( !isset($ret) or $ret=='' or $ret==null ) try { $ret = $this->getUrlFromShellCurl($url); } catch ( Exception $e ) {}
		return $ret ;
	}
	
	protected function scrapeNext () {
		$this->just_scrape_again = false ;
		if ( $this->finished ) return false ;
		$previous_url = $this->last_url ;
		$this->last_url = $this->getCurrentLevelsURL() ;
		if ( $previous_url == $this->last_url ) return false ; // Paranoia
		$cnt = 0 ;
		if ( $this->verbose ) $this->log ( "{$this->last_url}" ) ;
		$h = $this->getContentFromURL ( $this->last_url ) ;

		$this->urls_requested++ ;
		if ( $this->testing ) {
			$this->last_html = $h ;
		}
		if ( $h !== false ) {
			$cnt = $this->parseHTML ( $h ) ;
		} else if ( isset($this->j->options->skip_failed) and $this->j->options->skip_failed ) {
			// Pretend this didn't happen
			$cnt = 1 ;
		} else {
			$this->error = "Failed to load {$this->last_url}" ;
			$this->finished = true ;
			return false ;
		}
		if ( !$this->increaseLastLevel($cnt>0) ) return false ;
		if ( $cnt == 0 && !$this->testing ) $this->just_scrape_again = true ;
		return true ;
	}
	
	protected function parseHTML ( $h ) {
		$cnt = 0 ;
		if ( preg_match('|\bcharset=windows-1251\b|',$h) ) { # TODO generic?
			$h = mb_convert_encoding($h, "utf-8", "windows-1251");
		}
		$h = str_replace ( "\n" , ' ' , $h ) ;
		if ( isset($this->j->options) ) {
			if ( isset($this->j->options->utf8_encode) and $this->j->options->utf8_encode ) $h = utf8_encode($h) ;
			if ( isset($this->j->options->simple_space) and $this->j->options->simple_space ) $h = preg_replace ( '/\s+/' , ' ' , $h ) ;
		}
		if ( $this->verbose ) $this->log ( $h ) ;
		if ( isset($this->j->scraper->rx_block) and $this->j->scraper->rx_block != '' ) {
			$slash = '/' ;
			$r = $this->j->scraper->rx_block ;
			$r = $slash . str_replace ( $slash , '\\'.$slash , $r ) . $slash ;
			preg_match_all ( $r , $h , $m ) ;
			if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR) {   $this->log ( "Backtrack limit was exhausted!" ) ; }
			foreach ( $m[1] AS $block ) {
				$cnt += $this->parseBlock ( $block ) ;
			}
		} else {
			$cnt += $this->parseBlock ( $h ) ;
		}
		return $cnt ;
	}
	
	protected function replaceVars ( $s , $vars , $num ) {
		foreach ( $vars AS $k => $v ) {
			$s = str_replace ( '$'.$k , $v[$num] , $s ) ;
		}
		foreach ( $this->levels AS $k => $v ) {
			$ln = '$L'.($k+1) ;
			$cv = $v->getCurrentString() ;
			$s = str_replace ( $ln , $cv , $s ) ;
		}
		return $s ;
	}
	
	protected function parseBlock ( $h ) {
		$cnt = 0 ;
		$slash = '/' ;
		$backslash = chr(92) ;

		$regexes = $this->j->scraper->rx_entry ; // Try all regular expressions; first matching one is used
		if ( !is_array($regexes) ) $regexes = [ $regexes ] ;
		foreach ( $regexes AS $r ) {
			$r = $slash . str_replace ( $slash , $backslash.$slash , $r ) . $slash  ;
			$result = preg_match_all ( $r , $h , $m ) ;
			if ( $result === false or count($m[0]) == 0 ) continue ;
			$keys = array_keys ( $m ) ;
			if ( $this->verbose ) $this->log ( json_encode($m) ) ;
			foreach ( $m[0] AS $num => $full ) {
				$entry = $this->getNewEntry() ;
				foreach ( $this->j->scraper->resolve AS $key => $how ) {

					if ( $key == 'aux' ) {
						foreach ( $how AS $aux ) {
							$na = [] ;
							foreach ( $aux AS $k1 => $v1 ) {
								$na[$k1] = $this->replaceVars ( $v1 , $m , $num ) ;
							}
							$entry->aux[] = $na ;
						}
						continue ;
					}
				
					$u = $this->replaceVars ( $how->use , $m , $num ) ;
					$u = trim ( preg_replace ( '/\s{2,}/' , ' ' , $u ) ) ;
					if ( isset($how->url_decode) and $how->url_decode ) $u = urldecode($u) ;
					if ( isset($how->rx) ) {
						foreach ( $how->rx AS $dummy => $rx ) {
//							$r = preg_quote ( $rx[0] ) ;
							$r = $rx[0] ;
							foreach ( ['/'] AS $repl ) $r = str_replace ( $repl , $backslash.$repl , $r ) ; // '(',')',,'[',']'
							$r = $slash . $r . $slash ;
							$u = preg_replace ( $r , $rx[1] , $u ) ;
						}
					}
					$u = preg_replace ( '/<.*?>/' , ' ' , $u ) ; // HTML tags => space
//					$u = preg_replace ( '/&nbsp;/' , ' ' , $u ) ; // Spaces fix
					$u = preg_replace ( '/&#0*39;/' , '\'' , $u ) ; // Quote
//					$u = preg_replace ( '/&amp;/' , '&' , $u ) ; // &
					$u = html_entity_decode ( $u) ;
					$u = preg_replace ( '/\s{2,}/' , ' ' , $u ) ; // => Collapse spaces
					$u = preg_replace ( '/^ /' , '' , $u ) ;
					$u = preg_replace ( '/ $/' , '' , $u ) ;
					$entry->$key = $u ;
				}
				if ( $entry->id == '' or $entry->name == '' ) continue ;
				if ( $entry->desc == $entry->name ) $entry->desc = '' ; // No need to repeat the name
				if ( isset($this->had_that_id[$entry->id]) ) continue ;
				$this->had_that_id[$entry->id] = 1 ;
				$this->entries[] = $entry ;
				if ( $this->verbose ) $this->log ( json_encode ( $entry ) ) ;
				$cnt++ ;
			}
			break ;
		}
		return $cnt ;
	}
	
	protected function getNewEntry () {
		return (object) [ 'id'=>'' , 'name'=>'' , 'desc'=>'' , 'type'=>'' , 'url'=>'' ] ;
	}
	
	protected function getCurrentLevelsURL () {
		$url = $this->j->scraper->url ;
		foreach ( $this->levels AS $lk => $l ) {
			$url = $l->modifyURL ( $url ) ;
		}
		return $url ;
	}

	protected function increaseLastLevel ( $last_was_successful = false ) {
		return $this->levels[0]->next() ;
	}

	protected function initializeAllLevels () {
		$this->levels = [] ;
		foreach ( $this->j->levels AS $lk => $l ) {
			$level = AutoScrapeLevel::get_level_for_mode ( $l->mode , $this , $lk ) ;
			if ( isset($level) ) $this->levels[$lk] = $level ;
			else {
				$this->log ( "Unknown level mode for $lk:" ) ;
				$this->log ( json_encode ( $l ) ) ;
				exit ( 0 ) ;
			}
		}
	}
	
	public function getNextCatalogToUpdate () {
		$sql = "SELECT * FROM autoscrape WHERE do_auto_update=1 AND status IN ('OK','') ORDER BY last_update LIMIT 1" ;
		$result = $this->mnm->getSQL ( $sql ) ;
		if ($o = $result->fetch_object()) return $o->catalog ;
	}

}

?>