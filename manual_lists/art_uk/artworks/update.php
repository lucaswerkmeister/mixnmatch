#!/usr/bin/php
<?php

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$catalog = 2936 ; # Art UK artist artwork

$mnm = new MixNMatch ;

$artists = [] ;
$sql = "SELECT id,ext_id,ext_name FROM entry WHERE catalog=2" ; # Artists from Art UK
$sql .= " AND `type`='Q5'" ;
$result = $mnm->getSQL($sql) ;
while ( $o = $result->fetch_object() ) $artists[] = $o ;

$has_id = [] ;
$sql = "SELECT ext_id FROM entry WHERE catalog={$catalog}" ;
$result = $mnm->getSQL($sql) ;
while ( $o = $result->fetch_object() ) $has_id = $o->ext_id ;


foreach ( $artists AS $artist ) {
	$url = "https://artuk.org/discover/artworks/search/actor:{$artist->ext_id}/page/500" ;
	$html = file_get_contents($url) ;
	$html = preg_replace ( '|\s+|' , ' ' , $html ) ;
	if ( !preg_match_all ( '|<li.*?> *<a id="(.+?)".*?<span class="title">(.+?)<.*?>(.*?)</li>|' , $html , $lis , PREG_SET_ORDER ) ) continue ;
	foreach ( $lis AS $li ) {
		$id = trim($li[1]) ;
		if ( isset($has_id[$id]) ) continue ;
		$name = trim($li[2]) ;
		$desc = trim(strip_tags($li[3])) ;
		$desc = str_replace ( $name , ' ' , $desc ) ;
		$desc = trim ( preg_replace ( '/\s{2,}/' , '; ' , $desc ) ) ;

		$o = (object) [
			'catalog' => $catalog,
			'name' => $name,
			'id' => $id,
			'url' => 'https://artuk.org/discover/artworks/'.$id ,
			'type' => 'Q838948',
			'desc' => $desc
		] ;

		$has_ids[$id] = 1 ;
		$artwork_id = $mnm->addNewEntry($o) ;
		$mnm->linkEntriesViaProperty($artwork_id,'P170',$artist->id) ; # Creator
	}

	sleep(1); # Be nice
}

// Deactivated due to bad results
//$mnm->queue_job($catalog,'automatch') ;
$mnm->queue_job($catalog,'microsync') ;

?>