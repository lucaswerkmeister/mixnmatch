#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;

function getConstant ( $key ) {
	global $constants ;
	if ( $key == '' ) return 0 ;
	if ( !is_scalar($key) ) return 0 ;
	if ( !isset($constants[$key]) ) $constants[$key] = count($constants)+1 ;
	return $constants[$key] ;
}

$table_keys = [ 'name' , 'born' , 'died' , 'entry_type' , 'desc' , 'gender' , 'viaf' , 'place_of_birth' , 'place_of_death' , 'occupation' ] ;

function flushOut ( $json ) {
	global $sql , $table_keys , $lc ;

	if ( $json == '' ) return ;
	$j = @json_decode ( $json ) ;
	if ( $j == null or !isset($j) ) {
#		print "CAN'T PARSE JSON :\n$json\n\n" ;
		return ;
	}
	$j = (array) $j ;

	$idk = '@id' ;
	$typek = '@type' ;
	$valuek = '@value' ;
	$infok = 'http://d-nb.info/standards/elementset/gnd#biographicalOrHistoricalInformation' ;
	$genderk = 'http://d-nb.info/standards/elementset/gnd#gender' ;
	$namek = 'http://d-nb.info/standards/elementset/gnd#variantNameForThePerson' ;
	$samek = 'http://www.w3.org/2002/07/owl#sameAs' ;
	$prefk = 'http://d-nb.info/standards/elementset/gnd#preferredName' ;

	if ( !isset($j[$idk]) ) return ;
	if ( !preg_match ( '/^http:\/\/d-nb\.info\/gnd\/(.+)$/' , $j[$idk] , $m ) ) return ;
	$data = [] ;
	foreach ( $table_keys AS $k ) $data[$k] = '' ;
	$data['ext_id'] = $m[1] ;

	$data['entry_type'] = 0 ;
	if ( isset ( $j[$typek] ) ) $data['entry_type'] = getConstant ( $j[$typek][0] ) ;
	
	if ( isset ( $j[$infok] ) ) {
		$desc = [] ;
		foreach ( $j[$infok] AS $x ) {
			if ( isset($x->$valuek) ) $desc[] = $x->$valuek ;
		}
		$data['desc'] = implode ( '; ' , $desc ) ;
	}
	
	$data['gender'] = 0 ;
	if ( isset($j[$genderk]) ) $data['gender'] = getConstant ( $j[$genderk][0]->$idk ) ;

	foreach ( $j AS $k => $v ) {
		if ( substr ( $k , 0 , strlen($prefk) ) == $prefk ) $data['name'] = $v[0]->$valuek ;
		else if ( $k == 'http://d-nb.info/standards/elementset/gnd#placeOfBirth' and preg_match('/\/gnd\/(.+)$/',$v[0]->$idk,$m) ) $data['place_of_birth'] = $m[1] ;
		else if ( $k == 'http://d-nb.info/standards/elementset/gnd#placeOfDeath' and preg_match('/\/gnd\/(.+)$/',$v[0]->$idk,$m) ) $data['place_of_death'] = $m[1] ;
		else if ( $k == 'http://d-nb.info/standards/elementset/gnd#professionOrOccupation' and preg_match('/\/gnd\/(.+)$/',$v[0]->$idk,$m) ) $data['occupation'] = $m[1] ;
		else if ( $k == 'http://d-nb.info/standards/elementset/gnd#dateOfBirth' ) $data['born'] = $v[0]->$valuek ;
		else if ( $k == 'http://d-nb.info/standards/elementset/gnd#dateOfDeath' ) $data['died'] = $v[0]->$valuek ;
	}
	if ( $data['name'] == '' and isset($j[$namek]) ) $data['name'] = $j[$namek][0] ;
	
	if ( isset($j[$samek]) ) {
		foreach ( $j[$samek] AS $x ) {
			if ( preg_match ( '/^http:\/\/viaf\.org\/viaf\/(.+)$/' , $x->$idk , $m ) ) $data['viaf'] = $m[1] ;
		}
	}

	if ( $data['name'] == '' ) return ;
	
	// Amend/flush SQL
	if ( $sql == '' ) {
		$sql = "INSERT IGNORE INTO gnd (ext_id,`" . implode('`,`',$table_keys) . "`) VALUES\n" ;
	} else $sql .= ",\n" ;
	$sql .= "('" . $data['ext_id'] . "'" ;
	foreach ( $table_keys AS $f ) {
		$sql .= ",'" . $lc->db->real_escape_string((trim($data[$f]))) . "'" ;
	}
	$sql .= ")" ;
	if ( strlen($sql) > 100000 ) {
		print "$sql;\n" ;
		$sql = '' ;
	}

	
}


$lc = new largeCatalog ( 3 ) ;
$catalog_id = $lc->getCatalogID() ;

$data_dir = '/data/scratch/large_catalogs/gnd' ;
$data_file = "$data_dir/GND.jsonld.gz" ;

$constants = [] ;

print "CONNECT ". $lc->getFullDatabaseName() . ";\n" ;
$sql = "DROP TABLE IF EXISTS `gnd`;
DELETE FROM report WHERE catalog_id=$catalog_id;
DROP TABLE IF EXISTS `gnd_constants`;
CREATE TABLE `gnd` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ext_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `born` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `died` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `place_of_birth` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `place_of_death` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `occupation` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `entry_type` int(11) NOT NULL,
  `desc` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` int(11) NOT NULL,
  `viaf` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `q` int(11) DEFAULT NULL,
  `wd_sync_version` int(11) NOT NULL DEFAULT '0',
  KEY `q` (`q`),
  KEY `viaf` (`viaf`),
  KEY `wd_sync_version` (`wd_sync_version`),
  UNIQUE KEY `ext_id` (`ext_id`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;\n" ;

print $sql ;

$sql = '' ;


$row = 0 ;
$json = '' ;
$fh = gzopen ( $data_file , 'r' ) ;
while ( !feof($fh) ) {
	$json .= trim ( fgets ( $fh ) ) ;

//$row++ ; if ( $row % 1000 == 0 ) print "Row $row\n" ;
//if ( strlen($json) > 10000 ) die ( "$json\n" ) ;

	if ( preg_match ( '/^[\[ ]+(.*)$/' , $json , $m ) ) $json = $m[1] ;
	if ( preg_match ( '/^(.+)\]\s*,\s*\[\s*([\{ ]+)$/' , $json , $m ) ) {
		if ( null != json_decode ( $m[1] ) ) {
			flushOut ( $m[1] ) ;
			$json = $m[2] ;
		} else {
			die ( "$json\n" ) ;
		}
	} else if ( preg_match ( '/^(.+),\s*(\{)$/' , $json , $m ) ) {
		if ( null != json_decode ( $m[1] ) ) {
			flushOut ( $m[1] ) ;
			$json = $m[2] ;
		}
	}
}
flushOut ( $json ) ;
print "$sql;\n" ;
gzclose($fh) ;

// CONSTANTS
$lc->openDB() ;
$sql = "CREATE TABLE `gnd_constants` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;\n" ;
$sql .= "INSERT INTO gnd_constants (id,name) VALUES " ;
$first = true ;
foreach ( $constants AS $key => $id ) {
	if ( $first ) $first = false ;
	else $sql .= "," ;
	$sql .= "($id,'" . $lc->db->real_escape_string($key) . "')" ;
}
$sql .= ";\n" ;
print $sql ;

?>