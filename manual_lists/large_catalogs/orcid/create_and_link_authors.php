#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;
require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

$lc = new largeCatalog ( 4 ) ;

// Re-use this for orcid_shared functions
$wil = $lc->wil ;
function getQS () {
	global $lc ;
	return $lc->getQS() ;
}

$sql = "
SELECT orcid_person.*,group_concat(orcid_paper.q) AS papers,count(*) AS cnt 
FROM orcid_person,orcid_paper WHERE author_orcid=ext_id AND orcid_paper.wd_sync_version=0 AND orcid_paper.q IS NOT NULL AND orcid_person.q IS NULL
GROUP BY ext_id HAVING cnt>4 LIMIT 20000" ;

function getTypedLabelItem ( $type_qs , $label ) {
	global $lc ;
	$ret = [] ;
	$db = $lc->openWikidataDB() ;
	foreach ( $type_qs AS $k => $v ) $type_qs[$k] = $db->real_escape_string($v) ;
	$sql = "
SELECT DISTINCT page_title FROM wbt_text,wbt_item_terms,wbt_type,wbt_term_in_lang,wbt_text_in_lang,pagelinks,page
WHERE wbit_term_in_lang_id = wbtl_id AND wbtl_type_id = wby_id AND wbtl_text_in_lang_id = wbxl_id AND wbxl_text_id = wbx_id
AND wby_name IN ('label','alias')
AND wbx_text='" . $db->real_escape_string($label) . "'
AND page_namespace=0 AND page_title=concat('Q',wbit_item_id)
AND page_id=pl_from AND pl_title IN ('" . implode("','",$type_qs) . "')
ORDER BY wbit_item_id" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()) $ret[] = $o->term_full_entity_id ;
	return $ret ;
}

function createNewAuthorItem ( &$o ) {
	global $lc ;
	if ( isset($o->q) ) return ;
	$sparql = "SELECT ?q { ?q wdt:P496 '{$o->ext_id}' }" ;
	$items = getSPARQLitems ( $sparql ) ;
	if ( count($items) > 1 ) return ; // Say what?!?
	if ( count($items) == 1 ) { // Found'em!
		$o->q = $items[0] ;
		$sql = "UPDATE orcid_person SET q={$o->q} WHERE id={$o->id} AND q IS NULL" ;
		if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
		return ;
	}
	
	$name = $o->given_name . ' ' . $o->family_name ;
	$orcid = $o->ext_id ;
	
	$commands = [] ;
	$commands[] = "CREATE" ;
	$commands[] = "LAST\tP496\t\"$orcid\"" ;
	$commands[] = "LAST\tP31\tQ5" ;
	$commands[] = "LAST\tLen\t\"$name\"" ;
/*	
	# Family name
	$items = getTypedLabelItem ( ['Q101352'] , $o->family_name ) ;
	if ( count($items) > 0 ) $commands[] = "LAST\tP734\t" . $items[0] ;

	# Given name
	$given_name = ucfirst ( strtolower ( array_shift ( explode ( ' ' , trim($o->given_name) ) ) ) ) ;
	if ( strlen($given_name) > 1 ) {
		$items = getTypedLabelItem ( ['Q202444','Q12308941','Q11879590'] , $given_name ) ;
		if ( count($items) > 0 ) $commands[] = "LAST\tP735\t" . $items[0] ;
	}
*/
	# Run commands
	$qs = $lc->getQS() ;
	$qs->use_command_compression = true ;
	$tmp = $qs->importData ( implode("\n",$commands) , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	$author_q = $qs->last_item ;
	if ( !isset($author_q) or $author_q == '' ) continue ; // A problem
	$o->q = preg_replace('/\D/' , '' , $author_q ) ; // Set new author q
	
	// Set q in original table
	$sql = "UPDATE orcid_person SET q={$o->q} WHERE id={$o->id} AND q IS NULL" ;
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
	add_person_ids_from_orcid ( "Q".$o->q ) ; // Fill in other meta-data
}

function addAuthor ( $o , $paper_q ) {
	global $lc ;
	$i = $lc->wil->getItem ( $paper_q ) ;
	if ( !isset($i) ) {
		print "Paper item $paper_q was not loaded\n" ;
		return ;
	}
	if ( !isset($i->j) or !isset($i->j->claims) or !isset($i->j->claims->P2093) ) {
		print "Paper item $paper_q has no 'author name string'\n" ;
		return ;
	}
	
	# Paranoia: No one with the same family name is claiming this paper
	$name = $lc->db->real_escape_string ( $o->family_name ) ;
	$sql = "SELECT count(*) AS cnt FROM orcid_paper,orcid_person WHERE author_orcid=ext_id and orcid_paper.q=" . preg_replace('/\D/','',$paper_q) . " AND family_name='$name'" ;
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
	$x = $result->fetch_object() ;
	if ( $x->cnt != 1 ) {
		print "{$x->cnt} authors named '$name' claim paper $paper_q\n" ;
		return ;
	}
	
	$candidates = [] ;
	$ans = $i->j->claims->P2093 ;
	foreach ( $ans AS $k => $an ) {
		if ( !isset($an->mainsnak) ) continue ;
		if ( $an->type != 'statement' ) continue ;
		if ( $an->mainsnak->datatype != 'string' ) continue ;
		
		$name = $an->mainsnak->datavalue->value ;
		$pattern = '/ ' . $o->family_name . '$/' ;
		if ( !preg_match ( $pattern , $name ) ) continue ;
		if ( preg_match ( '/,/' , $name ) ) continue ;
		
		$first_letter_full_name = ucfirst(trim($name)[0]) ;
		$first_letter_given_name = ucfirst(trim($o->given_name)[0]) ;
		if ( $first_letter_full_name != $first_letter_given_name ) continue ;
		
		$candidates[$k] = $an ;
	}
	
	// Bail unless there is exactly one author with that last name
	if ( count($candidates) != 1 ) return ;

	$s = [] ; # Statement
	foreach ( $candidates AS $c ) $s = $c ;
	$name = $s->mainsnak->datavalue->value ;
#print_r ( $s ) ;
	
	$qualifiers = $i->statementQualifiersToQS ( $s ) ;
	$qualifiers[] = "P1932\t\"$name\"" ; # Adding name as "stated as"
#print_r ( $qualifiers ) ;
	
	$references = $i->statementReferencesToQS ( $s ) ;
#print_r ( $references ) ;
	
	$author_q = 'Q' . $o->q ;
	$base_command = "$paper_q\tP50\t$author_q" ;
	
	$commands = [] ;
	foreach ( $references AS $ref ) {
		$cmd = $base_command ;
		if ( count($commands) == 0 ) $cmd .= "\t" . implode ( "\t" , $qualifiers ) ; // First one
		$cmd .= "\t" . implode ( "\t" , $ref ) ;
		$commands[] = $cmd ;
	}
	if ( count($commands) == 0 ) $commands[] = $base_command . "\t" . implode ( "\t" , $qualifiers ) ; // No references
	$commands[] = "-$paper_q\tP2093\t\"$name\"" ; # Remove string
#print_r ( $commands ) ;

	$qs = $lc->getQS() ;
	$qs->use_command_compression = true ;
	$tmp = $qs->importData ( implode("\n",$commands) , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	
	$lc->openDB() ;
	$sql = "UPDATE orcid_paper SET wd_sync_version=1 WHERE q=" . preg_replace('/\D/','',$paper_q) . " AND author_orcid='" . $o->ext_id . "'" ;
#print "$sql\n" ;
	if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
}

if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");
while($o = $result->fetch_object()) {
	if ( trim($o->given_name) == '' or trim($o->family_name) == '' ) continue ;
	if ( $o->cnt > 100 ) continue ; // Many papers for an author, paranoia
	
	createNewAuthorItem ( $o ) ; // Create author item, if necessary
	if ( !isset($o->q) ) continue ; // Paranoia
	
	// Add to papers
	$paper_qs = explode ( ',' , $o->papers ) ;
	$lc->wil->loadItems ( $paper_qs ) ;
	foreach ( $paper_qs AS $qnum ) {
		addAuthor ( $o , "Q$qnum" ) ;
	}
}

?>