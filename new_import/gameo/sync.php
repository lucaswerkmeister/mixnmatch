#!/usr/bin/php
<?PHP

$url = "http://www.gameo.org/api.php?action=query&list=allpages&apfilterredir=nonredirects&aplimit=500&format=json" ;
$qc = 'query-continue' ;

$out = array ( 'catalog' => 19 , 'entries' => array() ) ;
$add = '' ;
while ( 1 ) {
	$j = json_decode ( file_get_contents ( $url.$add) ) ;
	if ( !isset($j->query) ) break ;
	if ( !isset($j->query->allpages) ) break ;
	foreach ( $j->query->allpages AS $v ) {
		$title = $v->title ;
		$desc = '' ;
		if ( preg_match ( '/^(.+)\s\((.+)\)$/' , $title , $m ) ) {
			$title = $m[1] ;
			$desc = $m[2] ;
			
			// Fix "lastname, firstname"
			if ( preg_match ( '/\d\d\d\d/' , $desc ) and preg_match ( '/^(.+), (.+)$/' , $title , $m ) ) {
				$title = $m[2] . " " . $m[1] ;
			}
		}
		
		$out['entries'][] = array (
			'id' => $v->pageid ,
			'name' => $title ,
			'desc' => $desc ,
			'url' => "http://www.gameo.org/index.php?title=" . urlencode ( str_replace ( ' ' , '_' , $v->title ) ) ,
			'aux' => array()
		) ;
	}
	
	if ( isset($j->$qc) and isset($j->$qc->allpages) and isset($j->$qc->allpages->apcontinue) ) {
		$add = '&apcontinue=' . urlencode($j->$qc->allpages->apcontinue) ;
	} else {
		break ;
	}
}

$fh = fopen ( "out.json" , 'w' ) ;
fwrite ( $fh , json_encode ( $out ) ) ;
fclose ( $fh ) ;


?>