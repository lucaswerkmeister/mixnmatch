#!/usr/bin/python
import csv
import json

# id,firstName,lastName,birthDate,birthPlace

j = { 'catalog':80 , 'entries':[] }
with open('data_by_nono.csv') as csvfile:
	reader = csv.reader ( csvfile , delimiter=',', quotechar='"')
	for row in reader:
		if row[0] == 'id' :
			continue
		id = row[0]
		name = row[1] + ' ' + row[2][0] + row[2][1:].lower()
		desc = "born " + row[3] + " at " + row[4]
		url = ''
		j['entries'].append ( { 'id':id , 'name':name , 'desc':desc , 'url':url , 'aux':[] , 'type':'person' } )

f = open('out.json','w')
f.write ( json.dumps ( j ) )
f.close()
