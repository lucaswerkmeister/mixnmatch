#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use URI::Escape;
use Data::Dumper ;
use JSON ;

my $testing = 0 ;

unless ( $testing ) {
	`curl -o full.gz http://iconclass.org/data/iconclass.20121019.nt.gz` ;
}

open OUT, "> out.json" or die $! ;
print OUT '{"catalog":51,"entries":' ;
print OUT "[\n" ;

my $first = 1 ;
open IN , 'zcat full.gz |' ;
while ( <IN> ) {
	if ( $_ =~ m|^<(http://iconclass.org/)(.+?)> <http://www.w3.org/2004/02/skos/core#prefLabel> "(.+?)"\@en \.$| ) {

		my $out = get_blank() ;
		$out->{id} = uri_unescape($2) ;
		$out->{url} = "$1$2" ;
		$out->{name} = $3 ;
#		$out->{name} = decode_json ( '"' . $out->{name} . '"' ) ;
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}
close IN ;

print OUT "\n]}" ;

`rm *.gz` unless $testing ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => '' } ;
}
