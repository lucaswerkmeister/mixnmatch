#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $testing = 1 ;

unless ( $testing ) {
	`curl -o full.zip http://vocab.getty.edu/dataset/tgn/full.zip` ;
}

open OUT, "> out.json" or die $! ;
print OUT '{"catalog":49,"entries":' ;
print OUT "[\n" ;

my $first = 1 ;
my $out ;
open IN , 'unzip -p full.zip TGNOut_Full.nt |' ; #  head -10000 |
while ( <IN> ) {
	if ( $_ =~ m|^<(http://vocab.getty.edu/tgn/)(\d+)>.*core#prefLabel> "(.+)" .| ) {
		if ( not defined $out->{id} or $2 != $out->{id} ) {
			$out = get_blank() ;
			$out->{id} = $2 ;
			$out->{url} = "$1$2" ;
		}
		$out->{name} = $3 unless defined $out->{name} ;
	} elsif ( $_ =~ m|#parentString> "(.+)" .$| ) {
		$out->{desc} = $1 ;
		if ( defined $out->{id} ) {
			$out->{name} = decode_json ( '"' . $out->{name} . '"' ) ;
			$out->{desc} = decode_json ( '"' . $out->{desc} . '"' ) ;
			if ( $first ) { $first = 0 ; }
			else { print OUT ",\n" ; }
			print OUT encode_json ( $out ) ;
		}
		$out = {} ;
	}
}
close IN ;

print OUT "\n]}" ;

`rm *.zip` unless $testing ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'location' } ;
}