#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;
use XML::Simple;


open OUT, "> out.json" or die $! ;

my $first = 1 ;
print OUT '{"catalog":65,"entries":' ;
print OUT "[\n" ;


foreach my $file ( `ls *.rdf` ) {
	chomp $file ;
	print "$file\n" ;
	my $xml = new XML::Simple;
	my $data = $xml->XMLin($file);
	parseXML ( $data ) ;
}


print OUT "\n]}" ;

0 ;

sub parseXML {
	my ( $d ) = @_ ;
	foreach my $x ( @{$d->{'skos:Concept'}} ) {
		next if ( not defined $x->{'skos:prefLabel'} ) ; # Bah!
		next if ( not defined $x->{'rdf:about'} ) ; # Bah!
		next if ( ref($x->{'skos:prefLabel'}) ne 'HASH' ) ; # Paranoia
		next unless $x->{'rdf:about'} =~ m|^(.+/voc/)(.+)$| ;
		
		my @desc ;
		if ( defined $x->{'skos:scopeNote'} ) {
			if ( ref($x->{'skos:scopeNote'}) eq 'ARRAY' ) {
				foreach my $d2 ( @{$x->{'skos:scopeNote'}} ) {
					if ( ref($d2) eq 'HASH' ) {
						push @desc , $d2->{content} ;
					} else {
						push @desc , $d2 ;
					}
				}
			} elsif ( ref($x->{'skos:scopeNote'}) eq 'HASH' ) {
				push @desc , $x->{'skos:scopeNote'}->{content} if defined $x->{'skos:scopeNote'}->{content} ;
			} else {
#				print Dumper($x->{'skos:scopeNote'}) . "\n" ;
			}
		}
		
		
		my $out = {
			"id" => $2 ,
			"name" => $x->{'skos:prefLabel'}->{content} ,
			"desc" => join ( ';' , @desc ) ,
			"url" => "$1$2" ,
			"type" => "" ,
			"aux" => []
		} ;


		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}
