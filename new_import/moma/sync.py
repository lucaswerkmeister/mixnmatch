#!/usr/bin/python
import csv
import json

# curl -o moma.csv 'https://raw.githubusercontent.com/MuseumofModernArt/collection/master/Artworks.csv'
# 0:Title,1:Artist,2:ArtistBio,3:Date,4:Medium,5:Dimensions,6:CreditLine,7:MoMANumber,8:Classification,9:Department,10:DateAcquired,11:CuratorApproved,12:ObjectID,13:URL


j = { 'catalog':82 , 'entries':[] }
with open('moma.csv') as csvfile:
	reader = csv.reader ( csvfile , delimiter=',', quotechar='"')
	for row in reader:
		if row[1] == 'Artist' :
			continue
		id = row[12]
		name = row[0]
		desc = "By " + row[1] + " (" + row[2] + "); " + row[3] + "; " + row[4] + "; " + row[5] ;
		url = row[13]
		j['entries'].append ( { 'id':id , 'name':name , 'desc':desc , 'url':url , 'aux':[] , 'type':'' } )

f = open('out.json','w')
f.write ( json.dumps ( j ) )
f.close()
