#!/usr/bin/perl

BEGIN{push @INC, '.';}

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;
use Text::CSV ;

# Download fresh CSV files
if ( 1 ) {
	`rm *.csv.gz` ;
#	`curl -o locations.csv.gz http://atlantides.org/downloads/pleiades/dumps/pleiades-locations-latest.csv.gz` ;
#	`curl -o names.csv.gz http://atlantides.org/downloads/pleiades/dumps/pleiades-names-latest.csv.gz` ;
	`curl -o places.csv.gz http://atlantides.org/downloads/pleiades/dumps/pleiades-places-latest.csv.gz` ;
}

open OUT, "> out.json" or die $! ;

my $first = 1 ;
print OUT '{"catalog":40,"entries":' ;
print OUT "[\n" ;

my $csv = Text::CSV->new ( { binary => 1 } ) or die "Cannot use CSV: ".Text::CSV->error_diag ();
my $fh ;
open $fh , 'zcat places.csv.gz |' ;
while ( my $r = $csv->getline( $fh ) ) {
	next if $r->[0] eq 'authors' ;
	my $out = {
		id => $r->[11] ,
		name => $r->[24] ,
		desc => $r->[6] . '; ' . $r->[22] ,
		url => 'http://pleiades.stoa.org'.$r->[16] ,
		aux => [] ,
		type => 'location'
	} ;
	$out->{name} = $r->[11] if $out->{name} eq '' ;
	utf8::decode( $out->{name} ) ;
	utf8::decode( $out->{desc} ) ;
	if ( $r->[18] ne '' and $r->[18] ne 'reprLatLong' ) {
		push @{$out->{aux}} , { prop => 'P625' , id => $r->[18] } ;
	}

	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $out ) ;
}
$csv->eof or $csv->error_diag();
close $fh ;

print OUT "\n]}" ;

0 ;
