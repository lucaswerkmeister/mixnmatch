<?php

# ./vendor/bin/phpunit tests/MixNMatchApiTest.php

use PHPUnit\Framework\TestCase;

require_once ( "/data/project/mix-n-match/tests/shared.php" ) ;

class MixNMatchApiTest extends TestCase {

    protected function setUp(): void {
    }

    protected function simulate_query ( $query , $params ) {
		$_REQUEST = $params ;
    	$api = new MixNMatch\API ;
    	return $api->query ( $query ) ;
    }

    public function test_get_users() {
    	$api = new MixNMatch\API ;
    	$users = $api->get_users(['2'=>'2','4'=>'4']);
    	$this->assertSame(count($users),2);
    	$this->assertSame($users{'2'}->name,'Magnus Manske');
    }

    public function test_get_entry_by_extid() {
    	$out = $this->simulate_query('get_entry_by_extid',['catalog'=>'1','extid'=>'101004970']);
    	$this->assertSame($out['data']['entries']['1993']->ext_name,'Ceadda');
    	$out = $this->simulate_query('get_entry_by_extid',['catalog'=>'0','extid'=>'101004970']);
    	$this->assertSame($out,['status'=>'Invalid catalog ID']);
    }

    public function test_catalog_overview() {
    	$out = $this->simulate_query('catalog_overview',['catalogs'=>'0,1,2']);
    	$this->assertSame(count($out['data']),2);
    	$this->assertSame($out['data']['1']['name'],'ODNB');
    }

    public function test_get_user_info() {
    	$out = $this->simulate_query('get_user_info',['username'=>'Magnus Manske']);
    	$this->assertSame($out['data']->id,'2');
    }

    public function test_get_jobs() {
    	$out = $this->simulate_query('get_jobs',['catalog'=>'2930']);
    	$this->assertTrue(count($out['data'])>=4);
    	$this->assertTrue(count($out['data'])<20);
    	foreach ( $out['data'] as $job ) {
    		$this->assertSame($job->catalog,'2930');
    		foreach ( ['id','action','status','last_ts','user_id','user_name'] AS $key ) {
    			$this->assertTrue(isset($job->$key));
    		}
    	}
    }

    public function test_sparql_list() {
    	$sparql = "SELECT DISTINCT ?item ?itemLabel WHERE { ?painting wdt:P195 wd:Q82941 ; wdt:P31 wd:Q3305213 ; wdt:P170 ?item SERVICE wikibase:label { bd:serviceParam wikibase:language 'en' }}" ;
    	$out = $this->simulate_query('sparql_list',['sparql'=>$sparql]);
    	$this->assertSame($out['data']['entries']['717128']->ext_id,'BKSR631R');
    	$this->assertSame($out['data']['users']['0']->name,'automatic');
	}

    public function test_catalog_details() {
    	$out = $this->simulate_query('catalog_details',['catalog'=>'1']);
    	$this->assertSame($out['data']['type'][0]->type,'Q5');
    	$this->assertSame($out['data']['ym'][0]->ym,'201311');
    }

    public function test_create() {
    	$out = $this->simulate_query('create',['catalog'=>'3']);
    	$this->assertTrue(count($out['data'])>10);
    }

    public function test_sitestats() {
    	$out = $this->simulate_query('sitestats',['catalog'=>'793']);
    	$this->assertTrue($out['data']['bgwiki']>3);
    }

    public function test_get_wd_props() {
    	$out = $this->simulate_query('get_wd_props',[]);
    	$this->assertTrue(in_array(214,$out));
    }

    public function test_top_missing() {
    	$out = $this->simulate_query('top_missing',['catalogs'=>'3491,1388']);
    	$this->assertTrue(count($out['data'])>1);
    }

    public function test_locations() {
    	$out = $this->simulate_query('locations',['bbox'=>'0.0760030746459961,52.19821707532257,0.15840053558349612,52.21231387558591']);
    	$this->assertTrue(count($out['data'])>3200);
    }

    public function test_download() {
    	$out = $this->simulate_query('download',['catalog'=>'64']);
    	$this->assertTrue(strpos($out, '957538	Legros-Alphonse') !== false);
    }

    public function test_download2() {
    	$out = $this->simulate_query('download2',[
    		'catalogs'=>'64',
    		'columns'=>'{"exturl":1,"username":1,"aux":0,"dates":0,"location":0,"multimatch":1}',
    		'hidden'=>'{"any_matched":0,"firmly_matched":0,"user_matched":0,"unmatched":0,"automatched":0,"name_date_matched":0,"aux_matched":0,"no_multiple":0}',
    		'format'=>'json'
    	]);
    	$out = json_decode($out) ;
    	$this->assertTrue(count($out)==1593);
    	$this->assertTrue($out[0]->entry_id=='3834657');
    }

    public function test_redirect() {
    	$expected_html = '<html><head><META http-equiv="refresh" content="0;URL=http://www.arthermitage.org/Amman-Iost/index.html"></head><body></body></html>' ;
    	$out = $this->simulate_query('redirect',['catalog'=>'64','ext_id'=>'Amman-Iost']);
    	$this->assertSame($out,$expected_html);
    }

    public function test_get_code_fragments() {
    	$out = $this->simulate_query('get_code_fragments',['catalog'=>'3512']);
    	$this->assertSame($out['data']['fragments'][0]->id,'633');
    	$this->assertTrue(count($out['data']['all_functions'])>3);
    }

    public function test_proxy_entry_url() {
    	$out = $this->simulate_query('proxy_entry_url',['entry_id'=>'93739654']);
    	$this->assertTrue(strpos($out, '<meta property="og:title" content="Ray Norris" />') !== false);
    }

    public function test_random() {
    	$out = $this->simulate_query('random',['id'=>'91618180']);
    	$this->assertSame($out['data']->ext_id,'1853');
    }

    public function test_missingpages() {
    	$out = $this->simulate_query('missingpages',['site'=>'enwiki','catalog'=>'3512']);
    	$this->assertTrue(isset($out['data']['entries']['91617288'])); # Might change if a page [[en:Pancrates Arcadius]] is created
    	$this->assertTrue(isset($out['data']['users']['4']));
    }

    public function test_catalog() {
    	$out = $this->simulate_query('catalog',['catalog'=>'1543','meta'=>'{"show_multiple":1,"per_page":51}']);
    	$this->assertSame(count($out['data']['entries']),51); # Might change with catalog
    }

    public function test_get_entry() {
    	$out = $this->simulate_query('get_entry',['catalog'=>'1543','entry'=>'57046307']);
    	$this->assertSame($out['data']['entries']['57046307']->ext_id,'146488221');
    }

    /*
    // Need to find better ones
    public function test_prep_new_item() {
    	$out = $this->simulate_query('prep_new_item',['entry_ids'=>'57187354,84183295']);
    	$this->assertTrue(strpos($out['data'], '{"mainsnak":{"snaktype":"value","property":"P31","datavalue":{"type":"wikibase-entityid","value":{"entity-type":"item","id":"Q5"}}},"type":"statement","rank":"normal"}') !== false);
    	$this->assertTrue(strpos($out['data'], '"sv":{"language":"sv","value":"Joachim Lindemann"}') !== false);
    }
    */

}

?>