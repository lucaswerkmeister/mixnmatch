<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once ( "/data/project/mix-n-match/tests/shared.php" ) ;
use PHPUnit\Framework\TestCase;

class HTMLtoDescriptionTest extends TestCase {

    protected $mnm ;
    protected $mnm_helper ;

    protected function setUp(): void {
        $this->mnm = new MixNMatch\MixNMatch ;
        $this->mnm_helper = new MixNMatch\HTMLtoDescription ( 1 , $this->mnm , true ) ;
    }

    public function test_rewriteGlobalMethods() {
        $method = getMethod('MixNMatch\HTMLtoDescription','rewriteGlobalMethods');
        
        // Neutral functionality already tested in MixnMatchHelperTest

        $this->assertSame('bclean_html("foo");',$method->invokeArgs($this->mnm_helper, ['bclean_html("foo");']));
        $this->assertSame('clean_html_("foo");',$method->invokeArgs($this->mnm_helper, ['clean_html_("foo");']));
        $this->assertSame('$this->clean_html("foo");',$method->invokeArgs($this->mnm_helper, ['clean_html("foo");']));

        $this->assertSame(';xdp("foo");',$method->invokeArgs($this->mnm_helper, [';xdp("foo");']));
        $this->assertSame(';dp_("foo");',$method->invokeArgs($this->mnm_helper, [';dp_("foo");']));
        $this->assertSame(';$this->parse_date("foo");',$method->invokeArgs($this->mnm_helper, [';dp("foo");']));
    }
}

?>